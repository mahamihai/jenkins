package com.montran.internsa.bankapp.interceptor;

import java.net.URI;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.montran.internsa.bankapp.service.ProfileService;
import com.montran.internsa.bankapp.service.UserService;
import com.montran.internsa.bankapp.service.XMLService;

/**
 * 
 * @author Marius Supuran
 *
 */
@Component
public class RightInterceptor implements HandlerInterceptor {

	@Autowired
	private UserService userService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private XMLService xMLService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		// TODO maybe filter some options
		if (!request.getMethod().equals("OPTIONS")) {

			URI uri = new URI(request.getRequestURL().toString());
			String path = uri.getPath();
			Map<String, String> map = profileService.getProfileFunctions();
			String function = map.get(path);
			if (function != null) {
				if (userService.checkFunction(function)) {
					return HandlerInterceptor.super.preHandle(request, response, handler);
				} else {
					return false;
				}
			} else {
				List<String> approved = xMLService.parse("approved.xml");
				if (approved.contains(path)) {
					return HandlerInterceptor.super.preHandle(request, response, handler);
				} else {
					return false;
				}
			}
		}

		return HandlerInterceptor.super.preHandle(request, response, handler);

	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
	}

}
