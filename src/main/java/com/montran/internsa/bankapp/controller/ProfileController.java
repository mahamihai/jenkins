package com.montran.internsa.bankapp.controller;

import java.lang.reflect.Field;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.montran.internsa.bankapp.entity.Profile;
import com.montran.internsa.bankapp.service.ProfileService;

/**
 * 
 * @author Corina Cordea
 *
 */
@RestController
@RequestMapping(value = "/profile")
public class ProfileController {
	@Autowired
	private ProfileService profileService;

	// function
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public @ResponseBody List<Profile> view() {
		return profileService.findAll();
	}

	// function
	@RequestMapping(value = "/viewone", method = RequestMethod.GET)
	public @ResponseBody Profile getProfile(@RequestParam("id") long id) {
		return profileService.findById(id).get();
	}

	// not approved
	@RequestMapping(value = "/activeProfiles", method = RequestMethod.GET)
	public @ResponseBody List<Profile> viewActiveProfiles() {
		return profileService.findAllModifiable();
	}

	// function
	@RequestMapping(value = "/pending", method = RequestMethod.GET)
	public @ResponseBody List<Profile> viewPendingProfiles() {
		return profileService.findAllPending();
	}

	// approved
	@RequestMapping(value = "/getProfileFunctions", method = RequestMethod.GET)
	public @ResponseBody List<String> getFunctions() {
		return profileService.getFunctions();
	}

	// not approved
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	public @ResponseBody Profile findByName(@RequestParam String name) {
		return profileService.findByName(name);

	}

	// not approved
	@RequestMapping(value = "/findAll", method = RequestMethod.GET)
	public @ResponseBody List<Profile> findAllByName(@RequestParam String name) {
		return profileService.findAllByName(name);

	}

	// function
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public @ResponseBody List<String> insert(@RequestBody Profile profile) {
		return profileService.add(profile).getErrors();

	}

	// function
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody List<String> update(@RequestBody Profile profile) {
		return profileService.modify(profile).getErrors();
	}

	// function
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody List<String> delete(@RequestBody Profile profile) {
		return profileService.delete(profile).getErrors();
	}

	// function
	@RequestMapping(value = "/approve", method = RequestMethod.POST)
	public @ResponseBody List<String> approve(@RequestBody Profile profile) {
		return profileService.approve(profile).getErrors();
	}

	// function
	@RequestMapping(value = "/reject", method = RequestMethod.POST)
	public @ResponseBody List<String> reject(@RequestBody Profile profile) {
		return profileService.reject(profile).getErrors();
	}

	// approved
	@RequestMapping(value = "/model", method = RequestMethod.GET)
	public @ResponseBody String getAccountModel() throws JSONException {
		JSONObject jsonString = new JSONObject();
		for (Field aux : Profile.class.getDeclaredFields()) {
			jsonString.put(aux.getName(), "");
		}
		String json = jsonString.toString();
		return json;

	}

}
