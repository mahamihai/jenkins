package com.montran.internsa.bankapp.controller;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.montran.internsa.bankapp.service.XMLService;

@RestController
public class DataController {

	@Autowired
	private XMLService xMLService;

	// approved
	@RequestMapping(value = "/functionalities", method = RequestMethod.GET)
	public @ResponseBody String viewFunctionalities() {
		Map<String, String> map = xMLService.parseFunctions("functionalities.xml", "forward");
		return new JSONObject(map).toString();
	}

	// approved
	@RequestMapping(value = "/functions", method = RequestMethod.GET)
	public @ResponseBody String viewFunctions() {
		Map<String, String> map = xMLService.parseFunctions("functions.xml", "forward");
		return new JSONObject(map).toString();
	}

	// approved
	@RequestMapping(value = "/rights", method = RequestMethod.GET)
	public @ResponseBody List<String> viewRights() {
		return xMLService.parse("rights.xml");
	}

}
