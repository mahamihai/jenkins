package com.montran.internsa.bankapp.controller;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.entity.Balance;
import com.montran.internsa.bankapp.entity.api.AccountApi;
import com.montran.internsa.bankapp.entity.api.BalanceApi;
import com.montran.internsa.bankapp.service.BalanceService;

/**
 * 
 * @author Marius Supuran
 *
 */
@RestController
public class BalanceController {

	@Autowired
	private BalanceService balanceService;
	@Autowired
	private ModelMapper modelMapper;

	// function
	@RequestMapping(value = "/balance/view", method = RequestMethod.GET)
	public @ResponseBody List<BalanceApi> view() {
		List<Balance> balances = balanceService.findAll();
		return balances.stream().map(balance -> modelMapper.map(balance, BalanceApi.class))
				.collect(Collectors.toList());
	}

	// function
	@RequestMapping(value = "/balance/view/date", method = RequestMethod.POST)
	public @ResponseBody List<BalanceApi> viewByAccountAndDate(@RequestBody AccountApi accountApi) {
		Account account = new Account(accountApi.getId());
		List<Balance> balances = balanceService.findBalanceByAccountAndBetweenDates(account, accountApi.getDate1(),
				accountApi.getDate2());
		return balances.stream().map(balance -> modelMapper.map(balance, BalanceApi.class))
				.collect(Collectors.toList());
	}

	// approved
	@RequestMapping(value = "/balance/model", method = RequestMethod.GET)
	public @ResponseBody String getAccountModel() throws JSONException {
		JSONObject jsonString = new JSONObject();
		for (Field aux : BalanceApi.class.getDeclaredFields()) {
			jsonString.put(aux.getName(), "");
		}
		String json = jsonString.toString();
		System.out.println(json);
		return json;

	}

}
