package com.montran.internsa.bankapp.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.montran.internsa.bankapp.entity.Payment;
import com.montran.internsa.bankapp.entity.api.PaymentApi;
import com.montran.internsa.bankapp.service.PaymentService;

/**
 * 
 * @author Radu Nechiti
 *
 */
@RestController
public class PaymentController {
	@Autowired
	private PaymentService paymentService;
	@Autowired
	private ModelMapper modelMapper;

	// function
	@GetMapping("/payment/view")
	public List<PaymentApi> getAllPayment() {
		List<Payment> payments = paymentService.findAll();
		return payments.stream().map(payment -> modelMapper.map(payment, PaymentApi.class))
				.collect(Collectors.toList());
	}

	// function
	@RequestMapping(value = "/payment/viewone", method = RequestMethod.GET)
	public @ResponseBody PaymentApi getPayment(@RequestParam("id") long id) {
		Payment payment = paymentService.getPaymentById(id);
		return modelMapper.map(payment, PaymentApi.class);
	}

	// function
	@RequestMapping(value = "/payment/enter", method = RequestMethod.POST)
	public @ResponseBody List<String> add(@RequestBody PaymentApi payment) {
		return paymentService.add(modelMapper.map(payment, Payment.class), payment.getCreditAccountNumber(),
				payment.getDebitAccountNumber()).getErrors();
	}

	// function
	@RequestMapping(value = "/payment/verify", method = RequestMethod.POST)
	public @ResponseBody List<String> verify(@RequestBody Payment payment) {
		return paymentService.verify(payment).getErrors();
	}

	// function
	@RequestMapping(value = "/payment/approve", method = RequestMethod.POST)
	public @ResponseBody List<String> approve(@RequestBody Payment payment) {
		return paymentService.approve(payment).getErrors();
	}

	// function
	@RequestMapping(value = "/payment/authorize", method = RequestMethod.POST)
	public @ResponseBody List<String> authorize(@RequestBody Payment payment) {
		return paymentService.authorize(payment).getErrors();
	}

	// TODO
	// maybe refactor
	// function
	@RequestMapping(value = "/payment/verify/cancel", method = RequestMethod.POST)
	public @ResponseBody List<String> verifyCancel(@RequestBody Payment payment) {
		return paymentService.cancel(payment).getErrors();
	}

	// function
	@RequestMapping(value = "/payment/approve/cancel", method = RequestMethod.POST)
	public @ResponseBody List<String> approveCancel(@RequestBody Payment payment) {
		return paymentService.cancel(payment).getErrors();
	}

	// function
	@RequestMapping(value = "/payment/authorize/cancel", method = RequestMethod.POST)
	public @ResponseBody List<String> authorizeCancel(@RequestBody Payment payment) {
		return paymentService.cancel(payment).getErrors();
	}

}
