package com.montran.internsa.bankapp.controller;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.entity.Balance;
import com.montran.internsa.bankapp.entity.api.AccountModel;
import com.montran.internsa.bankapp.service.AccountService;
import com.montran.internsa.bankapp.service.BalanceService;
import com.montran.internsa.bankapp.service.PaymentService;

/**
 * 
 * @author Marius Supuran
 *
 */
@RestController
public class AccountController {
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private AccountService accountService;

	@Autowired
	private BalanceService balanceService;

	@Autowired
	private PaymentService paymentService;

	// function
	@RequestMapping(value = "/account/view", method = RequestMethod.GET)
	public @ResponseBody List<AccountModel> view() {
		List<AccountModel> AccountModel=accountService.getAccounts().stream().map(x->modelMapper.map(x, AccountModel.class)).collect(Collectors.toList());;
		 return AccountModel;
		
	}

	// function
	@RequestMapping(value = "/account/pending", method = RequestMethod.GET)
	public @ResponseBody List<Account> viewPending() {
		return accountService.getPendingAccounts();
	}

	// function
	@RequestMapping(value = "/account/viewone", method = RequestMethod.GET)
	public @ResponseBody AccountModel getAccount(@RequestParam("id") long id) {
		AccountModel aux=this.modelMapper.map(accountService.findById(id), AccountModel.class);
		return aux;
	}

	// function
	@RequestMapping(value = "/account/overview", method = RequestMethod.GET)
	public @ResponseBody String getAccountOverview(@RequestBody Account account) throws JSONException {
		// get balance object
		Balance balance = balanceService.findBalanceForAccount(account);

		// create json object and fill fields
		JSONObject jsonString = new JSONObject();
		jsonString.put("Available balance", balance.getAvailable());
		jsonString.put("Pending balance", balance.getPending());
		jsonString.put("Projected balance", balance.getAvailable() + balance.getPending());
		jsonString.put("Transaction List", paymentService.getPayments(account));

		return jsonString.toString();
	}

	// function
	@RequestMapping(path = "/account/add", method = RequestMethod.POST)
	public @ResponseBody List<String> add(@RequestBody Account newAccount) {
		return accountService.saveAccount(newAccount).getErrors();
	}

	// function
	@RequestMapping(path = "/account/update", method = RequestMethod.POST)
	public @ResponseBody List<String> update(@RequestBody Account newAccount) {
		return accountService.modifyAccount(newAccount).getErrors();
	}

	// function
	@RequestMapping(path = "/account/delete", method = RequestMethod.POST)
	public @ResponseBody List<String> delete(@RequestBody Account newAccount) {
		return accountService.deleteAccount(newAccount).getErrors();
	}

	// function
	@RequestMapping(path = "/account/approve", method = RequestMethod.POST)
	public @ResponseBody List<String> approve(@RequestBody Account newAccount) {
		return accountService.approveAccount(newAccount).getErrors();
	}

	// function
	@RequestMapping(path = "/account/reject", method = RequestMethod.POST)
	public @ResponseBody List<String> reject(@RequestBody Account newAccount) {
		return accountService.reject(newAccount).getErrors();
	}

	// approved
	@RequestMapping(path = "/getAccountTypes", method = RequestMethod.GET)
	public @ResponseBody List<String> getAccountTypes() {
		return accountService.getAccountTypes();
	}

	// approved
	@RequestMapping(path = "/getAccountCurrencies", method = RequestMethod.GET)
	public @ResponseBody List<String> getAccountCurrencies() {
		List<String> all= accountService.getCurrencyList();
		return all;
	}

	// approved
	@RequestMapping(path = "/getAccountFinancialStatuses", method = RequestMethod.GET)
	public @ResponseBody List<String> getAccountFinancialStatuses() {
		return accountService.getFinancialStatusTypes();

	}

	// approved
	@RequestMapping(value = "/account/model", method = RequestMethod.GET)
	public @ResponseBody String getAccountModel() throws JSONException {
		JSONObject jsonString = new JSONObject();
		for (Field aux : AccountModel.class.getDeclaredFields()) {
			jsonString.put(aux.getName(), "");
		}
		String json = jsonString.toString();
		System.out.println(json);
		return json;

	}

}
