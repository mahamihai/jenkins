package com.montran.internsa.bankapp.controller;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.json.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.montran.internsa.bankapp.entity.User;
import com.montran.internsa.bankapp.entity.api.UserApi;
import com.montran.internsa.bankapp.result.Result;
import com.montran.internsa.bankapp.service.UserService;
import com.montran.internsa.bankapp.service.XMLService;

/**
 * 
 * @author Radu Nechiti
 * 
 */
@RestController
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private XMLService xMLService;

	// function
	@GetMapping("/user/view")
	public List<UserApi> getAllUsers() {
		List<User> users = userService.findAll();
		return users.stream().map(user -> modelMapper.map(user, UserApi.class)).collect(Collectors.toList());

	}

	// function
	@RequestMapping(value = "/user/viewone", method = RequestMethod.GET)
	public UserApi getUserById(@RequestParam("id") int id) {
		User user = userService.getUserById(id).get();
		return modelMapper.map(user, UserApi.class);
	}

	// approved
	@GetMapping("/user/current")
	public User getCurrentUser() {
		User user = userService.getCurrentUser();
		user.setPassword("");
		return user;
	}

	// function
	@RequestMapping(value = "/user/add", method = RequestMethod.POST)
	public @ResponseBody List<String> add(@RequestBody User user) {
		Result result = userService.create(user);
		return result.getErrors();
	}

	// function
	@RequestMapping(value = "/user/update", method = RequestMethod.POST)
	public @ResponseBody List<String> modify(@RequestBody User user) {
		Result result = userService.update(user);
		return result.getErrors();
	}

	// function
	@RequestMapping(value = "/user/delete", method = RequestMethod.POST)
	public @ResponseBody List<String> delete(@RequestBody User user) {
		Result result = userService.delete(user);
		return result.getErrors();
	}

	// function
	@RequestMapping(value = "/user/approve", method = RequestMethod.POST)
	public @ResponseBody List<String> approve(@RequestBody User user) {
		Result result = userService.approve(user);
		return result.getErrors();
	}

	// function
	@RequestMapping(value = "/user/reject", method = RequestMethod.POST)
	public @ResponseBody List<String> reject(@RequestBody User user) {
		Result result = userService.reject(user);
		return result.getErrors();
	}

	// function
	@GetMapping("/user/pending")
	public List<User> getPending() {
		return userService.getAllPendingUser();
	}

	@RequestMapping(value = "/user/model", method = RequestMethod.GET)
	public @ResponseBody String getAccountModel() throws JSONException {
		JSONObject jsonString = new JSONObject();
		for (Field aux : UserApi.class.getDeclaredFields()) {
			jsonString.put(aux.getName(), "");
		}
		String json = jsonString.toString();
		System.out.println(json);
		return json;

	}

	// approved
	@GetMapping("/user/current/urls")
	public String getCurrentUserUrls() {
		User user = userService.getCurrentUser();
		List<String> functions = Arrays.asList(user.getProfile().getFunctions().split(";"));

		Map<String, String> functionsMap = xMLService.parseFunctions("functions.xml", "forward");
		
		Map<String, String> map = new HashMap<>();

		for (String f : functions) {
			for (Entry<String, String> e : functionsMap.entrySet()) {
				if (e.getValue().equals(f)) {
					map.put(e.getKey(), e.getValue());
				}
			}
		}
		
		Map<String, String> result = new HashMap<>();
		
		Map<String, String> functionalitiesMap = xMLService.parseFunctions("functionalities.xml", "forward");
		
		
		for(Entry<String, String> mEntry: map.entrySet()) {
			for(Entry<String, String> f: functionalitiesMap.entrySet()) {
				if(mEntry.getKey().equals(f.getKey())) {
					result.put(mEntry.getKey(), f.getValue());
				}
			}
		}
		

		return new JSONObject(result).toString();
	}

}