package com.montran.internsa.bankapp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.entity.Balance;
import com.montran.internsa.bankapp.repository.BalanceRepository;

/**
 * 
 * @author Radu Nechiti
 *
 */
@Service
public class BalanceServiceImplementation implements BalanceService {

	@Autowired
	private BalanceRepository balanceRepository;

	@Override
	public List<Balance> findAll() {
		return balanceRepository.findAll();
	}

	@Override
	public List<Balance> findAllBalanceByAccount(Account account) {
		return balanceRepository.findAllBalanceByAccount(account);
	}

	@Override
	public List<Balance> findBalanceByAccountAndBetweenDates(Account account, Date startDate, Date endDate) {

		List<Balance> list = this.findAllBalanceByAccount(account);
		List<Balance> finalList = new ArrayList<Balance>();
		// before java 8
		/*
		 * if(startDate!=null && endDate!=null) for(Balance balance: list)
		 * if(balance.getTimestamp().after(startDate) &&
		 * balance.getTimestamp().before(endDate)) finalList.add(balance);
		 */
		if (startDate != null && endDate != null)
			finalList = list.stream().filter(x -> x.getTimestamp().after(startDate) && x.getTimestamp().before(endDate))
					.collect(Collectors.toList());
		return finalList;

	}

	/**
	 * @author Marius Supuran
	 *
	 */
	@Override
	public Balance findBalanceForAccount(Account account) {
		return balanceRepository.findFirstByAccountOrderByIdDesc(account);
	}

	@Override
	public void create(Account account) {
		Balance balance = new Balance(new Date(), 0, 0, account);
		balanceRepository.save(balance);
	}
	@Override
	public void refreshBalance(Account account,long amount) {
		Balance balance = new Balance(new Date(), amount, 0, account);
		balanceRepository.save(balance);
	}


	@Override
	public void verifyBalance(Account debit, Account credit, double value) {
		// get old balances
		Balance debitBalance = balanceRepository.findFirstByAccountOrderByIdDesc(debit);
		Balance creditBalance = balanceRepository.findFirstByAccountOrderByIdDesc(credit);

		// create and update debit balance
		Balance newDebitBalance = new Balance(debitBalance);
		newDebitBalance.setPending(newDebitBalance.getPending() - value);

		// create and update credit balance
		Balance newCreditBalance = new Balance(creditBalance);
		newCreditBalance.setPending(newCreditBalance.getPending() + value);

		// save in db
		balanceRepository.save(newCreditBalance);
		balanceRepository.save(newDebitBalance);
	}

	@Override
	public void completeBalance(Account debit, Account credit, double value) {
		// get old balances
		Balance debitBalance = balanceRepository.findFirstByAccountOrderByIdDesc(debit);
		Balance creditBalance = balanceRepository.findFirstByAccountOrderByIdDesc(credit);

		// create and update debit balance
		Balance newDebitBalance = new Balance(debitBalance);
		newDebitBalance.setAvailable(newDebitBalance.getAvailable() - value);
		newDebitBalance.setPending(newDebitBalance.getPending() + value);

		// create and update credit balance
		Balance newCreditBalance = new Balance(creditBalance);
		newCreditBalance.setAvailable(newCreditBalance.getAvailable() + value);
		newCreditBalance.setPending(newCreditBalance.getPending() - value);

		// save in db
		balanceRepository.save(newCreditBalance);
		balanceRepository.save(newDebitBalance);
	}

	@Override
	public void cancelBalance(Account debit, Account credit, double value) {
		// get old balances
		Balance debitBalance = balanceRepository.findFirstByAccountOrderByIdDesc(debit);
		Balance creditBalance = balanceRepository.findFirstByAccountOrderByIdDesc(credit);

		// create and update debit balance
		Balance newDebitBalance = new Balance(debitBalance);
		newDebitBalance.setPending(newDebitBalance.getPending() + value);

		// create and update credit balance
		Balance newCreditBalance = new Balance(creditBalance);
		newCreditBalance.setPending(newCreditBalance.getPending() - value);

		// save in db
		balanceRepository.save(newCreditBalance);
		balanceRepository.save(newDebitBalance);
	}

}
