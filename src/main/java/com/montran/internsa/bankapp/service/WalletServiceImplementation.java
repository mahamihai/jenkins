package com.montran.internsa.bankapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.SerializationUtils;
import org.web3j.crypto.Credentials;

import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.entity.Payment;
import com.montran.internsa.bankapp.entity.Wallet;
import com.montran.internsa.bankapp.repository.WalletRepository;
@Service
public class WalletServiceImplementation implements WalletService {
	
	EthHandling icoHandler=new EthHandling();
	@Autowired
	private WalletRepository walletRepository;
	@Override
	public Wallet getWallet(long id)
	{
		return walletRepository.getWalletById(id);
	}
	public boolean isCryptoAccount(Account account)
	{
		return this.icoHandler.isCryptoCurrency(account.getCurrency());
	}
	public long getWalletBalance(Wallet wall)
	{
		long available=this.icoHandler.getBalance(wall.getWalletCredentials());
		return available;
	}
	@Override
	public String addWallet(Account account,String password)
	{
		if(this.isCryptoAccount(account))
		{
		Credentials newCredentials=this.icoHandler.generateNewWallet(password);
		Wallet newWallet=new Wallet(account.getCurrency(),account);
		newWallet.storeByteKeys(newCredentials);
		account.setWallet(newWallet);
		
		
		
		
		}
		return "Success";
	}
	@Override
	public String makePayment(Payment pay) {
		Wallet debitWallet=pay.getDebitAccount().getWallet();
		if(this.getWalletBalance(debitWallet)<pay.getAmount())
		{
			return "Inssuficient currency available at the moment is the account";
		}
		// TODO Auto-generated method stub
		if(this.isCryptoAccount(pay.getCreditAccount()) && this.isCryptoAccount(pay.getDebitAccount()))
		{
		Credentials debit=pay.getDebitAccount().getWallet().getWalletCredentials();
		Credentials credit=pay.getCreditAccount().getWallet().getWalletCredentials();
		String result=this.icoHandler.transferMoney(debit, credit, (int)pay.getAmount());
		
		return result;

		}
		else
		{
			System.out.println("Account are not crypto accounts");
			return "Not crypto accounts";
		}
		
		
		
	}

}
