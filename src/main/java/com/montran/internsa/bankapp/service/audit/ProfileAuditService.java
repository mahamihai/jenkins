package com.montran.internsa.bankapp.service.audit;

import com.montran.internsa.bankapp.entity.Profile;
import com.montran.internsa.bankapp.entity.audit.ProfileAudit;

/**
 * 
 * @author Corina Cordea
 * 
 */
public interface ProfileAuditService {

	void createAudit(String action, Profile profile);

	ProfileAudit findLastByProfileName(String name);
}
