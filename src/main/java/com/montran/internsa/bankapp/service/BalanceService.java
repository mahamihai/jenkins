package com.montran.internsa.bankapp.service;

import java.util.Date;
import java.util.List;

import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.entity.Balance;

public interface BalanceService {
	List<Balance> findAll();

	List<Balance> findBalanceByAccountAndBetweenDates(Account account, Date startDate, Date endDate);

	List<Balance> findAllBalanceByAccount(Account account);

	void create(Account account);

	Balance findBalanceForAccount(Account account);

	void verifyBalance(Account debit, Account credit, double value);

	void completeBalance(Account debit, Account credit, double value);

	void cancelBalance(Account debit, Account credit, double value);
	
	public void refreshBalance(Account account,long amount) ;

}
