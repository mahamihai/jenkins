package com.montran.internsa.bankapp.service.audit;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.montran.internsa.bankapp.constant.Constants.Status;
import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.entity.User;
import com.montran.internsa.bankapp.entity.audit.AccountAudit;
import com.montran.internsa.bankapp.repository.UserRepository;
import com.montran.internsa.bankapp.repository.audit.AccountAuditRepository;

/**
 * 
 * @author Marius Supuran
 *
 */
@Service
public class AccountAuditServiceImplementation implements AccountAuditService {

	private AccountAuditRepository accountAuditRepository;
	private UserRepository userRepository;

	@Autowired
	public AccountAuditServiceImplementation(AccountAuditRepository accountAuditRepository,
			UserRepository userRepository) {
		this.accountAuditRepository = accountAuditRepository;
		this.userRepository = userRepository;
	}

	@Override
	public void create(Account account, String action) {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = userRepository.getUserByUsernameAndStatus(username, Status.ACTIVE);
		AccountAudit audit = new AccountAudit(new Date(), action, account, user);
		accountAuditRepository.save(audit);
	}

	@Override
	public AccountAudit getLatest(Account account) {
		return accountAuditRepository.findFirst1AccountAuditByAccountOrderByTimestampDesc(account);
	}

	@Override
	public AccountAudit getLatestByNumber(String number) {
		return accountAuditRepository.findFirst1AccountAuditByAccountNumberOrderByTimestampDesc(number);
	}

}
