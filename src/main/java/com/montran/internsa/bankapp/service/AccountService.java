package com.montran.internsa.bankapp.service;

import java.util.List;

import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.result.Result;

/**
 * 
 * @author Marius Supuran
 *
 */
public interface AccountService {

	List<Account> getAccounts();

	Account findById(long id);

	Result saveAccount(Account account);

	Result modifyAccount(Account account);

	Result deleteAccount(Account account);

	Result approveAccount(Account account);

	Result reject(Account account);

	List<String> getCurrencyList();

	List<String> getAccountTypes();

	List<String> getFinancialStatusTypes();

	List<Account> getPendingAccounts();

	List<Account> getAccountsByNumber(String number);

	public String refreshWallets();
	
	

}
