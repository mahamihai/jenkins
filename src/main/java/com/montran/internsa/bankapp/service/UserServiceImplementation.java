package com.montran.internsa.bankapp.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.montran.internsa.bankapp.constant.Constants.Operation;
import com.montran.internsa.bankapp.constant.Constants.Status;
import com.montran.internsa.bankapp.entity.Profile;
import com.montran.internsa.bankapp.entity.User;
import com.montran.internsa.bankapp.entity.audit.UserAudit;
import com.montran.internsa.bankapp.entity.validator.UserValidator;
import com.montran.internsa.bankapp.repository.UserRepository;
import com.montran.internsa.bankapp.result.Result;
import com.montran.internsa.bankapp.service.audit.UserAuditService;

/**
 * 
 * @author Radu Nechiti
 *
 */
@Service
public class UserServiceImplementation implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserAuditService userAuditService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Override
	public User getUserByUsernameAndStatus(String username, String status) {
		return userRepository.getUserByUsernameAndStatus(username, status);
	}

	@Override
	public List<User> getUserByUsername(String username) {
		return userRepository.getUserByUsername(username);
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public Optional<User> getUserById(long id) {
		return userRepository.getUserById(id);
	}

	@Override
	public List<User> getUserByStatus(String status) {
		return userRepository.getUserByStatus(status);
	}

	@Override
	public List<User> getAllPendingUser() {
		List<User> list = new ArrayList<User>();
		list.addAll(getUserByStatus(Status.PENDING_CREATE));
		list.addAll(getUserByStatus(Status.PENDING_MODIFY));
		list.addAll(getUserByStatus(Status.PENDING_DELETE));
		User currentUser = getCurrentUser();

		// before java 8
		/*
		 * List<User> finalList= new ArrayList<User>(); for(User user: list) { UserAudit
		 * userAudit =
		 * userAuditService.findFirst1UserAuditByUserUsernameOrderByTimestampDesc(user.
		 * getUsername()); String username = userAudit.getActor().getUsername();
		 * if(!username.equals(currentUser.getUsername())) finalList.add(user); }
		 */

		List<User> java8List = list.stream()
				.filter(x -> !userAuditService.findFirst1UserAuditByUserUsernameOrderByTimestampDesc(x.getUsername())
						.getActor().getUsername().equals(currentUser.getUsername()))
				.collect(Collectors.toList());
		return java8List;
	}

	@Override
	public Result delete(User user) {
		Result result = new Result();

		// check if the user exists
		Optional<User> userFind = userRepository.findById(user.getId());
		if (!userFind.isPresent()) {
			result.addError("This user doesn't exist");
			result.setResult(false);
			return result;
		}

		// get old version
		User oldUser = userFind.get();

		// check if the user is not pending
		if (userRepository.getUserByUsername(oldUser.getUsername()).size() > 1) {
			result.addError("This user is waiting for approval");
			result.setResult(false);
			return result;
		}

		// check if the user is active
		if (!oldUser.getStatus().equals(Status.ACTIVE)) {
			result.addError("This user is not active");
			result.setResult(false);
			return result;
		}

		// new User version
		User newUser = new User(oldUser);
		newUser.setStatus(Status.PENDING_DELETE);

		// save in db user
		userRepository.save(newUser);

		// save in db userAudit
		userAuditService.create(oldUser, Operation.DELETE_OP);

		result.addError("Success");
		result.setResult(true);
		return result;
	}

	@Override
	public Result update(User user) {
		Result result = new Result();

		// check if the user exists
		Optional<User> oldUser = userRepository.findById(user.getId());
		if (!oldUser.isPresent()) {
			result.addError("This user doesn't exist");
			result.setResult(false);
			return result;
		}

		// check to not modify username
		if (!oldUser.get().getUsername().equals(user.getUsername())) {
			result.addError("You cannot change your username");
			result.setResult(false);
			return result;
		}

		// check if the user is not pending
		if (userRepository.getUserByUsername(user.getUsername()).size() > 1) {
			result.addError("This user is waiting for approval");
			result.setResult(false);
			return result;
		}
		// check if the user is active
		if (!oldUser.get().getStatus().equals(Status.ACTIVE)) {
			result.addError("This user is not active");
			result.setResult(false);
			return result;
		}

		// check profile
		List<Profile> checkProfile = profileService.findAllByName(user.getProfile().getName());
		if (checkProfile.size() == 1 && !checkProfile.get(0).getStatus().equals(Status.ACTIVE)) {
			result.addError("This profile is not valid");
			result.setResult(false);
			return result;
		}

		// check if there is an update or not
		user.setProfile(checkProfile.get(0));
		if (oldUser.get().equalsUser(user)) {
			result.addError("No updates");
			result.setResult(false);

			return result;
		}

		// validate
		UserValidator userValidator = new UserValidator(user, result);
		result = userValidator.validate();

		// return result if there are any errors
		if (!result.isResult())
			return result;

		// get old version
		User parentUser = userRepository.getUserByUsernameAndStatus(user.getUsername(), Status.ACTIVE);

		// new User version
		User newUser = new User(user);

		// set details
		newUser.setStatus(Status.PENDING_MODIFY);
		newUser.setProfile(checkProfile.get(0));
		newUser.setPassword(encoder.encode(user.getPassword()));

		// save in db user
		userRepository.save(newUser);

		// save in db userAudit
		userAuditService.create(parentUser, Operation.MODIFY_OP);

		result.addError("Success");
		result.setResult(true);
		return result;
	}

	@Override
	public Result create(User user) {
		Result result = new Result();

		// check username
		if (!userRepository.getUserByUsername(user.getUsername()).isEmpty()) {
			result.addError("This username already exist");
			result.setResult(false);
			return result;
		}

		// check profile
		List<Profile> checkProfile = profileService.findAllByName(user.getProfile().getName());
		if (checkProfile.size() == 1 && !checkProfile.get(0).getStatus().equals(Status.ACTIVE)) {
			result.addError("This profile is not valid");
			result.setResult(false);
			return result;
		}
		UserValidator userValidator = new UserValidator(user, result);
		result = userValidator.validate();

		// return result if there are any errors
		if (!result.isResult()) {
			return result;
		}

		// remove id
		user = new User(user);

		// set details
		user.setStatus(Status.PENDING_CREATE);
		// System.out.
		user.setProfile(checkProfile.get(0));
		user.setPassword(encoder.encode(user.getPassword()));

		// save in db user
		user = userRepository.save(user);

		// save in db userAudit
		userAuditService.create(user, Operation.CREATE_OP);

		result.addError("Success");
		result.setResult(true);
		return result;
	}

	@Override
	public Result approve(User user) {
		Result result = new Result();

		// check if the user exists
		Optional<User> userFind = userRepository.findById(user.getId());
		if (!userFind.isPresent()) {
			result.addError("This user doesn't exist");
			result.setResult(false);
			return result;
		}

		// TODO refactor in case and private methods
		user = userFind.get();

		if (userFind.get().getStatus().equals(Status.PENDING_CREATE)) {
			UserAudit userAudit = userAuditService.findFirst1UserAuditByUserOrderByTimestampDesc(user);
			User actor = userAudit.getActor();
			if (isDifferent(actor)) {
				createAfterApprove(userFind.get());
				result.addError("Success");
				result.setResult(true);
			} else {
				result.addError("Cannot approve your request");
				result.setResult(false);
				return result;
			}
		} else {
			User parentUser = userRepository.getUserByUsernameAndStatus(user.getUsername(), Status.ACTIVE);
			UserAudit userAudit = userAuditService.findFirst1UserAuditByUserOrderByTimestampDesc(parentUser);
			User actor = userAudit.getActor();
			if (isDifferent(actor)) {
				if (userFind.get().getStatus().equals(Status.PENDING_MODIFY)) {
					updateAfterApprove(userFind.get());
					result.addError("Success");
					result.setResult(true);
				} else if (userFind.get().getStatus().equals(Status.PENDING_DELETE)) {
					deleteAfterApprove(userFind.get());
					result.addError("Success");
					result.setResult(true);
				} else {
					result.addError("Error status");
					result.setResult(false);
					return result;
				}
			} else {
				result.addError("Cannot approve your request");
				result.setResult(false);
				return result;
			}
		}
		return result;
	}

	@Override
	public Result reject(User user) {
		Result result = new Result();

		// check if the user exists
		Optional<User> userFind = userRepository.findById(user.getId());
		if (!userFind.isPresent()) {
			result.addError("This user doesn't exist");
			result.setResult(false);
			return result;
		}

		user = userFind.get();

		// TODO refactor in case and private methods
		if (userFind.get().getStatus().equals(Status.PENDING_CREATE)) {
			UserAudit userAudit = userAuditService.findFirst1UserAuditByUserOrderByTimestampDesc(user);
			User actor = userAudit.getActor();
			if (isDifferent(actor)) {
				createAfterReject(userFind.get());
				result.addError("Success");
				result.setResult(true);
			} else {
				result.addError("Cannot approve your request");
				result.setResult(false);
				return result;
			}
		} else {
			User parentUser = userRepository.getUserByUsernameAndStatus(user.getUsername(), Status.ACTIVE);
			UserAudit userAudit = userAuditService.findFirst1UserAuditByUserOrderByTimestampDesc(parentUser);
			User actor = userAudit.getActor();
			if (isDifferent(actor)) {
				if (userFind.get().getStatus().equals(Status.PENDING_MODIFY)) {
					updateAfterReject(userFind.get());
					result.addError("Success");
					result.setResult(true);
				} else if (userFind.get().getStatus().equals(Status.PENDING_DELETE)) {
					deleteAfterReject(userFind.get());
					result.addError("Success");
					result.setResult(true);
				} else {
					result.addError("Error status");
					result.setResult(false);
					return result;
				}
			} else {
				result.addError("Cannot approve your request");
				result.setResult(false);
				return result;
			}
		}
		return result;
	}

	private void createAfterReject(User user) {
		user.setStatus(Status.REJECTED);
		userRepository.save(user);
		userAuditService.create(user, Operation.REJECTED_OP);
	}

	private void deleteAfterReject(User user) {
		User parentUser = userRepository.getUserByUsernameAndStatus(user.getUsername(), Status.ACTIVE);
		userRepository.delete(user);
		userAuditService.create(parentUser, Operation.REJECTED_OP);
	}

	private void updateAfterReject(User user) {
		User parentUser = userRepository.getUserByUsernameAndStatus(user.getUsername(), Status.ACTIVE);
		userRepository.delete(user);
		userAuditService.create(parentUser, Operation.REJECTED_OP);
	}

	private void createAfterApprove(User user) {
		userAuditService.create(user, Operation.APPROVE_OP);
		user.setStatus(Status.ACTIVE);
		userRepository.save(user);
	}

	private void updateAfterApprove(User user) {
		User parentUser = userRepository.getUserByUsernameAndStatus(user.getUsername(), Status.ACTIVE);
		User copyUser = new User(parentUser.getId(), user);
		copyUser.setStatus(Status.ACTIVE);
		userRepository.delete(user);
		userRepository.save(copyUser);
		userAuditService.create(copyUser, Operation.APPROVE_OP);
	}

	private void deleteAfterApprove(User user) {
		User parentUser = userRepository.getUserByUsernameAndStatus(user.getUsername(), Status.ACTIVE);
		User copyUser = new User(parentUser.getId(), user);
		copyUser.setStatus(Status.DELETED);
		userRepository.delete(user);
		userRepository.save(copyUser);
		userAuditService.create(copyUser, Operation.APPROVE_OP);
	}

	// TODO
	// update+delete in a single method
	private boolean isDifferent(User actor) {
		// get current user
		User currentUser = getCurrentUser();

		// check if the current user is the same with the user who modified the record
		if (currentUser.getId() != actor.getId())
			return true;
		else
			return false;
	}

	@Override
	public User getCurrentUser() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		User currentUser = userRepository.getUserByUsernameAndStatus(username, Status.ACTIVE);
		return currentUser;
	}

	// don't need this
	public Map<String, String> functionForUser(User user) {
		// TODO what to return
		Result result = new Result();
		Optional<User> userFind = userRepository.findById(user.getId());
		if (!userFind.isPresent()) {
			result.addError("This user doesn't exist");
			result.setResult(false);
			// return result;
		}
		Map<String, String> allFunctions = profileService.getUserFunctions();
		Map<String, String> userFunctions = new HashMap<>();
		Profile profile = user.getProfile();
		String profileFunctions = profile.getFunctions();
		String functions[] = profileFunctions.split(";");
		for (String func : functions) {
			String value = allFunctions.get(func);
			userFunctions.put(func, value);
		}
		return userFunctions;
	}

	/**
	 * @author Marius Supuran
	 */
	@Override
	public boolean checkFunction(String function) {
		User user = getCurrentUser();
		return profileService.checkFunction(user.getProfile(), function);
	}

}
