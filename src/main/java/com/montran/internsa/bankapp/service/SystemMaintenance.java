package com.montran.internsa.bankapp.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SystemMaintenance {

    private static final Logger log = LoggerFactory.getLogger(SystemMaintenance.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    @Autowired
    AccountService accountService;
    @Scheduled(fixedRate = 600000)
    public void refreshWalletsInDb() {
    	log.info("STarted to refresh the wallets");
        this.accountService.refreshWallets();
    	log.info("Finished refreshing");

    }
}