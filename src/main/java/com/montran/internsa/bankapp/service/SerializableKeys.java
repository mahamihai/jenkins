package com.montran.internsa.bankapp.service;

import java.io.Serializable;
import java.math.BigInteger;

import org.web3j.crypto.Credentials;
import org.web3j.crypto.ECKeyPair;

public class SerializableKeys  implements Serializable {
	
	 private final BigInteger privateKey;
	   private final BigInteger publicKey;
	
	public ECKeyPair getKeyPair()
	{
		return new ECKeyPair(this.privateKey, this.publicKey);
	}
	public SerializableKeys(ECKeyPair keys)
	{
		this.privateKey=keys.getPrivateKey();
		this.publicKey=keys.getPublicKey();
		System.out.println(keys.toString());
	
	}
   

}
