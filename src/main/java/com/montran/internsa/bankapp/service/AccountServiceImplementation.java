package com.montran.internsa.bankapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.montran.internsa.bankapp.constant.Constants.Operation;
import com.montran.internsa.bankapp.constant.Constants.Status;
import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.entity.Payment;
import com.montran.internsa.bankapp.entity.Wallet;
import com.montran.internsa.bankapp.entity.audit.AccountAudit;
import com.montran.internsa.bankapp.entity.validator.AccountValidator;
import com.montran.internsa.bankapp.repository.AccountRepository;
import com.montran.internsa.bankapp.result.Result;
import com.montran.internsa.bankapp.service.audit.AccountAuditService;

/**
 * 
 * @author Marius Supuran
 *
 */
@Service
public class AccountServiceImplementation implements AccountService {

	private AccountRepository accountRepository;
	private AccountAuditService accountAuditService;
	private XMLService xMLService;
	private BalanceService balanceService;
	private PaymentService paymentService;
	private WalletService walletService;
	@Autowired
	public AccountServiceImplementation(AccountRepository accountRepository, AccountAuditService accountAuditService,
			XMLService xMLService, BalanceService balanceService, PaymentService paymentService,WalletService walletService) {
		this.accountRepository = accountRepository;
		this.accountAuditService = accountAuditService;
		this.xMLService = xMLService;
		this.balanceService = balanceService;
		this.paymentService = paymentService;
		this.walletService=walletService;
	}

	@Override
	public List<Account> getAccounts() {
		return accountRepository.findAll();
	}

	@Override
	public Account findById(long id) {
		return accountRepository.getOne(id);
	}

	@Override
	public Result saveAccount(Account account) {
		// create result object
		Result result = new Result();

		// do validation
		AccountValidator accountValidator = new AccountValidator(account, getAccountTypes(), getCurrencyList(),
				getFinancialStatusTypes(), result);
		result = accountValidator.validate();
		if (!result.isResult()) {
			return result;
		}

		// remove id just in case
		account = new Account(account);
							
		// check if number is unique
		if (!accountRepository.findByNumber(account.getNumber()).isEmpty()) {
			result.addError("Cannot add account with same number as existing account");
			result.setResult(false);
			return result;
		}

		// set status to pending approval
		account.setStatus(Status.PENDING_CREATE);
		//check to see if it need a wallet
		this.walletService.addWallet(account, "12345678");
		// save in db
		account = accountRepository.save(account);
		System.out.println("Succesfully saved the account with the address "+account.getWallet().getWalletCredentials().getAddress());
		result.addError("Success");
		result.setResult(true);

		// audit
		accountAuditService.create(account, Operation.CREATE_OP);

		// return result
		return result;
	}

	@Override
	public Result modifyAccount(Account account) {
		// create result object
		Result result = new Result();

		// do validation
		AccountValidator accountValidator = new AccountValidator(account, getAccountTypes(), getCurrencyList(),
				getFinancialStatusTypes(), result);
		result = accountValidator.validate();
		if (!result.isResult()) {
			return result;
		}

		// check if an older version with same id exists and is active
		Optional<Account> optional = accountRepository.findByIdAndStatus(account.getId(), Status.ACTIVE);
		if (!optional.isPresent()) {
			result.addError("No account with given id exists");
			result.setResult(false);
			return result;
		}

		// check if same number
		Account oldVersion = optional.get();
		if (!oldVersion.getNumber().equals(account.getNumber())) {
			result.addError("Cannot modify account number");
			result.setResult(false);
			return result;
		}

		// check if same currency
		if (!oldVersion.getCurrency().equals(account.getCurrency())) {
			result.addError("Cannot modify account currency");
			result.setResult(false);
			return result;
		}

		// check if not already awaiting approval
		if (accountRepository.findByNumber(account.getNumber()).size() > 1) {
			result.addError("Already modified, awaiting approval");
			result.setResult(false);
			return result;
		}

		// check if something is different
		if (oldVersion.equalsContent(account)) {
			result.addError("Nothing changed");
			result.setResult(false);
			return result;
		}

		// set status to pending approval
		Account newAccount = new Account(account);
		newAccount.setStatus(Status.PENDING_MODIFY);

		// save in db
		accountRepository.save(newAccount);
		result.addError("Success");
		result.setResult(true);

		// audit
		accountAuditService.create(oldVersion, Operation.MODIFY_OP);

		// return result
		return result;
	}

	@Override
	public Result deleteAccount(Account account) {
		// create result object
		Result result = new Result();

		// check if id is present and active
		Optional<Account> optional = accountRepository.findByIdAndStatus(account.getId(), Status.ACTIVE);
		if (!optional.isPresent()) {
			result.addError("No such account");
			result.setResult(false);
			return result;
		}

		Account oldAccount = optional.get();

		// check if not already deleted waiting for approval
		if (accountRepository.findByNumber(oldAccount.getNumber()).size() > 1) {
			result.addError("Already deleted, awaiting approval");
			result.setResult(false);
			return result;
		}

		// check if it has pending payments
		List<Payment> payments = paymentService.getPayments(oldAccount).stream()
				.filter(p -> !(p.getStatus().equals(Status.COMPLETED) || p.getStatus().equals(Status.CANCELLED)))
				.collect(Collectors.toList());
		if (payments.size() > 0) {
			result.addError("Cannot delete, inbound payments present");
			result.setResult(false);
			return result;
		}

		// set status to pending approval
		account = accountRepository.findById(account.getId()).get();
		Account newAccount = new Account(account);
		newAccount.setStatus(Status.PENDING_DELETE);

		// save in db
		accountRepository.save(newAccount);
		result.addError("Success");
		result.setResult(true);

		// audit
		accountAuditService.create(oldAccount, Operation.DELETE_OP);

		// return result
		return result;
	}

	@Override
	public Result approveAccount(Account account) {
		// create result object
		Result result = new Result();

		// check if id exists
		Optional<Account> optional = accountRepository.findById(account.getId());
		if (!optional.isPresent()) {
			result.addError("No such account!");
			result.setResult(false);
			return result;
		}

		// get both versions
		Account duplicate = optional.get();
		switch (duplicate.getStatus()) {

		case Status.PENDING_CREATE:
			return approveCreate(duplicate, result);

		case Status.PENDING_MODIFY:
			return approveModify(duplicate, result);

		case Status.PENDING_DELETE:
			return approveDelete(duplicate, result);

		default:
			result.addError("Invalid status for approval");
			result.setResult(false);
			break;
		}

		return result;
	}

	@Override
	public Result reject(Account account) {
		// create result object
		Result result = new Result();

		// check if id exists
		Optional<Account> optional = accountRepository.findById(account.getId());
		if (!optional.isPresent()) {
			result.addError("No such account!");
			result.setResult(false);
			return result;
		}

		// get both versions
		Account duplicate = accountRepository.findById(account.getId()).get();
		switch (duplicate.getStatus()) {

		case Status.PENDING_CREATE:
			return rejectCreate(duplicate, result);

		case Status.PENDING_MODIFY:
			return rejectModify(duplicate, result);

		case Status.PENDING_DELETE:
			return rejectDelete(duplicate, result);

		default:
			result.addError("Invalid status for rejection");
			result.setResult(false);
			break;
		}

		return result;
	}

	private boolean isDifferentUser(Account account) {
		AccountAudit latest = accountAuditService.getLatest(account);
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		return !latest.getActor().getUsername().equals(username);
	}

	private Result approveCreate(Account account, Result result) {
		// check different user
		if (!isDifferentUser(account)) {
			result.addError("Cannot approve own requests");
			result.setResult(false);
			return result;
		}

		// modify status
		account = accountRepository.findById(account.getId()).get();
		account.setStatus(Status.ACTIVE);
		account = accountRepository.save(account);

		// create inital balance
		balanceService.create(account);

		// audit
		accountAuditService.create(account, Operation.APPROVE_OP);

		// set result
		result.setResult(true);
		result.addError("Success");
		return result;
	}

	private Result approveModify(Account account, Result result) {
		// get other account
		Account oldAccount = accountRepository.findByNumberAndStatusOrderById(account.getNumber(), Status.ACTIVE)
				.get(0);

		// check different user
		if (!isDifferentUser(oldAccount)) {
			result.addError("Cannot approve own requests!");
			result.setResult(false);
			return result;
		}

		// delete duplicate
		accountRepository.delete(account);

		// replace with modified version
		Account newAccount = new Account(account);
		newAccount.setId(oldAccount.getId());
		newAccount.setStatus(Status.ACTIVE);
		accountRepository.save(newAccount);

		// audit
		accountAuditService.create(newAccount, Operation.APPROVE_OP);

		// set result
		result.setResult(true);
		result.addError("Success");
		return result;
	}

	private Result approveDelete(Account account, Result result) {
		// get other account
		Account oldAccount = accountRepository.findByNumberAndStatusOrderById(account.getNumber(), Status.ACTIVE)
				.get(0);

		// check different user
		if (!isDifferentUser(oldAccount)) {
			result.addError("Cannot approve own requests!");
			result.setResult(false);
			return result;
		}

		// delete duplicate
		accountRepository.delete(account);

		// replace with modified version
		oldAccount.setStatus(Status.DELETED);
		accountRepository.save(oldAccount);

		// audit
		accountAuditService.create(oldAccount, Operation.APPROVE_OP);

		// set result
		result.setResult(true);
		result.addError("Success");
		return result;
	}

	private Result rejectCreate(Account account, Result result) {
		// check different user
		if (!isDifferentUser(account)) {
			result.addError("Cannot reject own requests!");
			result.setResult(false);
			return result;
		}

		// modify status
		account = accountRepository.findById(account.getId()).get();
		account.setStatus(Status.REJECTED);
		accountRepository.save(account);

		// audit
		accountAuditService.create(account, Operation.REJECTED_OP);

		// set result
		result.setResult(true);
		result.addError("Success");
		return result;
	}

	private Result rejectModify(Account account, Result result) {
		// get other account
		Account oldAccount = accountRepository.findByNumberAndStatusOrderById(account.getNumber(), Status.ACTIVE)
				.get(0);

		// check different user
		if (!isDifferentUser(oldAccount)) {
			result.addError("Cannot reject own requests!");
			result.setResult(false);
			return result;
		}

		// delete duplicate
		accountRepository.delete(account);

		// audit
		accountAuditService.create(oldAccount, Operation.REJECTED_OP);

		// set result
		result.setResult(true);
		result.addError("Success");
		return result;
	}
	public String refreshWallets()
	{
		List<Account> allAccounts=this.accountRepository.findByStatus(Status.ACTIVE);
		for(Account t:allAccounts)
		{
			if(this.walletService.isCryptoAccount(t))
			{
				Wallet wall=t.getWallet();
				long amount=this.walletService.getWalletBalance(wall);
				if(amount!=-1)
				{
					System.out.println("Refreshed the value for address:"+wall.getWalletCredentials().getAddress()+" with value:"+amount)                                           ;
				this.balanceService.refreshBalance(t, amount);
				}
				
			}
		}
		return "Success";
	}
	private Result rejectDelete(Account account, Result result) {
		// get other account
		Account oldAccount = accountRepository.findByNumberAndStatusOrderById(account.getNumber(), Status.ACTIVE)
				.get(0);

		// check different user
		if (!isDifferentUser(oldAccount)) {
			result.addError("Cannot reject own requests!");
			result.setResult(false);
			return result;
		}

		// delete duplicate
		accountRepository.delete(account);
		// audit
		accountAuditService.create(oldAccount, Operation.REJECTED_OP);

		// set result
		result.setResult(true);
		result.addError("Success");
		return result;
	}

	@Override
	public List<String> getCurrencyList() {
		return xMLService.parse("currency.xml");
	}

	@Override
	public List<String> getAccountTypes() {
		return xMLService.parse("account_type.xml");
	}

	@Override
	public List<String> getFinancialStatusTypes() {
		return xMLService.parse("financial_status.xml");
	}

	// TODO only return for current user
	@Override
	public List<Account> getPendingAccounts() {
		List<Account> accounts = new ArrayList<>();
		accounts.addAll(accountRepository.findByStatus(Status.PENDING_CREATE));
		accounts.addAll(accountRepository.findByStatus(Status.PENDING_MODIFY));
		accounts.addAll(accountRepository.findByStatus(Status.PENDING_DELETE));

		accounts = accounts.stream()
				.filter(a -> !accountAuditService.getLatestByNumber(a.getNumber()).getActor().getUsername()
						.equals(SecurityContextHolder.getContext().getAuthentication().getName()))
				.collect(Collectors.toList());

		return accounts;
	}

	@Override
	public List<Account> getAccountsByNumber(String number) {
		return accountRepository.findByNumber(number);
	}

}