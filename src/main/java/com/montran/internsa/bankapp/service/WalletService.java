package com.montran.internsa.bankapp.service;

import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.entity.Payment;
import com.montran.internsa.bankapp.entity.Wallet;

public interface WalletService {
	 public Wallet getWallet(long id);
	
		public String addWallet(Account account,String password);
	
		public String makePayment(Payment pay) ;
		public boolean isCryptoAccount(Account account);
		public long getWalletBalance(Wallet wall);

			
		
	
}
