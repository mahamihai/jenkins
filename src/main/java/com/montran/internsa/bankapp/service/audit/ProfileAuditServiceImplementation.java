package com.montran.internsa.bankapp.service.audit;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.montran.internsa.bankapp.constant.Constants.Status;
import com.montran.internsa.bankapp.entity.Profile;
import com.montran.internsa.bankapp.entity.User;
import com.montran.internsa.bankapp.entity.audit.ProfileAudit;
import com.montran.internsa.bankapp.repository.UserRepository;
import com.montran.internsa.bankapp.repository.audit.ProfileAuditRepository;

/**
 * 
 * @author Corina Cordea
 *
 */
@Service
public class ProfileAuditServiceImplementation implements ProfileAuditService {

	@Autowired
	private ProfileAuditRepository profileAuditRepository;
	@Autowired
	private UserRepository userRepository;

	@Override
	public void createAudit(String action, Profile profile) {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = userRepository.getUserByUsernameAndStatus(username, Status.ACTIVE);
		ProfileAudit audit = new ProfileAudit(new Date(), action, profile, user);
		profileAuditRepository.save(audit);

	}

	@Override
	public ProfileAudit findLastByProfileName(String name) {
		return profileAuditRepository.findTop1ByProfileNameOrderByTimestampDesc(name);
	}

}
