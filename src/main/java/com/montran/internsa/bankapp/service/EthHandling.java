package com.montran.internsa.bankapp.service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.response.EthGetBalance;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Transfer;
import org.web3j.utils.Convert;

public class EthHandling {

	public  Web3j getBlockChainConnection()
	{
        Web3j web3j =Web3j.build(new HttpService("https://rinkeby.infura.io/v3/644ed07fa04f4ec0a35070826c9556e3"));
        return web3j;
	}
		
	String [] blockChainCurrencies= {"ETH"};
	List<String> icoMoney=new ArrayList<String>(Arrays.asList(blockChainCurrencies));
	public boolean isCryptoCurrency(String name)
	{
		return icoMoney.contains(name);
	}
	
	public Credentials generateNewWallet(String password)
	{
		String walletFileName="";
		try {
			walletFileName = WalletUtils.generateFullNewWalletFile(password,new File("/home/montran"));
		} catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidAlgorithmParameterException
				| CipherException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 Credentials credentials=null;
		try {
		
			credentials = WalletUtils.loadCredentials(
			        password,
			        "/home/montran/"+walletFileName);
		} catch (IOException | CipherException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Created the new account "+credentials.getAddress());
		 return credentials;
		
	}
	public long getBalance(Credentials creds)
	{
		Web3j web3 = this.getBlockChainConnection();
		// send asynchronous requests to get balance
		EthGetBalance ethGetBalance;
		try {
			ethGetBalance = web3
			  .ethGetBalance(creds.getAddress(), DefaultBlockParameterName.LATEST)
			  .sendAsync()
			  .get();
		} catch (InterruptedException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;

		}

		BigInteger wei = ethGetBalance.getBalance();
		return wei.longValue();
	}
	public String transferMoney(Credentials debitor, Credentials creditor,int amount)
	{
		
		
		Web3j web3 = this.getBlockChainConnection(); // defaults to http://localhost:8545/
	
		System.out.println("Transfering from "+debitor.getAddress()+" into the account "+creditor.getAddress());
		
		TransactionReceipt transactionReceipt=null;
		try {
			transactionReceipt = Transfer.sendFunds(
			        web3, debitor, creditor.getAddress(),
			        BigDecimal.valueOf(amount), Convert.Unit.WEI)
			        .send();
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Transaction failed "+e.toString();
		}
		
		 System.out.println("Transaction complete, view it at https://rinkeby.etherscan.io/tx/"
	                + transactionReceipt.getTransactionHash());

		return ("Transaction complete, view it at https://rinkeby.etherscan.io/tx/"
                + transactionReceipt.getTransactionHash());
		
		
		
		
		
		
		
		
		
	}
}
