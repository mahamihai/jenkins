package com.montran.internsa.bankapp.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.montran.internsa.bankapp.constant.Constants.FinancialStatus;
import com.montran.internsa.bankapp.constant.Constants.Operation;
import com.montran.internsa.bankapp.constant.Constants.Status;
import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.entity.Payment;
import com.montran.internsa.bankapp.entity.audit.PaymentAudit;
import com.montran.internsa.bankapp.entity.validator.PaymentValidator;
import com.montran.internsa.bankapp.repository.AccountRepository;
import com.montran.internsa.bankapp.repository.PaymentRepository;
import com.montran.internsa.bankapp.result.Result;
import com.montran.internsa.bankapp.service.audit.PaymentAuditService;

/**
 * 
 * @author Corina Cordea
 *
 */
@Service
public class PaymentServiceImplementation implements PaymentService {
	@Autowired
	PaymentRepository paymentRepository;
	@Autowired
	PaymentAuditService paymentAuditService;
	@Autowired
	BalanceService balanceService;
	@Autowired
	AccountRepository accountRepository;
	@Autowired
	XMLService xmlService;
	@Autowired
	WalletService walletService;
	public String nextState(Payment payment) {
		switch (payment.getStatus()) {
		case Status.VERIFY:
			// cancel if closed
			if (isClosed(payment)) {
				return Status.CANCELLED;
			} else if (payment.getAmount() > getPaymentLimit()) {
				return Status.APPROVE;
			} else if (isBlocked(payment)) {
				return Status.AUTHORIZE;
			} else {
				return Status.COMPLETED;
			}
		case Status.APPROVE:
			if (isBlocked(payment)) {
				return Status.AUTHORIZE;
			} else {
				return Status.COMPLETED;
			}
		}
		return null;
	}

	@Override
	public List<Payment> findAll() {
		return paymentRepository.findAll();
	}

	@Override
	public List<Payment> findAllByStatus(String status) {
		return paymentRepository.findByStatus(status);
	}

	@Override
	public Result add(Payment payment, String creditNumber, String debitNumber) {
		Result result = new Result();

		// check debit account != credit account
		if (creditNumber.equals(debitNumber)) {
			result.addError("Invalid accounts");
			result.setResult(false);
			return result;
		}

		// validate
		PaymentValidator paymentValidator = new PaymentValidator(payment, xmlService.parse("currency.xml"), result);
		result = paymentValidator.validate();
		if (!result.isResult()) {
			return result;
		}

		// verify currency
		String currency = payment.getCurrency();

		// verify debit account
		List<Account> debitAccount = accountRepository.findByNumber(debitNumber);
		result = validateAccount(debitAccount, "Debit", currency);
		if (!result.isResult()) {
			return result;
		}
		payment.setDebitAccount(debitAccount.get(0));

		// verify credit account
		List<Account> creditAccount = accountRepository.findByNumber(creditNumber);
		result = validateAccount(creditAccount, "Credit", currency);
		if (!result.isResult()) {
			return result;
		}
		payment.setCreditAccount(creditAccount.get(0));

		// generate reference
		payment = new Payment(payment);
		String uuid = UUID.randomUUID().toString().replace("-", "");
		payment.setReference(uuid);

		// set status and time stamp
		payment.setTimestamp(new Date());
		payment.setStatus(Status.VERIFY);
		// save
		payment = paymentRepository.save(payment);
		// create audit
		paymentAuditService.create(payment, Operation.CREATE_OP);
		result.addError("Success");
		result.setResult(true);
		return result;
	}

	private Result validateAccount(List<Account> account, String type, String currency) {
		Result result = new Result();
		// verify that account exists
		if (account.size() == 0) {
			result.addError(type + " account does not exist");
			result.setResult(false);
			return result;
		}
		// verify account not already involved in another operation
		if (account.size() > 1) {
			result.addError(type + " account involved in another operation");
			result.setResult(false);
			return result;
		}
		// verify status active
		if (account.size() == 1 && !account.get(0).getStatus().equals(Status.ACTIVE)) {
			result.addError(type + " account is not active");
			result.setResult(false);
			return result;
		}
		// verify account currency
		if (!(account.get(0).getCurrency().equals(currency))) {
			result.addError(type + " account currency not same as payment currency");
			result.setResult(false);
			return result;
		}
		result.setResult(true);
		return result;
	}

	@Override
	@Transactional
	public Result verify(Payment payment) {
		Result result = new Result();
		// verify payment exists
		Optional<Payment> storedPayment = paymentRepository.findById(payment.getId());
		if (!(storedPayment.isPresent() && storedPayment.get().getStatus().equals(Status.VERIFY))) {
			result.addError("Invalid payment");
			result.setResult(false);
			return result;
		}
		//public boolean makePayment()
		// verify different user
		PaymentAudit audit = paymentAuditService.findLastByPaymentId(payment.getId());
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		if (username.equals(audit.getActor().getUsername())) {
			result.addError("Can not verify this payment");
			result.setResult(false);
			return result;
		}
		// verify amount
		if (storedPayment.get().getAmount() != payment.getAmount()) {
			result.addError("Wrong amount");
			result.setResult(false);
			return result;
		}
		payment = storedPayment.get();

		// find next state
		String status = nextState(payment);

		// update balance --pending
		balanceService.verifyBalance(payment.getDebitAccount(), payment.getCreditAccount(), payment.getAmount());
		// if complete update balance --available and pending
		String transferMessage="";
		if (status.equals(Status.COMPLETED)) {
			transferMessage=this.walletService.makePayment(payment);
			balanceService.completeBalance(payment.getDebitAccount(), payment.getCreditAccount(), payment.getAmount());
		} 
		
		
		if (status.equals(Status.CANCELLED)) {
			balanceService.cancelBalance(payment.getDebitAccount(), payment.getCreditAccount(), payment.getAmount());
		}

		// set status and save
		payment.setStatus(status);
		paymentRepository.save(payment);
		// create account
		paymentAuditService.create(payment, Operation.VERIFY);
		result.addError("Success "+transferMessage);
		result.setResult(true);
		return result;
	}
	private boolean isCryptoPayment(Payment payment)
	{
		return this.walletService.isCryptoAccount(payment.getCreditAccount())
				&&
				this.walletService.isCryptoAccount(payment.getDebitAccount());
				
	}

	@Override
	@Transactional
	public Result approve(Payment payment) {
		Result result = new Result();
		// verify payment exists
		Optional<Payment> storedPayment = paymentRepository.findById(payment.getId());
		if (!(storedPayment.isPresent() && storedPayment.get().getStatus().equals(Status.APPROVE))) {
			result.addError("Invalid payment");
			result.setResult(false);
			return result;
		}
		payment = storedPayment.get();
		// verify different user
		PaymentAudit audit = paymentAuditService.findLastByPaymentId(payment.getId());
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		if (username.equals(audit.getActor().getUsername())) {
			result.addError("Can not approve this payment");
			result.setResult(false);
			return result;
		}
		// find next state
		String transferMessage="";

		String status = nextState(payment);
		// if complete update balance --available and pending
		if (
				status.equals(Status.AUTHORIZE)
				&&
				(this.isCryptoPayment(payment))
					
			) 
		{
			transferMessage=("Cannot go below 0 with this account");
			this.cancel(payment);
			status=Status.CANCELLED;
			result.addError(transferMessage);
			result.setResult(true);
			return result;
			
			
		}
		if (status.equals(Status.COMPLETED)) {
			transferMessage=this.walletService.makePayment(payment);
			balanceService.completeBalance(payment.getDebitAccount(), payment.getCreditAccount(), payment.getAmount());
		}
		
		// set status and save
		payment.setStatus(status);
		paymentRepository.save(payment);
		// create account
		paymentAuditService.create(payment, Operation.APPROVE);
		result.addError(transferMessage);
		result.setResult(true);
		return result;
	}

	@Override
	@Transactional
	public Result authorize(Payment payment) {
		Result result = new Result();
		// verify payment exists
		Optional<Payment> storedPayment = paymentRepository.findById(payment.getId());
		if (!(storedPayment.isPresent() && storedPayment.get().getStatus().equals(Status.AUTHORIZE))) {
			result.addError("Invalid payment");
			result.setResult(false);
			return result;
		}
		payment = storedPayment.get();
		// verify different user
		PaymentAudit audit = paymentAuditService.findLastByPaymentId(payment.getId());
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		if (username.equals(audit.getActor().getUsername())) {
			result.addError("Can not authorize this payment");
			result.setResult(false);
			return result;
		}
		// update balance --available and pending
		balanceService.completeBalance(payment.getDebitAccount(), payment.getCreditAccount(), payment.getAmount());
		// set status and save
		payment.setStatus(Status.COMPLETED);
		paymentRepository.save(payment);
		// create account
		paymentAuditService.create(payment, Operation.AUTHORIZE);
		result.addError("Success");
		result.setResult(true);
		return result;
	}

	@Override
	@Transactional
	public Result cancel(Payment payment) {
		Result result = new Result();
		// verify payment exists
		Optional<Payment> storedPayment = paymentRepository.findById(payment.getId());
		if (!storedPayment.isPresent()) {
			result.addError("Payment does not exist");
			result.setResult(false);
			return result;
		}
		payment = storedPayment.get();
		// verify different user
		PaymentAudit audit = paymentAuditService.findLastByPaymentId(payment.getId());
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		if (username.equals(audit.getActor().getUsername())) {
			result.addError("Can not cancel this payment");
			result.setResult(false);
			return result;
		}
		// update balance --pending
		if (!payment.getStatus().equals(Status.VERIFY)) {
			balanceService.cancelBalance(payment.getDebitAccount(), payment.getCreditAccount(), payment.getAmount());
		}
		// set status and save
		payment.setStatus(Status.CANCELLED);
		paymentRepository.save(payment);
		// create audit
		paymentAuditService.create(payment, Operation.CANCEL);
		result.addError("Success");
		result.setResult(true);
		return result;
	}

	@Override
	public List<Payment> getPayments(Account account) {
		List<Payment> payments = paymentRepository.findAllByDebitAccount(account);
		payments.addAll(paymentRepository.findAllByCreditAccount(account));
		return payments;
	}

	@Override
	public Double getPaymentLimit() {
		Double paymentLimit = Double.valueOf(xmlService.parse("payment.xml").get(0));
		return paymentLimit;
	}

	@Override
	public Payment getPaymentById(long id) {
		return paymentRepository.getPaymentById(id);
	}

	/**
	 * 
	 * @author Marius Supuran
	 * 
	 */
	private boolean isBlocked(Payment payment) {
		return balanceService.findBalanceForAccount(payment.getDebitAccount()).getAvailable() < payment.getAmount()
				|| payment.getCreditAccount().getFinancialStatus().equals(FinancialStatus.BLOCKED)
				|| payment.getCreditAccount().getFinancialStatus().equals(FinancialStatus.BLOCK_CREDIT)
				|| payment.getDebitAccount().getFinancialStatus().equals(FinancialStatus.BLOCKED)
				|| payment.getDebitAccount().getFinancialStatus().equals(FinancialStatus.BLOCK_DEBIT);
	}

	private boolean isClosed(Payment payment) {
		return payment.getCreditAccount().getFinancialStatus().equals(FinancialStatus.CLOSED)
				|| payment.getDebitAccount().getFinancialStatus().equals(FinancialStatus.CLOSED);
	}
}
