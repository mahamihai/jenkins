package com.montran.internsa.bankapp.service.audit;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.montran.internsa.bankapp.entity.Payment;
import com.montran.internsa.bankapp.entity.User;
import com.montran.internsa.bankapp.entity.audit.PaymentAudit;
import com.montran.internsa.bankapp.repository.audit.PaymentAuditRepository;
import com.montran.internsa.bankapp.service.UserService;

/**
 * 
 * @author Radu Nechiti
 * 
 */
@Service
public class PaymentAuditServiceImplementation implements PaymentAuditService {

	@Autowired
	private PaymentAuditRepository paymentAuditRepository;

	@Autowired
	private UserService userService;

	@Override
	public void create(Payment payment, String action) {
		User actor = userService.getCurrentUser();
		Date date = new Date();
		PaymentAudit paymentAudit = new PaymentAudit(date, action, payment, actor);
		paymentAuditRepository.save(paymentAudit);

	}

	@Override
	public PaymentAudit findLastByPaymentId(long id) {
		return paymentAuditRepository.findTop1ByPaymentIdOrderByTimestampDesc(id);
	}
}