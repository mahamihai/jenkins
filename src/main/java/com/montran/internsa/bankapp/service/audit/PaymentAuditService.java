package com.montran.internsa.bankapp.service.audit;

import com.montran.internsa.bankapp.entity.Payment;
import com.montran.internsa.bankapp.entity.audit.PaymentAudit;

/**
 * 
 * @author Radu Nechiti
 *
 */

public interface PaymentAuditService {

	void create(Payment payment, String action);

	PaymentAudit findLastByPaymentId(long id);
}