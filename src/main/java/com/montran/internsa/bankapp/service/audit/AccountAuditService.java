package com.montran.internsa.bankapp.service.audit;

import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.entity.audit.AccountAudit;

/**
 * 
 * @author Marius Supuran
 *
 */
public interface AccountAuditService {

	void create(Account account, String action);

	AccountAudit getLatest(Account account);

	AccountAudit getLatestByNumber(String number);
}
