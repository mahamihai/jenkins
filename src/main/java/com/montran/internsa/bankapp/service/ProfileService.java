package com.montran.internsa.bankapp.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.montran.internsa.bankapp.entity.Profile;
import com.montran.internsa.bankapp.result.Result;

/**
 * @author Corina Cordea
 */
public interface ProfileService {
	List<Profile> findAll();

	List<Profile> findAllModifiable();

	List<Profile> findAllByStatus(String status);

	Profile findByNameAndStatus(String name, String status);

	List<Profile> findAllByName(String name);

	List<Profile> findAllPending();

	Optional<Profile> findById(long id);

	List<String> getFunctions();

	Map<String, String> getProfileFunctions();

	Result add(Profile profile);

	Result modify(Profile profile);

	Result delete(Profile profile);

	Result approve(Profile profile);

	Result reject(Profile profile);

	Map<String, String> getUserFunctions();

	Profile findByName(String name);

	/**
	 * @author Marius Supuran
	 */
	boolean checkFunction(Profile profile, String function);

}
