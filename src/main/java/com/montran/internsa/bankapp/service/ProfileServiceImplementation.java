package com.montran.internsa.bankapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.montran.internsa.bankapp.constant.Constants.Operation;
import com.montran.internsa.bankapp.constant.Constants.Status;
import com.montran.internsa.bankapp.entity.Profile;
import com.montran.internsa.bankapp.entity.audit.ProfileAudit;
import com.montran.internsa.bankapp.entity.validator.ProfileValidator;
import com.montran.internsa.bankapp.repository.ProfileRepository;
import com.montran.internsa.bankapp.result.Result;
import com.montran.internsa.bankapp.service.audit.ProfileAuditService;

/**
 * 
 * @author Corina Cordea
 * 
 */
@Service
public class ProfileServiceImplementation implements ProfileService {
	@Autowired
	private ProfileRepository profileRepository;
	@Autowired
	private ProfileAuditService profileAuditService;
	@Autowired
	private XMLService XMLService;

	@Override
	public List<Profile> findAll() {
		return profileRepository.findAll();
	}

	@Override
	public Optional<Profile> findById(long id) {
		return profileRepository.findById(id);
	}

	@Override
	public List<Profile> findAllByStatus(String status) {
		return profileRepository.findByStatus(status);
	}

	@Override
	public Profile findByNameAndStatus(String name, String status) {
		return profileRepository.findByNameAndStatus(name, status);
	}

	@Override
	public Profile findByName(String name) {
		return profileRepository.findByName(name).get(0);
	}

	@Override
	public List<Profile> findAllByName(String name) {
		return profileRepository.findByName(name);
	}

	@Override
	public List<Profile> findAllPending() {
		List<Profile> profilesPending = profileRepository.findByStatus(Status.PENDING_CREATE);
		profilesPending.addAll(profileRepository.findByStatus(Status.PENDING_MODIFY));
		profilesPending.addAll(profileRepository.findByStatus(Status.PENDING_DELETE));
		List<Profile> profilesPendingFiltered = new ArrayList<>();
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		for (Profile profile : profilesPending) {
			ProfileAudit audit = profileAuditService.findLastByProfileName(profile.getName());
			if (!username.equals(audit.getActor().getUsername())) {
				profilesPendingFiltered.add(profile);
			}
		}
		return profilesPendingFiltered;
	}

	@Override
	public List<Profile> findAllModifiable() {
		List<Profile> activeProfiles = findAllByStatus(Status.ACTIVE);
		List<Profile> modifiableProfiles = new ArrayList<>();
		for (Profile profile : activeProfiles) {
			if (profileRepository.findByName(profile.getName()).size() == 1) {
				modifiableProfiles.add(profile);
			}
		}
		return modifiableProfiles;
	}

	@Override
	public Result add(Profile profile) {
		Result result = new Result();

		// validate profile
		ProfileValidator validator = new ProfileValidator(profile, getFunctions(), result);
		result = validator.validate();
		if (!result.isResult()) {
			return result;
		}
		// verify name not already taken
		if (!profileRepository.findByName(profile.getName()).isEmpty()) {
			result.addError("Profile with this name already exists");
			result.setResult(false);
			return result;
		}
		// verify profile with same set of functions does not already exist
		if (!verifyListOfFunctions(profile)) {
			result.addError("Profile with same set of functions already exists");
			result.setResult(false);
			return result;
		}
		// set status and save
		profile = new Profile(profile);
		profile.setStatus(Status.PENDING_CREATE);
		profile.setFunctions(addView(profile.getFunctions()));
		profile = profileRepository.save(profile);
		// create audit
		profileAuditService.createAudit(Operation.CREATE_OP, profile);
		result.addError("Success");
		result.setResult(true);
		return result;
	}

	@Override
	public Result modify(Profile profile) {
		Result result = new Result();

		// validate profile
		ProfileValidator validator = new ProfileValidator(profile, getFunctions(), result);
		result = validator.validate();
		if (!result.isResult()) {
			return result;
		}
		List<Profile> dbProfile = profileRepository.findByName(profile.getName());
		// verify that profile exists
		if (dbProfile.size() == 0) {
			result.addError("Profile does not exist");
			result.setResult(false);
			return result;
		}
		// verify profile with same set of functions does not already exist
		if (!verifyListOfFunctions(profile)) {
			result.addError("Profile with same set of functions already exists");
			result.setResult(false);
			return result;
		}
		// verify profile not already involved in another operation
		if (dbProfile.size() > 1) {
			result.addError("Profile involved in another operation");
			result.setResult(false);
			return result;
		}
		// verify status active
		if (dbProfile.size() == 1 && !dbProfile.get(0).getStatus().equals(Status.ACTIVE)) {
			result.addError("Profile is not active");
			result.setResult(false);
			return result;
		}
		// insert profile with status pending modify
		Profile newProfile = new Profile(profile);
		newProfile.setFunctions(addView(profile.getFunctions()));
		newProfile.setStatus(Status.PENDING_MODIFY);
		profileRepository.save(newProfile);
		// create audit
		profileAuditService.createAudit(Operation.MODIFY_OP, profileRepository.findById(profile.getId()).get());
		result.addError("Success");
		result.setResult(true);

		return result;
	}

	@Override
	public Result delete(Profile profile) {
		Result result = new Result();

		Optional<Profile> dbProfile = profileRepository.findById(profile.getId());
		// verify that profile exists
		if (!dbProfile.isPresent()) {
			result.addError("Profile does not exist");
			result.setResult(false);
			return result;
		}

		profile = dbProfile.get();
		List<Profile> nameList = profileRepository.findByName(profile.getName());

		// verify profile not already involved in another operation
		if (nameList.size() > 1) {
			result.addError("Profile involved in another operation");
			result.setResult(false);
			return result;
		}
		// verify status active
		if (nameList.size() == 1 && !nameList.get(0).getStatus().equals(Status.ACTIVE)) {
			result.addError("Profile is not active");
			result.setResult(false);
			return result;
		}
		// insert profile with status pending delete
		profile = profileRepository.findById(profile.getId()).get();
		Profile newProfile = new Profile(profile);
		newProfile.setStatus(Status.PENDING_DELETE);
		profileRepository.save(newProfile);
		// create audit
		profileAuditService.createAudit(Operation.DELETE_OP, profile);
		result.addError("Success");
		result.setResult(true);

		return result;
	}

	@Override
	public Result approve(Profile profile) {
		Result result = new Result();

		// verify that profile exists
		Optional<Profile> dbProfile = profileRepository.findById(profile.getId());
		if (!dbProfile.isPresent()) {
			result.addError("Profile does not exist");
			result.setResult(false);
			return result;
		}
		profile = dbProfile.get();
		// verify user approving operation is different than the one requesting it
		ProfileAudit audit = profileAuditService.findLastByProfileName(profile.getName());
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		if (!username.equals(audit.getActor().getUsername())) {
			switch (profile.getStatus()) {
			case Status.PENDING_CREATE:
				approveAdd(profile);
				result.addError("Success");
				result.setResult(true);
				break;
			case Status.PENDING_MODIFY:
				approveModify(profile);
				result.addError("Success");
				result.setResult(true);
				break;
			case Status.PENDING_DELETE:
				approveDelete(profile);
				result.addError("Success");
				result.setResult(true);
				break;
			default:
				result.addError("Invalid status");
				result.setResult(false);
				break;
			}
		} else {
			result.addError("Cannot approve own requests!");
			result.setResult(false);
		}
		return result;
	}

	private void approveAdd(Profile profile) {
		// modify status
		profile.setStatus(Status.ACTIVE);
		profileRepository.save(profile);
		// create audit
		profileAuditService.createAudit(Operation.APPROVE_OP, profile);
	}

	private void approveModify(Profile profile) {
		// copy and save modifications
		Profile old = findByNameAndStatus(profile.getName(), Status.ACTIVE);
		old.setFunctions(profile.getFunctions());
		profileRepository.save(old);
		// delete duplicate
		profileRepository.delete(profile);
		// create audit
		profileAuditService.createAudit(Operation.APPROVE_OP, old);
	}

	private void approveDelete(Profile profile) {
		// modify status
		Profile old = findByNameAndStatus(profile.getName(), Status.ACTIVE);
		old.setStatus(Status.DELETED);
		profileRepository.save(old);
		// delete duplicate
		profileRepository.delete(profile);
		// create audit
		profileAuditService.createAudit(Operation.APPROVE_OP, old);
	}

	@Override
	public Result reject(Profile profile) {
		Result result = new Result();

		// verify that profile exists
		Optional<Profile> dbProfile = profileRepository.findById(profile.getId());
		if (!dbProfile.isPresent()) {
			result.addError("Profile does not exist");
			result.setResult(false);
			return result;
		}
		profile = dbProfile.get();
		// verify user rejecting operation is different than the one requesting it
		ProfileAudit audit = profileAuditService.findLastByProfileName(profile.getName());
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		if (!username.equals(audit.getActor().getUsername())) {
			switch (profile.getStatus()) {
			case Status.PENDING_CREATE:
				rejectAdd(profile);
				result.addError("Success");
				result.setResult(true);
				break;
			case Status.PENDING_MODIFY:
				rejectOperation(profile);
				result.addError("Success");
				result.setResult(true);
				break;
			case Status.PENDING_DELETE:
				rejectOperation(profile);
				result.addError("Success");
				result.setResult(true);
				break;
			default:
				result.addError("Invalid status");
				result.setResult(false);
				break;
			}
		} else {
			result.addError("Cannot approve own requests!");
			result.setResult(false);
		}
		return result;
	}

	private void rejectAdd(Profile profile) {
		// modify status
		profile.setStatus(Status.REJECTED);
		profileRepository.save(profile);
		// create audit
		profileAuditService.createAudit(Operation.REJECTED_OP, profile);
	}

	private void rejectOperation(Profile profile) {
		// delete duplicate
		Profile old = findByNameAndStatus(profile.getName(), Status.ACTIVE);
		profileRepository.delete(profile);
		// create audit
		profileAuditService.createAudit(Operation.REJECTED_OP, old);
	}

	private boolean verifyListOfFunctions(Profile newProfile) {
		List<Profile> profiles = findAll();
		String newProfileFunctions = newProfile.getFunctions();
		String newFunctions[] = newProfileFunctions.split(";");
		for (Profile profile : profiles) {
			String functions[] = profile.getFunctions().split(";");
			label: if (newFunctions.length == functions.length) {
				for (String function : functions) {
					if (!newProfileFunctions.contains(function)) {
						break label;
					}
				}
				return false;
			}
		}
		return true;
	}

	@Override
	public List<String> getFunctions() {
		List<String> functions = new ArrayList<>(getProfileFunctions().values());
		return functions;
	}

	@Override
	public Map<String, String> getProfileFunctions() {
		return XMLService.parseFunctions("functions.xml", "forward");
	}

	@Override
	public Map<String, String> getUserFunctions() {
		return XMLService.parseFunctions("functions.xml", "backward");
	}

	/**
	 * @author Marius Supuran
	 */
	@Override
	public boolean checkFunction(Profile profile, String function) {
		return profile.getFunctions().contains(function) && profile.getStatus().equals("ACTIVE");
	}

	private String addView(String functions) {
		if (functions.contains("user") && !functions.contains("View user")) {
			functions += "View user;";
		}

		if (functions.contains("profile") && !functions.contains("View profile")) {
			functions += "View profile;";
		}

		if (functions.contains("account") && !functions.contains("View account")) {
			functions += "View account;";
		}

		if (functions.contains("payment") && !functions.contains("View payment")) {
			functions += "View payment;";
		}

		if (functions.contains("Create user") && !functions.contains("View profile")) {
			functions += "View profile;";
		}

		if (functions.contains("Enter payment") && !functions.contains("View account")) {
			functions += "View account;";
		}

		if (functions.contains("View balance") && !functions.contains("View account")) {
			functions += "View account;";
		}

		return functions;
	}

}
