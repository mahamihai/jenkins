package com.montran.internsa.bankapp.service;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author Corina Cordea
 * 
 */
public interface XMLService {

	List<String> parse(String name);

	Map<String, String> parseFunctions(String name, String dir);

}
