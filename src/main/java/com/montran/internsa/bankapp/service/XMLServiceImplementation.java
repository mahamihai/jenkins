package com.montran.internsa.bankapp.service;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.springframework.stereotype.Service;

/**
 * 
 * @author Corina Cordea
 * 
 */
@Service
public class XMLServiceImplementation implements XMLService {
	@Override
	public List<String> parse(String name) {
		List<String> values = new ArrayList<>();
		try {
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			InputStream in = new FileInputStream(name);
			XMLStreamReader streamReader = inputFactory.createXMLStreamReader(in);
			streamReader.nextTag();
			streamReader.nextTag();

			while (streamReader.hasNext()) {
				if (streamReader.isStartElement()) {
					values.add(streamReader.getElementText());
				}
				streamReader.next();
			}
			return values;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Map<String, String> parseFunctions(String name, String dir) {
		Map<String, String> values = new HashMap<>();
		try {
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			InputStream in = new FileInputStream(name);
			XMLStreamReader streamReader = inputFactory.createXMLStreamReader(in);
			streamReader.nextTag();
			streamReader.nextTag();

			while (streamReader.hasNext()) {
				if (streamReader.isStartElement()) {
					if (dir.equals("forward")) {
						values.put(streamReader.getAttributeValue(0), streamReader.getElementText());
					} else {
						String url = streamReader.getAttributeValue(0);
						values.put(streamReader.getElementText(), url);
					}
				}
				streamReader.next();
			}
			return values;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
