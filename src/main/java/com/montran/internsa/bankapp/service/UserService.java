package com.montran.internsa.bankapp.service;

import java.util.List;
import java.util.Optional;

import com.montran.internsa.bankapp.entity.User;
import com.montran.internsa.bankapp.result.Result;

/**
 * 
 * @author Radu Nechiti
 * 
 */
public interface UserService {

	public List<User> getUserByUsername(String username);

	public Optional<User> getUserById(long id);

	public List<User> findAll();

	public Result delete(User user);

	public Result update(User user);

	public Result create(User user);

	public Result approve(User user);

	public Result reject(User user);

	public List<User> getUserByStatus(String status);

	public User getUserByUsernameAndStatus(String username, String status);

	public User getCurrentUser();

	public List<User> getAllPendingUser();

	/**
	 * @author Marius Supuran
	 */
	boolean checkFunction(String function);
}