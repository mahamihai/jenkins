package com.montran.internsa.bankapp.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.entity.Payment;
import com.montran.internsa.bankapp.result.Result;

@Service
public interface PaymentService {
	List<Payment> findAll();

	List<Payment> findAllByStatus(String status);

	Result add(Payment payment, String creditNumber, String debitNumber);

	Result verify(Payment payment);

	Result approve(Payment payment);

	Result authorize(Payment payment);

	Result cancel(Payment payment);

	List<Payment> getPayments(Account account);

	Double getPaymentLimit();

	Payment getPaymentById(long id);
}
