package com.montran.internsa.bankapp.service.audit;

import java.util.List;

import com.montran.internsa.bankapp.entity.User;
import com.montran.internsa.bankapp.entity.audit.UserAudit;

/**
 * 
 * @author Radu Nechiti
 *
 */
public interface UserAuditService {

	public List<UserAudit> findAll();

	public void create(User user, String action);

	public List<UserAudit> getUserAuditByUser(User user);

	public UserAudit findFirst1UserAuditByUserOrderByTimestampDesc(User user);

	public UserAudit findFirst1UserAuditByUserUsernameOrderByTimestampDesc(String username);
}
