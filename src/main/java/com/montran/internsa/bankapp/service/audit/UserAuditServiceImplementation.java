package com.montran.internsa.bankapp.service.audit;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.montran.internsa.bankapp.entity.User;
import com.montran.internsa.bankapp.entity.audit.UserAudit;
import com.montran.internsa.bankapp.repository.audit.UserAuditRepository;
import com.montran.internsa.bankapp.service.UserService;

/**
 * 
 * @author Radu Nechiti
 *
 */
@Service
public class UserAuditServiceImplementation implements UserAuditService {

	@Autowired
	private UserAuditRepository userAuditRepository;

	@Autowired
	private UserService userService;

	@Override
	public void create(User user, String action) {
		User actor = userService.getCurrentUser();
		Date date = new Date();
		UserAudit userAudit = new UserAudit(date, action, user, actor);
		userAuditRepository.save(userAudit);
	}

	@Override
	public List<UserAudit> findAll() {
		return userAuditRepository.findAll();
	}

	@Override
	public List<UserAudit> getUserAuditByUser(User user) {
		return userAuditRepository.getUserAuditByUser(user);
	}

	@Override
	public UserAudit findFirst1UserAuditByUserOrderByTimestampDesc(User user) {
		return userAuditRepository.findFirst1UserAuditByUserOrderByTimestampDesc(user);
	}

	@Override
	public UserAudit findFirst1UserAuditByUserUsernameOrderByTimestampDesc(String username) {
		return userAuditRepository.findFirst1UserAuditByUserUsernameOrderByTimestampDesc(username);
	}

}
