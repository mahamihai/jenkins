package com.montran.internsa.bankapp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.montran.internsa.bankapp.entity.User;

/***
 * 
 * @author Radu Nechiti
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {

	List<User> getUserByUsername(String username);

	Optional<User> getUserById(long id);

	Optional<User> getUserByUsernameAndPassword(String username, String password);

	User getUserByUsernameAndStatus(String username, String status);

	List<User> getUserByStatus(String status);
}
