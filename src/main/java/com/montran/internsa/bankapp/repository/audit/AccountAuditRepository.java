package com.montran.internsa.bankapp.repository.audit;

import org.springframework.data.jpa.repository.JpaRepository;

import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.entity.audit.AccountAudit;

/**
 * 
 * @author Marius Supuran
 *
 */
public interface AccountAuditRepository extends JpaRepository<AccountAudit, Long> {

	AccountAudit findFirst1AccountAuditByAccountOrderByTimestampDesc(Account account);

	AccountAudit findFirst1AccountAuditByAccountNumberOrderByTimestampDesc(String number);

}
