package com.montran.internsa.bankapp.repository.audit;

import org.springframework.data.jpa.repository.JpaRepository;

import com.montran.internsa.bankapp.entity.audit.ProfileAudit;

/**
 * 
 * @author Corina Cordea
 *
 */
public interface ProfileAuditRepository extends JpaRepository<ProfileAudit, Long> {

	public ProfileAudit findTop1ByProfileNameOrderByTimestampDesc(String name);
}
