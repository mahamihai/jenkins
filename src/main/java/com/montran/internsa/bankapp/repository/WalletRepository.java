package com.montran.internsa.bankapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.montran.internsa.bankapp.entity.Wallet;

public interface WalletRepository extends JpaRepository<Wallet, Long> {
	
	
	Wallet getWalletById(long id);
	

}
