package com.montran.internsa.bankapp.repository.audit;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.montran.internsa.bankapp.entity.User;
import com.montran.internsa.bankapp.entity.audit.UserAudit;

/**
 * 
 * @author Radu Nechiti
 *
 */
public interface UserAuditRepository extends JpaRepository<UserAudit, Long> {

	UserAudit findFirst1UserAuditByUserOrderByTimestampDesc(User user);

	List<UserAudit> getUserAuditByUser(User user);

	UserAudit findFirst1UserAuditByUserUsernameOrderByTimestampDesc(String username);
}
