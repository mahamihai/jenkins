package com.montran.internsa.bankapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.entity.Balance;

public interface BalanceRepository extends JpaRepository<Balance, Long> {

	Balance findFirstByOrderByIdDesc();

	Balance findFirstByAccountOrderByIdDesc(Account account);

	List<Balance> findAllBalanceByAccount(Account account);

}
