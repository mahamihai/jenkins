package com.montran.internsa.bankapp.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.montran.internsa.bankapp.entity.Account;

/**
 * 
 * @author Marius Supuran
 *
 */
public interface AccountRepository extends JpaRepository<Account, Long> {

	List<Account> findByNumberAndStatusOrderById(String number, String status);

	List<Account> findByNumber(String number);

	List<Account> findByStatus(String status);

	Optional<Account> findByIdAndStatus(Long id, String status);

}
