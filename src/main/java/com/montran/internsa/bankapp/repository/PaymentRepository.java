package com.montran.internsa.bankapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.entity.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Long> {

	List<Payment> findByStatus(String status);

	List<Payment> findAllByDebitAccount(Account account);

	List<Payment> findAllByCreditAccount(Account account);

	Payment getPaymentById(long id);

}
