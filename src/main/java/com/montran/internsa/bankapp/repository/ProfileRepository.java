package com.montran.internsa.bankapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.montran.internsa.bankapp.entity.Profile;

/**
 * 
 * @author Corina Cordea
 *
 */
public interface ProfileRepository extends JpaRepository<Profile, Long> {

	public List<Profile> findByStatus(String status);

	public Profile findByNameAndStatus(String name, String status);

	public List<Profile> findByName(String name);
}
