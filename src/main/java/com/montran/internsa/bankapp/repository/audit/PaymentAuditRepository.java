package com.montran.internsa.bankapp.repository.audit;

import org.springframework.data.jpa.repository.JpaRepository;

import com.montran.internsa.bankapp.entity.audit.PaymentAudit;

public interface PaymentAuditRepository extends JpaRepository<PaymentAudit, Long> {

	PaymentAudit findTop1ByPaymentIdOrderByTimestampDesc(long id);

}
