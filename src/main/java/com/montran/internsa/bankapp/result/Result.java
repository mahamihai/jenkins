package com.montran.internsa.bankapp.result;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Marius Supuran
 *
 */
public class Result {

	private boolean result;
	private List<String> errors;

	public Result() {
		errors = new ArrayList<>();
		result = false;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public void addError(String error) {
		errors.add(error);
	}

}
