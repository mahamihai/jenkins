package com.montran.internsa.bankapp.constant;

public final class Constants {

	public static final class Status {
		public static final String PENDING_CREATE = "PENDING-CREATE";
		public static final String PENDING_MODIFY = "PENDING-MODIFY";
		public static final String PENDING_DELETE = "PENDING-DELETE";
		public static final String DELETED = "DELETED";
		public static final String ACTIVE = "ACTIVE";
		public static final String REJECTED = "REJECTED";
		public static final String VERIFY = "VERIFY";
		public static final String APPROVE = "APPROVE";
		public static final String AUTHORIZE = "AUTHORIZE";
		public static final String COMPLETED = "COMPLETED";
		public static final String CANCELLED = "CANCELLED";
	}

	public static final class Operation {
		public static final String CREATE_OP = "CREATE";
		public static final String MODIFY_OP = "MODIFY";
		public static final String DELETE_OP = "DELETE";
		public static final String APPROVE_OP = "APPROVE";
		public static final String REJECTED_OP = "REJECT";
		public static final String VERIFY = "VERIFY";
		public static final String APPROVE = "APPROVE";
		public static final String AUTHORIZE = "AUTHORIZE";
		public static final String COMPLETE = "COMPLETE";
		public static final String CANCEL = "CANCEL";
	}

	public static final class FinancialStatus {
		public static final String ACTIVE = "ACTIVE";
		public static final String BLOCKED = "BLOCKED";
		public static final String BLOCK_DEBIT = "BLOCK_DEBIT";
		public static final String BLOCK_CREDIT = "BLOCK_CREDIT";
		public static final String CLOSED = "CLOSED";
	}
}
