package com.montran.internsa.bankapp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.montran.internsa.bankapp.interceptor.RightInterceptor;

/**
 * 
 * @author Marius Supuran
 *
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

	@Autowired
	private RightInterceptor rightInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(rightInterceptor);
	}

}
