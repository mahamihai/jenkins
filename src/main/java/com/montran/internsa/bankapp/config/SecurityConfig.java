package com.montran.internsa.bankapp.config;

import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private DataSource dataSource;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.jdbcAuthentication()
				.usersByUsernameQuery("select username, password, 1 from user where username=? and status='ACTIVE'")
				// no idea how to make this better but it works for now
				.authoritiesByUsernameQuery("select 1, 1 from user where username=?").dataSource(dataSource)
				.passwordEncoder(bCryptPasswordEncoder);

	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
	}

	@Bean
	public AuthenticationFailureHandler authenticationFailureHandler() {
		return new SimpleUrlAuthenticationFailureHandler();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/").permitAll().antMatchers("/login").permitAll().antMatchers("/logout")
				.permitAll().anyRequest().authenticated().and().formLogin().defaultSuccessUrl("/user/current", true)
				.failureHandler(authenticationFailureHandler()).usernameParameter("username")
				.passwordParameter("password").and().logout().logoutUrl("/logout").invalidateHttpSession(true)
				.deleteCookies("JSESSIONID")
				.logoutSuccessHandler((httpServletRequest, httpServletResponse, authentication) -> {
					httpServletResponse.setStatus(HttpServletResponse.SC_OK);
				}).and().csrf().disable();

		http.exceptionHandling().authenticationEntryPoint(new Http403ForbiddenEntryPoint());
	}

}
