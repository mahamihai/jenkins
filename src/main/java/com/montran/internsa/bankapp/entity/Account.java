package com.montran.internsa.bankapp.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author Marius Supuran
 *
 */
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Account {

	
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	

	private String number;

	public Account(long id, String number, String type, String name, String address1, String address2, String address3,
			String address4, String currency, String financialStatus, String status, Wallet wallet) {
		super();
		this.id = id;
		this.number = number;
		this.type = type;
		this.name = name;
		this.address1 = address1;
		this.address2 = address2;
		this.address3 = address3;
		this.address4 = address4;
		this.currency = currency;
		this.financialStatus = financialStatus;
		this.status = status;
		this.wallet = wallet;
	}

	public Wallet getWallet() {
		return wallet;
	}

	public void setWallet(Wallet wallet) {
		this.wallet = wallet;
	}

	private String type;
	private String name;
	private String address1;
	private String address2;
	private String address3;
	private String address4;
	private String currency;
	private String financialStatus;
	private String status;
	  @OneToOne(cascade=CascadeType.ALL,fetch = FetchType.EAGER,optional = true)
	   @JoinColumn(name = "wallet_id")
	   private Wallet wallet;
	   
	   
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress4() {
		return address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getFinancialStatus() {
		return financialStatus;
	}

	public void setFinancialStatus(String financialStatus) {
		this.financialStatus = financialStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Account(long id, String number, String type, String name, String address1, String address2, String address3,
			String address4, String currency, String financialStatus, String status) {
		this.id = id;
		this.number = number;
		this.type = type;
		this.name = name;
		this.address1 = address1;
		this.address2 = address2;
		this.address3 = address3;
		this.address4 = address4;
		this.currency = currency;
		this.financialStatus = financialStatus;
		this.status = status;
	}

	public Account() {

	}

	public Account(long id) {
		this.id = id;
	}

	public Account(Account account) {
		this.number = account.number;
		this.type = account.type;
		this.name = account.name;
		this.address1 = account.address1;
		this.address2 = account.address2;
		this.address3 = account.address3;
		this.address4 = account.address4;
		this.currency = account.currency;
		this.financialStatus = account.financialStatus;
		this.status = account.status;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", number=" + number + ", type=" + type + ", name=" + name + ", address1="
				+ address1 + ", address2=" + address2 + ", address3=" + address3 + ", address4=" + address4
				+ ", currency=" + currency + ", financialStatus=" + financialStatus + ", status=" + status 
				;
	}

	public boolean equalsContent(Account account) {
		return this.name.equals(account.name) && this.type.equals(account.type)
				&& this.address1.equals(account.address1) && this.address2 != null && account.address2 != null
				&& this.address2.equals(account.address2) && this.address3 != null && account.address3 != null
				&& this.address3.equals(account.address3) && this.address4 != null && account.address4 != null
				&& this.address4.equals(account.address4) && this.currency.equals(account.currency)
				&& this.financialStatus.equals(account.financialStatus);
	}

}
