package com.montran.internsa.bankapp.entity.validator;

import java.util.List;

import com.montran.internsa.bankapp.entity.Payment;
import com.montran.internsa.bankapp.result.Result;

public class PaymentValidator {

	private Payment payment;
	private List<String> currencies;
	private Result result;

	public PaymentValidator(Payment payment, List<String> currencies, Result result) {
		this.payment = payment;
		this.currencies = currencies;
		this.result = result;
	}

	public Result validate() {
		validateCurrency();
		validateAmount();

		if (result.getErrors().isEmpty()) {
			result.setResult(true);
		} else {
			result.setResult(false);
		}
		return result;
	}

	private void validateCurrency() {
		if (!currencies.contains(payment.getCurrency())) {
			result.addError("Invalid currency");
		}
	}

	private void validateAmount() {
		if (payment.getAmount() < 0) {
			result.addError("Invalid amount");
		}
	}

}
