package com.montran.internsa.bankapp.entity.validator;

import java.util.List;
import java.util.regex.Pattern;

import com.montran.internsa.bankapp.entity.Profile;
import com.montran.internsa.bankapp.result.Result;

/**
 * @author Corina Cordea
 */
public class ProfileValidator {
	private static final String PROFILE_NAME_PATTERN = "^[a-zA-Z]+(([ -][a-zA-Z ])?[a-zA-Z]*)*$";
	private Profile profile;
	private List<String> availableFunctions;
	private Result result;

	public ProfileValidator(Profile profile, List<String> availableFunctions, Result result) {
		super();
		this.profile = profile;
		this.availableFunctions = availableFunctions;
		this.result = result;
	}

	public Result validate() {
		if (profile.getName().isEmpty()) {
			result.addError("Name is empty");
		}
		if (profile.getFunctions().isEmpty()) {
			result.addError("Functions are empty");
		}
		Pattern pattern = Pattern.compile(PROFILE_NAME_PATTERN);
		if (!pattern.matcher(profile.getName()).matches()) {
			result.addError("Invalid profile name");
		}
		String profileFunctions = profile.getFunctions();
		String functions[] = profileFunctions.split(";");
		for (String function : functions) {
			if (!availableFunctions.contains(function)) {
				result.addError("Invalid function");
				return result;
			}
		}
		if (result.getErrors().isEmpty()) {
			result.setResult(true);
		} else {
			result.setResult(false);
		}
		return result;
	}

}
