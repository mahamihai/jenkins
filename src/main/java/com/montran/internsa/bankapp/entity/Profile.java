package com.montran.internsa.bankapp.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author Marius Supuran
 *
 */
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Profile {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	@Lob
	private String functions;
	private String status;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFunctions() {
		return functions;
	}

	public void setFunctions(String functions) {
		this.functions = functions;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Profile(long id, String name, String functions, String status) {
		this.id = id;
		this.name = name;
		this.functions = functions;
		this.status = status;
	}

	public Profile() {

	}

	public Profile(Profile profile) {
		this.name = profile.getName();
		this.functions = profile.getFunctions();
		this.status = profile.getStatus();
	}

	public Profile(long id) {
		this.id = id;
	}

}
