package com.montran.internsa.bankapp.entity.api;

import java.util.Date;

public class AccountApi {

	private long id;
	private Date date1;
	private Date date2;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDate1() {
		return date1;
	}

	public void setDate1(Date date1) {
		this.date1 = date1;
	}

	public Date getDate2() {
		return date2;
	}

	public void setDate2(Date date2) {
		this.date2 = date2;
	}

}
