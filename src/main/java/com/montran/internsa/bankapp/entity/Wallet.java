
	
	
	
	
	package com.montran.internsa.bankapp.entity;

	import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.util.SerializationUtils;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.ECKeyPair;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.montran.internsa.bankapp.service.SerializableKeys;

	/**
	 * 
	 * @author Marius Supuran
	 *
	 */
	@Entity
	@Table(name = "wallet")
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	public class Wallet {

		
		public Wallet(  String currency, Account account) {
			super();
			
			this.walletByteString = walletByteString;
			this.currency = currency;
			//this.account = account;
		}

		public Wallet() {
			// TODO Auto-generated constructor stub
		}

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long id;
	    public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public byte[] getWalletByteString() {
			return walletByteString;
		}

		public void setWalletByteString(byte[] walletByteString) {
			this.walletByteString = walletByteString;
		}

		public String getCurrency() {
			return currency;
		}

		public void setCurrency(String currency) {
			this.currency = currency;
		}

		
		public void storeByteKeys(Credentials credentials)
		{
			SerializableKeys serializableKeys=new SerializableKeys(credentials.getEcKeyPair());
			byte[] serializedWallet = SerializationUtils.serialize(serializableKeys);
			this.setWalletByteString(serializedWallet);
		}
		
		public Credentials getWalletCredentials()
		
		{
			byte[] byteKeys=this.getWalletByteString();
			SerializableKeys serializableKeys=(SerializableKeys) SerializationUtils.deserialize(byteKeys);;

			ECKeyPair keys= serializableKeys.getKeyPair();
			Credentials newCreds=Credentials.create(keys);
			return newCreds;
		}
		
		@Lob
	    private byte[] walletByteString;
		private String currency;
		
		 
		public Account getAccount() {
			return account;
		}

		public void setAccount(Account account) {
			this.account = account;
		}
		@OneToOne(fetch = FetchType.EAGER,mappedBy = "wallet")
		 private Account account;
		public Wallet(long id, byte[] walletByteString, String currency, Account account) {
			super();
			this.id = id;
			this.walletByteString = walletByteString;
			this.currency = currency;
			this.account = account;
		}
		 
		 
}
