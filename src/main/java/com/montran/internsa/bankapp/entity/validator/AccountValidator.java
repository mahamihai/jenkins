package com.montran.internsa.bankapp.entity.validator;

import java.util.List;
import java.util.regex.Pattern;

import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.result.Result;

/**
 * 
 * @author Marius Supuran
 *
 */
public class AccountValidator {

	private static final String NUMBER_REGEX = "[A-Z]{4}[0-9]{4}";
	private static final String NAME_REGEX = "^[\\p{L} .'-]+$";

	private Account account;
	private List<String> types;
	private List<String> currencies;
	private List<String> financialStatuses;
	private Result result;

	public AccountValidator(Account account, List<String> types, List<String> currencies,
			List<String> financialStatuses, Result result) {
		this.account = account;
		this.types = types;
		this.currencies = currencies;
		this.financialStatuses = financialStatuses;
		this.result = result;
	}

	public Result validate() {
		validateNumber();
		validateName();
		validateType();
		validateCurrency();
		validateFinancialStatus();
		validateAddress();

		if (result.getErrors().isEmpty()) {
			result.setResult(true);
		} else {
			result.setResult(false);
		}
		return result;
	}

	private void validateAddress() {
		if (account.getAddress1() == null || account.getAddress1().isEmpty()) {
			result.addError("No address");
		}
	}

	private void validateName() {
		if (!Pattern.compile(NAME_REGEX).matcher(account.getName()).matches()) {
			result.addError("Invalid name format");
		}
	}

	private void validateFinancialStatus() {
		if (!financialStatuses.contains(account.getFinancialStatus())) {
			result.addError("Invalid financial status");
		}
	}

	private void validateCurrency() {
		if (!currencies.contains(account.getCurrency())) {
			result.addError("Invalid currency");
		}
	}

	private void validateType() {
		if (!types.contains(account.getType())) {
			result.addError("Invalid type");
		}
	}

	private void validateNumber() {
		if (!Pattern.compile(NUMBER_REGEX).matcher(account.getNumber()).matches()) {
			result.addError("Invalid number format");
		}
	}

}
