package com.montran.internsa.bankapp.entity.audit;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.montran.internsa.bankapp.entity.Account;
import com.montran.internsa.bankapp.entity.User;

/**
 * 
 * @author Marius Supuran
 *
 */
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class AccountAudit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private Date timestamp;
	private String operation;
	@ManyToOne
	private Account account;

	// the user which modified the account
	@ManyToOne
	private User actor;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public User getActor() {
		return actor;
	}

	public void setActor(User actor) {
		this.actor = actor;
	}

	public AccountAudit(Date timestamp, String operation, Account account, User actor) {
		this.timestamp = timestamp;
		this.operation = operation;
		this.account = account;
		this.actor = actor;
	}

	public AccountAudit() {

	}

}
