package com.montran.internsa.bankapp.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author Corina Cordea
 *
 */
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Balance {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private Date timestamp;
	private double available;
	private double pending;
	@ManyToOne
	private Account account;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public double getAvailable() {
		return available;
	}

	public void setAvailable(double available) {
		this.available = available;
	}

	public double getPending() {
		return pending;
	}

	public void setPending(double pending) {
		this.pending = pending;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Balance() {

	}

	public Balance(long id, Date timestamp, double available, double pending, Account account) {
		this.id = id;
		this.timestamp = timestamp;
		this.available = available;
		this.pending = pending;
		this.account = account;
	}

	public Balance(Date timestamp, double available, double pending, Account account) {
		this.timestamp = timestamp;
		this.available = available;
		this.pending = pending;
		this.account = account;
	}

	public Balance(Balance balance) {
		this.timestamp = new Date();
		this.available = balance.available;
		this.pending = balance.pending;
		this.account = balance.account;
	}

	public Balance(long id) {
		this.id = id;
	}
}
