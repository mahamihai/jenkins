package com.montran.internsa.bankapp.entity.api;

import java.util.Date;

public class PaymentApi {

	private long id;
	private Date timestamp;
	private double amount;
	private String currency;
	private String status;
	private String reference;
	private long debitAccountId;
	private String debitAccountNumber;
	private long creditAccountId;
	private String creditAccountNumber;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public long getDebitAccountId() {
		return debitAccountId;
	}

	public void setDebitAccountId(long debitAccountId) {
		this.debitAccountId = debitAccountId;
	}

	public String getDebitAccountNumber() {
		return debitAccountNumber;
	}

	public void setDebitAccountNumber(String debitAccountNumber) {
		this.debitAccountNumber = debitAccountNumber;
	}

	public long getCreditAccountId() {
		return creditAccountId;
	}

	public void setCreditAccountId(long creditAccountId) {
		this.creditAccountId = creditAccountId;
	}

	public String getCreditAccountNumber() {
		return creditAccountNumber;
	}

	public void setCreditAccountNumber(String creditAccountNumber) {
		this.creditAccountNumber = creditAccountNumber;
	}

}
