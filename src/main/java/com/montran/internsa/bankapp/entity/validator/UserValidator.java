package com.montran.internsa.bankapp.entity.validator;

import java.util.regex.Pattern;

import com.montran.internsa.bankapp.entity.User;
import com.montran.internsa.bankapp.result.Result;

/**
 * 
 * @author Radu Nechiti
 *
 */
public class UserValidator {

	private User user;
	private Result result;
	private static final String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
	private static final String USERNAME_REGEX = "^[a-z0-9_-]{3,15}$";
	private static final String NAME_REGEX = "^[\\p{L} .'-]+$";
	private static final String PASSWORD_REGEX = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";

	public UserValidator(User user, Result result) {
		this.user = user;
		this.result = result;
	}

	public Result validate() {
		emailValidator();
		usernameValidator();
		nameValidator();
		validateAddress();
		passwordValidator();
		if (result.getErrors().isEmpty()) {
			result.setResult(true);
		} else {
			result.setResult(false);
		}
		return result;
	}

	// TODO
	// refactor
	private void emailValidator() {
		Pattern pattern = Pattern.compile(EMAIL_REGEX);
		if (!pattern.matcher(user.getEmail()).matches())
			result.addError("Invalid Email");
	}
	private void passwordValidator() {
		Pattern pattern = Pattern.compile(PASSWORD_REGEX);
		if (!pattern.matcher(user.getPassword()).matches())
			result.addError("Invalid Password");
	}

	private void usernameValidator() {
		Pattern pattern = Pattern.compile(USERNAME_REGEX);
		if (!pattern.matcher(user.getUsername()).matches())
			result.addError("Invalid Username");
	}

	private void nameValidator() {
		Pattern pattern = Pattern.compile(NAME_REGEX);
		if (!pattern.matcher(user.getName()).matches())
			result.addError("Invalid Name");
	}

	private void validateAddress() {
		if (user.getAddress1() == null || user.getAddress1().isEmpty()) {
			result.addError("No address");
		}
	}
}
