package com.montran.internsa.bankapp.entity.audit;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.montran.internsa.bankapp.entity.User;

/**
 * 
 * @author Marius Supuran
 *
 */
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class UserAudit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private Date timestamp;
	private String operation;
	// the user that was modified
	@ManyToOne
	private User user;
	// the user which modified the user
	@ManyToOne
	private User actor;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getActor() {
		return actor;
	}

	public void setActor(User actor) {
		this.actor = actor;
	}

	public UserAudit(Date timestamp, String operation, User user, User actor) {
		this.timestamp = timestamp;
		this.operation = operation;
		this.user = user;
		this.actor = actor;
	}

	public UserAudit() {

	}

}
