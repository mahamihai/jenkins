package com.montran.internsa.bankapp.entity.audit;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.montran.internsa.bankapp.entity.Payment;
import com.montran.internsa.bankapp.entity.User;

/**
 * 
 * @author Corina Cordea
 *
 */
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class PaymentAudit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private Date timestamp;
	private String operation;
	@ManyToOne
	private Payment payment;
	@ManyToOne
	private User actor;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public User getActor() {
		return actor;
	}

	public void setActor(User actor) {
		this.actor = actor;
	}

	public PaymentAudit() {

	}

	public PaymentAudit(long id, Date timestamp, String operation, Payment payment, User actor) {
		super();
		this.id = id;
		this.timestamp = timestamp;
		this.operation = operation;
		this.payment = payment;
		this.actor = actor;
	}

	public PaymentAudit(Date timestamp, String operation, Payment payment, User actor) {
		this.timestamp = timestamp;
		this.operation = operation;
		this.payment = payment;
		this.actor = actor;
	}

}
