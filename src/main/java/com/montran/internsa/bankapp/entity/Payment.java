package com.montran.internsa.bankapp.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author Corina Cordea
 *
 */
@Entity
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Payment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private Date timestamp;
	private double amount;
	private String currency;
	private String status;
	private String reference;
	@ManyToOne
	private Account debitAccount;
	@ManyToOne
	private Account creditAccount;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Account getDebitAccount() {
		return debitAccount;
	}

	public void setDebitAccount(Account debitAccount) {
		this.debitAccount = debitAccount;
	}

	public Account getCreditAccount() {
		return creditAccount;
	}

	public void setCreditAccount(Account creditAccount) {
		this.creditAccount = creditAccount;
	}

	public Payment() {

	}

	public Payment(long id, Date timestamp, double amount, String currency, String status, String reference,
			Account debitAccount, Account creditAccount) {
		super();
		this.id = id;
		this.timestamp = timestamp;
		this.amount = amount;
		this.currency = currency;
		this.status = status;
		this.reference = reference;
		this.debitAccount = debitAccount;
		this.creditAccount = creditAccount;
	}

	public Payment(Payment payment) {
		this.timestamp = payment.getTimestamp();
		this.amount = payment.getAmount();
		this.currency = payment.getCurrency();
		this.status = payment.getStatus();
		this.reference = payment.getReference();
		this.debitAccount = payment.getDebitAccount();
		this.creditAccount = payment.getCreditAccount();
	}

	public Payment(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Payment [id=" + id + ", timestamp=" + timestamp + ", amount=" + amount + ", currency=" + currency
				+ ", status=" + status + ", reference=" + reference + ", debitAccount=" + debitAccount
				+ ", creditAccount=" + creditAccount + "]";
	}

}