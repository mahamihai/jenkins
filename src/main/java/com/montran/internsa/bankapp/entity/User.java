package com.montran.internsa.bankapp.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * 
 * @author Marius Supuran
 *
 */
@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String username;
	private String password;
	private String name;
	private String email;
	private String address1;
	private String address2;
	private String address3;
	private String address4;
	@ManyToOne
	private Profile profile;
	private String status;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress4() {
		return address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User(long id, String username, String password, String name, String email, String address1, String address2,
			String address3, String address4, Profile profile, String status) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.name = name;
		this.email = email;
		this.address1 = address1;
		this.address2 = address2;
		this.address3 = address3;
		this.address4 = address4;
		this.profile = profile;
		this.status = status;
	}

	public User(User user) {
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.name = user.getName();
		this.email = user.getEmail();
		this.address1 = user.getAddress1();
		this.address2 = user.getAddress2();
		this.address3 = user.getAddress3();
		this.address4 = user.getAddress4();
		this.profile = user.getProfile();
		this.status = user.getStatus();
	}

	public User(long id, User user) {
		this.id = id;
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.name = user.getName();
		this.email = user.getEmail();
		this.address1 = user.getAddress1();
		this.address2 = user.getAddress2();
		this.address3 = user.getAddress3();
		this.address4 = user.getAddress4();
		this.profile = user.getProfile();
		this.status = user.getStatus();
	}

	public User() {

	}

	public User(long id) {
		this.id = id;
	}

	public boolean equalsUser(User user) {
		boolean equals;
		equals = this.getAddress1().equals(user.getAddress1()) && this.address2 != null && user.getAddress2() != null
				&& this.getAddress2().equals(user.getAddress2()) && this.address3 != null && user.getAddress3() != null
				&& this.getAddress3().equals(user.getAddress3()) && this.address4 != null && user.getAddress4() != null
				&& this.getAddress4().equals(user.getAddress4()) && this.getEmail().equals(user.getEmail())
				&& this.getName().equals(user.getName()) && this.getPassword().equals(user.getPassword())
				&& this.getUsername().equals(user.getUsername()) && this.getProfile().equals(user.getProfile());
		return equals;
	}

}
