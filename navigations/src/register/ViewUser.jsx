import React, { Component } from "react";
import {UserModifyControl} from '../usersModify/UsersModifyControl';
import { UserContext } from "../App";

export class ViewUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pending:false,
      userInfo: "",
      profiles: [],
      modify:false
    };
    this.getUserInfo();
    this.toggleModify=this.toggleModify.bind(this);
  }
  getFunctionUrl(name) {
    let functions = this.props.functions;
    console.log(JSON.stringify(functions));

    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {
        // console.log(functions[t].name+" "+name);

        if (functions[t].name == name) {
          return "http://localhost:8080" + functions[t].url;
        }
      }
    }
  }

  getUserInfo() {
    //console.log(JSON.stringify(this.props.functions));

    let queryString = this.props.history.location.state; //get passed id from history
    let id = parseInt(queryString["id"]);
    let url = this.getFunctionUrl("View one user");
    fetch(url + "?id=" + id, {
      method: "GET",
      mode: "cors",
      credentials: 'include',
    })
      .then(res => res.json())
      .then(
        result => {
          console.log("Received the user", JSON.stringify(result));
          this.setState({
            userInfo: result
          });
        },

        error => {
          console.log(error);
        }
      )
      .catch(err => {});
  }
  fetchRights() {
    return {
      option1: "Home",
      option3: "Users",
      option2: "Accounts"
    };
  }
  deleteUser(user, url) {
    console.log("Deleting user from " + url);
    fetch(url, {
      method: "POST",
      mode: "cors",
      credentials: 'include',
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(user)
    })
      .then(res => res.json())
      .then(
        result => {
          console.log(JSON.stringify(result));
        },
        error => {
          console.log(error);
        }
      )
      .catch(err => {});
    this.props.history.push("/users");
  }
  getButtons() {
    let functions = this.props.functions;
    let rendering = [];
    let backBtn = <button className="btn btn-success" onClick={this.toggleModify}>Back</button>
    for (var f in functions) {
      if (functions.hasOwnProperty(f)) {
        switch (functions[f].name) {
          case "Delete user": {
            let url = this.getFunctionUrl("Delete user");
            rendering.push(
              <button
                type="button"
                onClick={() => {
                  let user = this.state.userInfo;
                  user.profile = {
                    id: user.profileId,
                    name: user.profileName,
                  }
                  this.deleteUser(user, url);
                }}
                className="btn btn-danger "
              >
                Delete user
              </button>
            );
            break;
          }
          case "Modify user": {
            let url = this.getFunctionUrl("Modify user");
            
            rendering.push(
              <button
                type="button"
                onClick={() => {
                  this.toggleModify();
                }}
                className="btn btn-warning "
              >
                Modify user
              </button>

         );
            break;
          }
        }
      }
    }
    if(this.state.modify !=true)
      return (<div>{rendering}</div>);
    else
      return backBtn;
  }
  
  toggleModify()
  {
   console.log("toggled"+this.state.modify);
    
    this.setState({modify:!this.state.modify});
  }
  getUserForm(session) {
    if (this.state.modify != true) {
    
      return (
        this.state.accountInfo != "" && (
          <div>
            <div className="form-group row">
              <label htmlFor="nameBox" className="col-sm-2 col-form-label">
                Name
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  
                  className="form-control"
                  id="nameBox"
                  value={this.state.userInfo.name}
                />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="numberBox" className="col-sm-2 col-form-label">
                Username
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  aria-describedby="numberHelp"
                  id="numberBox"
                  value={this.state.userInfo.username}
                />
                <small id="numberHelp" className="form-text text-muted">
                  Your unique username
                </small>
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="nameBox" className="col-sm-2 col-form-label">
                Profile
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  
                  className="form-control"
                  id="profileBox"
                  value={this.state.userInfo.profileName}
                />
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="nameBox" className="col-sm-2 col-form-label">
                E-mail
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  
                  className="form-control"
                  id="mailBox"
                  value={this.state.userInfo.email}
                />
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="typeBox" className="col-sm-2 col-form-label">
                Status
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="typeBox"
                  value={this.state.userInfo.status}
                />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="addressBox" className="col-sm-2 col-form-label">
                User address
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="addressBox"
                  value={
                    this.state.userInfo.address1 +
                    this.state.userInfo.address2 +
                    this.state.userInfo.address3 +
                    this.state.userInfo.address4
                  }
                />
              </div>
            </div>
          </div>
        )
      );
    }
  }
  render() {
    return (
      <div>
        <UserContext.Consumer>
          {item => {
            let rendered = [];
            let session = item.session;
            rendered.push(<form>{this.getUserForm(session)}</form>);
            rendered.push(this.getButtons());
            rendered.push(this.state.modify && <UserModifyControl id={this.state.userInfo.id}/>);

            return rendered;
          }}
        </UserContext.Consumer>
      </div>
    );
  }
}
export default ViewUser;