import React from 'react';
import './registerStyle.css';

export class RegisterViewUser extends React.Component {

  constructor(props){
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateUsername = this.validateUsername.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
  }

  render() {
      const register = (
        <form onSubmit={this.handleSubmit} className="registerBox">
          <h2 className="registerTitle">Register a user</h2>
            <input type="text" placeholder="Username" id="username" className="registerLine" onBlur={this.validateUsername} />
            <small id="userInfo" className="redText" hidden="true" /><br />
            <input type="email" placeholder="E-Mail" id="email" className="registerLine" onBlur={this.validateEmail}/><br />
            <small id="emailInfo" className="redText" hidden="true" />
            <input type="password" placeholder="Password" id="passwordReg" className="registerLine" onKeyUp={this.validatePassword}/><br />
            <input type="password" placeholder="Retype password" id="passwordRepeat" className="registerLine" onKeyUp={this.validatePassword} />
            <small id="passInfo" className="redText" hidden="true">Passwords must match</small><br />
            <input type="submit" value="Next" className="registerBtn" id="nextBtn" />
        </form>
      );

      return (
        <div>
          {register}
        </div>
        );
    }

    vaidateInput(){
      return this.validateUsername() && this.validatePassword() && this.validateEmail();
    }

    validateUsername(){
      let userInput = document.getElementById("username")
      this.props.checkUser(userInput.value);
      if(userInput.value.length == 0){
        userInput.classList.add('red')
        document.getElementById('userInfo').innerHTML = "User must have a username!"
        document.getElementById('userInfo').hidden = false;
        return false;
      } else {
        userInput.classList.remove('red');
        document.getElementById('userInfo').hidden = true;
        return true;
      }  
    }

    validateEmail(){
      if(document.getElementById("email").value.length > 0){
        document.getElementById("emailInfo").hidden = true;
        document.getElementById("email").classList.remove("red");
        return true;
      } else {
        document.getElementById("email").classList.add("red");
        document.getElementById("emailInfo").innerHTML = "Email can't be empty!";
        document.getElementById("emailInfo").hidden = false;
        return false;
      }
    }

    validatePassword(){
      if(document.getElementById("passwordReg").value.length == 0 && document.getElementById("passwordRepeat").value.length == 0){
        document.getElementById('passwordReg').classList.add('red');
        document.getElementById('passwordRepeat').classList.add('red');
        document.getElementById("passInfo").hidden = false;
        document.getElementById("passInfo").innerHTML = "Password can't be empty!"
        return false;
      }
      if(document.getElementById('passwordReg').value !== document.getElementById('passwordRepeat').value){
        document.getElementById('passwordReg').classList.add('red');
        document.getElementById('passwordRepeat').classList.add('red');
        document.getElementById("passInfo").hidden = false;
        document.getElementById("passInfo").innerHTML = "Passwords must match!"
        return false;
      } else {
        document.getElementById("passInfo").hidden = true;
        document.getElementById('passwordReg').classList.remove('red');
        document.getElementById('passwordRepeat').classList.remove('red');
        return true;
      }
    }

    componentDidMount(){
      document.getElementById('username').value = this.props.userDetails.username;
      document.getElementById('email').value = this.props.userDetails.email;
    }
    
    handleSubmit(ev){
      ev.preventDefault();
      if(this.vaidateInput()){
        let params = {
          username: document.getElementById('username').value,
          password: document.getElementById('passwordReg').value,
          email: document.getElementById('email').value,
        };
        this.props.onSubmit(params);
      }
    }
}

RegisterViewUser.defaultProps = {
  userDetails: {
    username: '',
    email: ''
  }
};