import React, { Component } from "react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import {UserApprovalsControl} from '../approvals/UserApprovalsControl';
import { UserContext } from "../App";
import { RegisterControl } from './RegisterControl'

export class UsersWindow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modify:false,
      users: [],
      userModel: ""
    };
    this.onRowClick = this.onRowClick.bind(this);
    this.createNewUserClicked = this.createNewUserClicked.bind(this);
    this.usersOverview = this.usersOverview.bind(this);
    this.showPendingTable = this.showPendingTable.bind(this);
  }
  getFunctionUrl(functions,name) {

    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {

        if (functions[t].name == name) {
          return "http://localhost:8080" + functions[t].url;
        }
      }
    }
  }
  getModel() {
    //TODO here
   
    let url=this.getFunctionUrl(this.props.functions,"View user model");
    fetch(url, {
      method: "GET",
      mode: "cors",
      credentials: 'include',
    })
      .then(res => res.json())
      .then(
        result => {
          
          this.setState({
            userModel: result
          });
        },
        error => {
          console.log(error);
        }
      )
      .catch(err => {});
  }

  createNewUserClicked() {
    if(this.state.createClicked === "true")
      this.setState({
        createClicked : "false"
      })
    else 
      this.setState({
        createClicked : "true"
      })
  }

  componentWillMount() {
    this.getData();
    this.getModel();
  }
  
  checkRights(functions) {
    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {
        if (functions[t].name=="Create user") {
          return (
            <button
              type="button"
              onClick={this.createNewUserClicked}
              className="btn btn-success"
            >
              Create new user
            </button>
          );
        }
      }
    }
    return null;
  }
  createUser(){
    return <RegisterControl />
  }

  render() 
  {
    return (
      <div>
        <UserContext.Consumer>
          {item => {
            let session = item.session;

            let rendered = [];
            rendered.push(!this.state.createClicked && this.usersOverview(session));

            rendered.push(this.state.createClicked && <RegisterControl />);
           
           
            return rendered;
          }}
        </UserContext.Consumer>
      </div>
    );
  }

  onRowClick(state, rowInfo, column, instance) {
    return {
      onClick: e => {
        let clickedId = rowInfo.original["id"];
        console.log("The clicked id is " + clickedId),
          this.props.history.push({
            pathname: "viewUser",
            state: { id: clickedId }
          });


        }
      }
  }

  filterNonPending(result){
    let filteredAccounts = result.filter(user => !user.status.includes("PENDING"));
    this.setState({
      users: filteredAccounts
    })
  }

  createCollumns(model) {
    let collumns = [];
    console.log("The model is " + JSON.stringify(model));
    collumns = Object.keys(model).map(key => {
      return {
        Header: key,
        accessor: key
      };
    });
    return collumns;
  }
  getData() {
    let ui = [];
    fetch("http://localhost:8080/user/view", {
      method: "GET",
      mode: "cors",
      credentials: 'include'
    })
      .then(res => res.json())
      .then(res => this.filterNonPending(res))
      .catch(err => {
        err.text().then(errorMessage => {});
      });
  }
  checkIfHasRight(right) {
    let functions = this.props.functions;
    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {
        if (functions[t].name == right) {
         // console.log(JSON.stringify(functions[t]));

          return true;
        }
      }
    }
    return false;
  }
  usersOverview(session)
  {
    //console.log(JSON.stringify(session));

    let rendered = [];
    rendered.push(this.checkRights(session.functions));
    rendered.push(this.checkIfHasRight("Modify user") && <div>
       <label class="switch">
    <input id="modifyUser" type="checkbox" onChange={this.showPendingTable}/>
    <span class="slider round"></span>
    
  </label>
  <label for="modifyUser">Approve users</label>
</div>
    );
  
     rendered.push((!this.state.modify) && this.createTable())
     rendered.push((this.state.modify) && this.usersModify());

    return rendered;
 
  }

  showPendingTable()
  {
    this.setState(
      {
        modify: (!this.state.modify)
      }
    );
  }

  usersModify()
  {
    let rendered = [];
    rendered.push(<UserApprovalsControl/>);



    return rendered;
  }


  createTable() {
    let table = [];

    let colls = this.createCollumns(this.state.userModel);
    console.log("cols are:" + JSON.stringify(colls));
    let data = this.state.users;
    console.log("the data is:" + JSON.stringify(this.state.users));
return (
      <ReactTable
        showPaginationTop={true}
        data={data}
        columns={colls}
        getTrProps={this.onRowClick}
      />
    );
  }
}
export default UsersWindow;