import React from 'react';
import { RegisterViewUser } from './RegisterViewUser';
import { RegisterViewDetails } from './RegisterViewDetails';

export class RegisterControl extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            status: 'accountFetch'
          };
        this.handleRegister = this.handleRegister.bind(this);
        this.handleCollect = this.handleCollect.bind(this);
        this.handleBack = this.handleBack.bind(this);
        this.checkUser = this.checkUser.bind(this);
    }

    componentDidMount(){
        fetch('http://localhost:8080/profile/view' ,{
            method: 'GET',
            mode: 'cors',
            credentials: 'include'
        })
        .then(response => response.json())
        .then(response => response.filter(profile => profile.status === "ACTIVE"))
        .then(response => this.setState({
            profiles: response
        }));
    }

    handleRegister(params){
         fetch('http://localhost:8080/user/add', {
             method: 'post',
             mode: 'cors',
             credentials: 'include',
             headers: {
                'Content-Type': 'application/json; charset=utf-8'
             },
             body: JSON.stringify(params)
            })
            .then(respone => respone.text())
            .then(respone => alert(respone));
    }

    handleCollect(params){
        this.setState({
            userDetails: params,
            status: 'detailsFetch'
        });
    }

    handleBack(details){
        this.setState({
            status: 'accountFetch',
            userDetails: details
        });
    }

    checkUser(userSent){
        fetch('http://localhost:8080/user/view' ,{
            method: 'GET',
            mode: 'cors',
            credentials: 'include'
        })
        .then(response => response.json())
        .then(response => response.filter(user => user.username === userSent))
        .then(response => {
            if(response.length > 0){
                document.getElementById("username").classList.add("red");
                document.getElementById("userInfo").hidden = false;
                document.getElementById("userInfo").innerHTML = "User already exists!";
            }
        })
    }

    render(){
        if (this.state.status === 'accountFetch'){
            return <RegisterViewUser onSubmit={this.handleCollect} checkUser={this.checkUser} userDetails={this.state.userDetails} />;
        } else {
            return <RegisterViewDetails onSubmit={this.handleRegister} profiles={this.state.profiles} userDetails={this.state.userDetails} onClick={this.handleBack}/>;
        }

    }
}