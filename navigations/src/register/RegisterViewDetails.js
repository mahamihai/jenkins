import React from 'react';
import './registerStyle.css';

export class RegisterViewDetails extends React.Component {

  constructor(props){
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleBack = this.handleBack.bind(this);
  }

  createProfiles(){
    const options = this.props.profiles.map((profile, key) => <option key={key} >{profile.name}</option>);
    let body = (
      <select className="registerProfile" id="selectedProfile">
        {options}
      </select>
    );
    return body;
  }

    render() {
        const details = (
          <form onSubmit={this.handleSubmit} className="registerBox">
          <h2 className="registerTitle">User Details</h2>
            <input type="text" placeholder="Name" id="name" className="registerLine" /><br />
            <input type="text" placeholder="Country" id="country" className="registerLine" /><br />
            <input type="text" placeholder="District/County" id="district" className="registerLine" /><br />
            <input type="text" placeholder="City/Town" id="city" className="registerLine" /><br />
            <input type="text" placeholder="Street" id="street" className="registerLine" /><br />
            {this.createProfiles()} <br />
            <button onClick={this.handleBack} className="registerDetailBtn" >Back</button>
            <input type="submit" value="Register" className="registerDetailBtn register" />
          </form>
        );

        return (
          <div>
            {details}
          </div>
        );
      }

    handleBack(ev){
      ev.preventDefault();
      this.props.onClick(this.props.userDetails);
    }
    
    handleSubmit(ev){
      ev.preventDefault();
      let selectedProfile = this.props.profiles.filter((profile) => profile.name == document.getElementById("selectedProfile").value);
      let params = {
        username: this.props.userDetails.username,
        password: this.props.userDetails.password,
        email: this.props.userDetails.email,
        name: document.getElementById('name').value,
        address1: document.getElementById('country').value,
        address2: document.getElementById('district').value,
        address3: document.getElementById('city').value,
        address4: document.getElementById('street').value,
        profile: selectedProfile[0],
      };
      this.props.onSubmit(params);
    }
}