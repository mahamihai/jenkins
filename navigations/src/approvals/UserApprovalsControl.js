import React from 'react';
import { UserApprovalsView } from './UserApprovalsView'

export class UserApprovalsControl extends React.Component {

    constructor(props){
        super(props);

        this.state = {
           users : [],
        }

        this.submitUserReject = this.submitUserReject.bind(this);
        this.submitUserApprove = this.submitUserApprove.bind(this);
        this.fetchApprovals = this.fetchApprovals.bind(this);
    }

    componentDidMount(){
        this.fetchApprovals();
    }

    fetchApprovals(){
        fetch('http://localhost:8080/user/pending' ,{
            method: 'GET',
            mode: 'cors',   
            credentials: 'include'
        })
        .then(response => response.json())
        .then(response => this.setState({
            users: response,
        }));
        fetch('http://localhost:8080/user/view' ,{
            method: 'GET',
            mode: 'cors',   
            credentials: 'include'
        })
        .then(response => response.json())
        .then(response => response.filter(user => user.status === "ACTIVE"))
        .then(response => this.setState({
            activeUsers: response,
        }));
    }


    submitUserApprove(user){
        fetch('http://localhost:8080/user/approve',
        {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }, 
            body: JSON.stringify(user),
        })
        .then(response => response.json())
        .then(res => alert(res))
        .then(this.fetchApprovals)
    }

    submitUserReject(user){
        fetch('http://localhost:8080/user/reject',
        {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }, 
            body: JSON.stringify(user),
        })
        .then(response => response.json())
        .then(res => alert(res))
        .then(this.fetchApprovals)
    }

    selectViews(){
        let body = (
            <div>
                <UserApprovalsView  
                users={this.state.users}
                activeUsers={this.state.activeUsers}
                submitUserRejection={this.submitUserReject}
                submitUserApproval={this.submitUserApprove} />
            </div>
        )
        return body;
    }

    render(){
        return this.selectViews();
    }
}