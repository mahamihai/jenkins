import React from 'react';
import { AccountApprovalsView } from './AccountApprovalsView'

export class AccountApprovalsControl extends React.Component {

    constructor(props){
        super(props);

        this.state = {
           accounts : [],
           activeAccounts: [],
        }

        this.submitAccountReject = this.submitAccountReject.bind(this);
        this.submitAccountApprove = this.submitAccountApprove.bind(this);
        this.fetchApprovals = this.fetchApprovals.bind(this);
    }

    componentDidMount(){
        this.fetchApprovals();
    }

    fetchApprovals(){
        fetch('http://localhost:8080/account/view' ,{
            method: 'GET',
            mode: 'cors',   
            credentials: 'include'
        })
        .then(response => response.json())
        .then(response => response.filter(account => account.status === "ACTIVE"))
        .then(response => this.setState({
            activeAccounts: response,
        }));
        fetch('http://localhost:8080/account/pending' ,{
            method: 'GET',
            mode: 'cors',   
            credentials: 'include'
        })
        .then(response => response.json())
        .then(response => this.setState({
            accounts: response,
        }));

    }


    submitAccountApprove(account){
        fetch('http://localhost:8080/account/approve',
        {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }, 
            body: JSON.stringify(account),
        })
        .then(response => response.json())
        .then(res => alert(res))
        .then(this.fetchApprovals)
    }

    submitAccountReject(account){
        fetch('http://localhost:8080/account/reject',
        {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }, 
            body: JSON.stringify(account),
        })
        .then(response => response.json())
        .then(res => alert(res))
        .then(this.fetchApprovals)
    }

    selectViews(){
        let body = (
            <div>
                <AccountApprovalsView  
                accounts={this.state.accounts}
                activeAccounts = {this.state.activeAccounts}
                submitAccountRejection={this.submitAccountReject}
                submitAccountApproval={this.submitAccountApprove} />
            </div>
        )
        return body;
    }

    render(){
        return this.selectViews();
    }
}