import React from 'react';
import { ProfileApprovalsView } from './ProfileApprovalsView'

export class ProfileApprovalsControl extends React.Component {

    constructor(props){
        super(props);

        this.state = {
           profiles : [],
           activeProfiles: [],
        }

        this.submitProfileReject = this.submitProfileReject.bind(this);
        this.submitProfileApprove = this.submitProfileApprove.bind(this);
        this.fetchApprovals = this.fetchApprovals.bind(this);
    }

    componentDidMount(){
        this.fetchApprovals();
    }

    fetchApprovals(){
        fetch('http://localhost:8080/profile/pending' ,{
            method: 'GET',
            mode: 'cors',   
            credentials: 'include'
        })
        .then(response => response.json())
        .then(response => this.setState({
            profiles: response,
        }));
        fetch('http://localhost:8080/profile/view' ,{
            method: 'GET',
            mode: 'cors',   
            credentials: 'include'
        })
        .then(response => response.json())
        .then(response => response.filter(profile => profile.status === "ACTIVE"))
        .then(response => this.setState({
            activeProfiles: response,
        }));
    }


    submitProfileApprove(profile){
        fetch('http://localhost:8080/profile/approve',
        {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }, 
            body: JSON.stringify(profile),
        })
        .then(response => response.json())
        .then(res => alert(res))
        .then(this.fetchApprovals)
    }

    submitProfileReject(profile){
        fetch('http://localhost:8080/profile/reject',
        {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }, 
            body: JSON.stringify(profile),
        })
        .then(response => response.json())
        .then(res => alert(res))
        .then(this.fetchApprovals)
    }

    selectViews(){
        let body = (
            <div>
                <ProfileApprovalsView  
                profiles={this.state.profiles}
                activeProfiles={this.state.activeProfiles}
                submitProfileRejection={this.submitProfileReject}
                submitProfileApproval={this.submitProfileApprove} />
            </div>
        )
        return body;
    }

    render(){
        return this.selectViews();
    }
}