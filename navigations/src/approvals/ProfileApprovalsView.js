import React from 'react';
import './approvalsStyle.css';

export class ProfileApprovalsView extends React.Component{

    constructor(props){
        super(props);

        this.handleProfile = this.handleProfile.bind(this);
        this.handleProfileView = this.handleProfileView.bind(this);
        this.seeDetailsClicked = this.seeDetailsClicked.bind(this);

        this.state = {
            details: ""
        }
    }

    render() {
        const approval = (
          <div>
          {this.renderProfiles()}
          </div>
    );

        if(this.state.details == "")
            return (
                <div>
                {approval}
                </div>
        );
        else
            return (
                <div>
                    {approval}
                    {this.seeDetails(this.state.details)}
                </div>
        )
    }

    seeDetailsClicked(ev){
        let id = ev.target.name;
        if(id == ""){
            document.getElementById("profilesApprovalTable").hidden = false;
        }
        else {
            document.getElementById("profilesApprovalTable").hidden = true;
        }
        this.setState({
            details: id,
        })
    }

    handleProfile(ev){
        let id = ev.target.name;
        let stance = ev.target.value;
        let currentRow = document.getElementById("profilesApprovalTable").rows[id];
        let result = {};
        for(let index=0; index<currentRow.cells.length -3; index++){
           result[currentRow.cells[index].headers] = currentRow.cells[index].innerText;
        }
        if(stance === "Approve")
            this.props.submitProfileApproval(result);
        else
            this.props.submitProfileRejection(result);
    }

    handleProfileView(ev){
        this.seeDetailsClicked({
            target:{
                name: ""
            }
        })
        let id = ev.target.name;
        let stance = ev.target.value;
        let currentRow = document.getElementById("detailsView").rows[id];
        let result = {};
        result[currentRow.cells[1].headers] = currentRow.cells[1].innerText;
        if(stance === "Approve")
            this.props.submitProfileApproval(result);
        else
            this.props.submitProfileRejection(result);
    }


    renderProfiles(){
        let profiles = this.props.profiles;
        let tableRows = profiles.map((profile, row) => 
            <tr key={row}>
            <td headers="id">{profile.id}</td>
            <td headers="name">{profile.name}</td>
            <td headers="functions">{profile.functions}</td>
            <td>{profile.status}</td>
            <td name="handleProfile"><input 
                type="submit" 
                value="Approve" 
                className="btn approve" 
                name={row + 2}
                onClick={this.handleProfile}/>
            </td>
            <td name="handleProfile"><input 
                type="submit" 
                value="Reject" 
                className="btn reject" 
                name={row + 2}
                onClick={this.handleProfile}/>
            </td>
            <td name="seeDetails"><input 
                type="submit" 
                value="Details" 
                className="btn details" 
                name={row + 2}
                onClick={this.seeDetailsClicked}/>
            </td>
        </tr>)
            return (
                <table className="approvalTable" id="profilesApprovalTable">
                    <thead>
                        <tr>
                            <th colSpan="7"><h1>Profiles</h1></th>
                        </tr>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Permissions</th>
                            <th>Pending status</th>
                            <th>Approve</th>
                            <th>Reject</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tableRows} 
                    </tbody>      
                </table>
            )  
    }

    seeDetails(id){
        let currentRow = document.getElementById("profilesApprovalTable").rows[id];
        let modified = this.props.profiles.filter(profile => profile.name === currentRow.cells[1].innerText)[0];
        let active = this.props.activeProfiles.filter(profile => profile.name === currentRow.cells[1].innerText)[0];
        let table = (
            <table id="detailsView" className="approvalTable">
                <thead>
                    <tr>
                        <th colSpan="8"><h1>Profile</h1></th>
                    </tr>
                    <tr>
                        <th>State</th>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Permissions</th>
                        <th>Pending status</th>
                        <th>Approve</th>
                        <th>Reject</th>
                        <th>Details</th>                        
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Modified: </td>
                        <td headers="id">{modified.id}</td>
                        <td>{modified.name}</td>
                        <td>{modified.functions}</td>
                        <td>{modified.status}</td>
                        <td name="handleProfile" rowSpan="2"><input 
                            type="submit" 
                            value="Approve" 
                            className="btn approve" 
                            name="2"
                            onClick={this.handleProfileView}/>
                        </td>
                        <td name="handleProfile" rowSpan="2"><input 
                            type="submit" 
                            value="Reject" 
                            className="btn reject" 
                            name="2"
                            onClick={this.handleProfileView}/>
                        </td>
                        <td name="seeDetails" rowSpan="2"><input 
                            type="submit" 
                            value="Back" 
                            className="btn details" 
                            name=""
                            onClick={this.seeDetailsClicked}/>
                        </td>
                    </tr>
                    <tr>
                        <td>Currently Active: </td>
                        <td>{active.id}</td>
                        <td>{active.name}</td>
                        <td>{active.functions}</td>
                        <td>{active.status}</td>
                    </tr>
                    
                </tbody>
            </table>
        )
        return table;
    }
    
}