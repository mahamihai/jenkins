import React from 'react';
import './approvalsStyle.css';

export class UserApprovalsView extends React.Component{

    constructor(props){
        super(props);

        this.handleUser = this.handleUser.bind(this);
        this.handleUserDetails = this.handleUserDetails.bind(this);
        this.seeDetailsClicked = this.seeDetailsClicked.bind(this);

        this.state ={
            details: ""
        }
    }

    render() {
        const approval = (
          <div>
          {this.renderUsers()}
          </div>
    );

    if(this.state.details == "")
        return (
            <div>
            {approval}
            </div>
        );
    else
        return (
            <div>
                {approval}
                {this.seeDetails(this.state.details)}
            </div>
)
    }

    seeDetailsClicked(ev){
        let id = ev.target.name;
        if(id == ""){
            document.getElementById("usersApprovalTable").hidden = false;
        }
        else {
            document.getElementById("usersApprovalTable").hidden = true;
        }
        this.setState({
            details: id,
        })
    }

    handleUser(ev){
        let id = ev.target.name;
        let stance = ev.target.value;
        let currentRow = document.getElementById("usersApprovalTable").rows[id];
        let result = {};
        result[currentRow.cells[0].headers] = currentRow.cells[0].innerText;
        if(stance === "Approve")
            this.props.submitUserApproval(result);
        else
            this.props.submitUserRejection(result);
    }

    handleUserDetails(ev){
        this.seeDetailsClicked(
            {
                target: {
                    name: "",
                }
            });

        let id = ev.target.name;
        let stance = ev.target.value;
        let currentRow = document.getElementById("detailsView").rows[id];
        let result = {};
        result[currentRow.cells[1].headers] = currentRow.cells[1].innerText;
        if(stance === "Approve")
            this.props.submitUserApproval(result);
        else
            this.props.submitUserRejection(result);
    }


    renderUsers(){
        let users = this.props.users;
        let tableRows = users.map((user, row) => 
            <tr key={row}>
                <td headers="id">{user.id}</td>
                <td headers="username">{user.username}</td>
                <td headers="email">{user.email}</td>
                <td headers="name">{user.name}</td>
                <td headers="address">{user.address1}, {user.address2}, {user.address3}, {user.address4}</td>
                <td headers="profile">{user.profile.name}</td>
                <td>{user.status}</td>
                <td name="handleUser"><input 
                    type="submit" 
                    value="Approve" 
                    className="btn approve" 
                    name={row + 2}
                    onClick={this.handleUser}/>
                </td>
                <td name="handleUser"><input 
                    type="submit" 
                    value="Reject" 
                    className="btn reject" 
                    name={row + 2}
                    onClick={this.handleUser}/>
                </td>
                <td name="seeDetails"><input 
                type="submit" 
                value="Details" 
                className="btn details" 
                name={row + 2}
                onClick={this.seeDetailsClicked}/>
                </td>
            </tr>)
            return (
                <table className="approvalTable" id="usersApprovalTable">
                    <thead>
                        <tr>
                            <th colSpan="10"><h1>Users</h1></th>
                        </tr>
                        <tr>
                            <th>Id</th>
                            <th>Username</th>
                            <th>E-mail</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Profile</th>
                            <th>Pending status</th>
                            <th>Approve</th>
                            <th>Reject</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tableRows} 
                    </tbody>      
                </table>
            )
    }
    
    seeDetails(id){
        let currentRow = document.getElementById("usersApprovalTable").rows[id];
        let modified = this.props.users.filter(user => user.username === currentRow.cells[1].innerText)[0];
        let active = this.props.activeUsers.filter(user => user.username === currentRow.cells[1].innerText)[0];
        let table = (
            <table id="detailsView" className="approvalTable">
                <thead>
                    <tr>
                        <th colSpan="11"><h1>User</h1></th>
                    </tr>
                    <tr>
                        <th>State</th>
                        <th>Id</th>
                        <th>Username</th>
                        <th>E-mail</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Profile</th>
                        <th>Pending status</th>
                        <th>Approve</th>
                        <th>Reject</th>
                        <th>Details</th>                       
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        
                        <td>Modified: </td>
                        <td headers="id">{modified.id}</td>
                        <td>{modified.username}</td>
                        <td>{modified.email}</td>
                        <td>{modified.name}</td>
                        <td>{modified.address1}, {modified.address2}, {modified.address3}, {modified.address4}</td>
                        <td>{modified.profile.name}</td>
                        <td>{modified.status}</td>

                        <td name="handleProfile" rowSpan="2"><input 
                            type="submit" 
                            value="Approve" 
                            className="btn approve" 
                            name="2"
                            onClick={this.handleUserDetails}/>
                        </td>
                        <td name="handleProfile" rowSpan="2"><input 
                            type="submit" 
                            value="Reject" 
                            className="btn reject" 
                            name="2"
                            onClick={this.handleUserDetails}/>
                        </td>
                        <td name="seeDetails" rowSpan="2"><input 
                            type="submit" 
                            value="Back" 
                            className="btn details" 
                            name=""
                            onClick={this.seeDetailsClicked}/>
                        </td>
                    </tr>
                    <tr>
                    <td>Currently Active: </td>
                        <td headers="id">{active.id}</td>
                        <td>{active.username}</td>
                        <td>{active.email}</td>
                        <td>{active.name}</td>
                        <td>{active.address1}, {active.address2}, {active.address3}, {active.address4}</td>
                        <td>{active.profileName}</td>
                        <td>{active.status}</td>
                    </tr>
                </tbody>
            </table>
        )
        return table;
    }
    
}