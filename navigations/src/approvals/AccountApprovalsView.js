import React from 'react';
import './approvalsStyle.css';

export class AccountApprovalsView extends React.Component{

    constructor(props){
        super(props);

        this.seeDetailsClicked = this.seeDetailsClicked.bind(this);
        this.handleAccount = this.handleAccount.bind(this);
        this.handleAccountView = this.handleAccountView.bind(this);

        this.state = {
            details: ""
        }
    }

    render() {
        const approval = (
          <div>
          {this.renderAccounts()}
          </div>
        );
        if(this.state.details == "")
            return (
                <div>
                {approval}
                </div>
            );
        else
            return (
                <div>
                    {approval}
                    {this.seeDetails(this.state.details)}
                </div>
            )
    }

    seeDetailsClicked(ev){
        let id = ev.target.name;
        if(id == ""){
            document.getElementById("accountsApprovalTable").hidden = false;
        }
        else {
            document.getElementById("accountsApprovalTable").hidden = true;
        }
        this.setState({
            details: id,
        })
    }

    handleAccount(ev){
        let id = ev.target.name;
        let stance = ev.target.value;
        let currentRow = document.getElementById("accountsApprovalTable").rows[id];
        let result = {};
        for(let index=0; index<currentRow.cells.length -3; index++){
           result[currentRow.cells[index].headers] = currentRow.cells[index].innerText;
        }
        if(stance === "Approve")
            this.props.submitAccountApproval(result);
        else
            this.props.submitAccountRejection(result);
    }

    handleAccountView(ev){
        this.seeDetailsClicked({
            target: {
                name: ""
            }
        })
        let id = ev.target.name;
        let stance = ev.target.value;
        let currentRow = document.getElementById("detailsView").rows[id];
        let result = {};
        result[currentRow.cells[1].headers] = currentRow.cells[1].innerText;
        if(stance === "Approve")
            this.props.submitAccountApproval(result);
        else
            this.props.submitAccountRejection(result);
    }

    renderAccounts(){
        let accounts = this.props.accounts;
        let tableRows = accounts.map((account, row) => 
            <tr key={row}>
            <td headers="id">{account.id}</td>
            <td headers="number">{account.number}</td>
            <td headers="type">{account.type}</td>
            <td headers="name">{account.name}</td>
            <td headers="address">{account.address1}, {account.address2}, {account.address3}, {account.address4}</td>
            <td headers="currency">{account.currency}</td>
            <td headers="financialStatus">{account.financialStatus}</td>
            <td>{account.status}</td>
            <td name="handleAccount"><input 
                type="submit" 
                value="Approve" 
                className="btn approve" 
                name={row + 2}
                onClick={this.handleAccount}/>
            </td>
            <td name="handleAccount"><input 
                type="submit" 
                value="Reject" 
                className="btn reject" 
                name={row + 2}
                onClick={this.handleAccount}/>
            </td>
            <td name="seeDetails"><input 
                type="submit" 
                value="Details" 
                className="btn details" 
                name={row + 2}
                onClick={this.seeDetailsClicked}/>
            </td>
            </tr>)
        return (
            <table className="approvalTable" id="accountsApprovalTable">
                <thead>
                    <tr>
                        <th colSpan="11"><h1>Account</h1></th>
                    </tr>
                    <tr>
                        <th>Id</th>
                        <th>Number</th>
                        <th>Account type</th>
                        <th>Account holder</th>
                        <th>Address</th>
                        <th>Currency</th>
                        <th>Financial Status</th>
                        <th>Pending status</th>
                        <th>Approve</th>
                        <th>Reject</th>
                        <th>Details</th>                        
                        </tr>
                </thead>
                <tbody>
                    {tableRows}  
                </tbody>     
            </table>
            )
}

seeDetails(id){
    let currentRow = document.getElementById("accountsApprovalTable").rows[id];
    let modified = this.props.accounts.filter(account => account.number === currentRow.cells[1].innerText)[0];
    let active = this.props.activeAccounts.filter(account => account.number === currentRow.cells[1].innerText)[0];
    let table = (
        <table id="detailsView" className="approvalTable">
            <thead>
                <tr>
                    <th colSpan="12"><h1>Accounts</h1></th>
                </tr>
                <tr>
                    <th>State</th>
                    <th>Id</th>
                    <th>Number</th>
                    <th>Account type</th>
                    <th>Account holder</th>
                    <th>Address</th>
                    <th>Currency</th>
                    <th>Financial Status</th>
                    <th>Pending status</th>
                    <th>Approve</th>
                    <th>Reject</th>
                    <th>Back</th>                        
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Modified: </td>
                    <td headers="id">{modified.id}</td>
                    <td>{modified.number}</td>
                    <td>{modified.type}</td>
                    <td>{modified.name}</td>
                    <td>{modified.address1}, {modified.address2}, {modified.address3}, {modified.address4}</td>
                    <td>{modified.currency}</td>
                    <td>{modified.financialStatus}</td>
                    <td>{modified.status}</td>
                    <td name="handleAccount" rowSpan="2"><input 
                        type="submit" 
                        value="Approve" 
                        className="btn approve" 
                        name="2"
                        onClick={this.handleAccountView}/>
                    </td>
                    <td name="handleAccount" rowSpan="2"><input 
                        type="submit" 
                        value="Reject" 
                        className="btn reject" 
                        name="2"
                        onClick={this.handleAccountView}/>
                    </td>
                    <td name="seeDetails" rowSpan="2"><input 
                        type="submit" 
                        value="Back" 
                        className="btn details" 
                        name=""
                        onClick={this.seeDetailsClicked}/>
                    </td>
                </tr>
                <tr>
                    <td>Currently Active: </td>
                    <td>{active.id}</td>
                    <td>{active.number}</td>
                    <td>{active.type}</td>
                    <td>{active.name}</td>
                    <td>{active.address1}, {active.address2}, {active.address3}, {active.address4}</td>
                    <td>{active.currency}</td>
                    <td>{active.financialStatus}</td>
                    <td>{active.status}</td>
                </tr>
                
            </tbody>
        </table>
    )
    return table;
}

}