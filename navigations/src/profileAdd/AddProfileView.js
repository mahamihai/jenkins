import React from 'react';
import './profileStyle.css';

export class AddProfileView extends React.Component {

  constructor(props){
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.createPermissions = this.createPermissions.bind(this);
    this.parseCheckboxes = this.parseCheckboxes.bind(this);
  }
  
  createPermissions(){
    const permissions = this.props.permissions.map((permission) => 
              <div><input type='checkbox' className="checkBox" value={permission} />{permission}</div>);
    return permissions;
  }

    render() {
        const profile = (
          <form onSubmit={this.handleSubmit} className="profileBox">
            <h2 className="profileTitle">Create profile</h2>
            <br />
            <input type="text" placeholder="Name" id="profileName" className="profileLine" /><br />
            {this.createPermissions()}
            <input type="submit" value="Submit" className="profileBtn" />
          </form>
        );

        return (
          <div>
            {profile}
          </div>
        );
      }
    
    parseCheckboxes(){
      var checkedValues = ''; 
      var inputElements = document.getElementsByClassName('checkBox');
      for(var i=0; inputElements[i]; ++i){
            if(inputElements[i].checked){
                 checkedValues += inputElements[i].value + ';';
            }
      }
      return checkedValues;
    }  

    handleSubmit(ev){
      ev.preventDefault();
      let params = {
        name: document.getElementById('profileName').value,
        functions: this.parseCheckboxes()
      };
      this.props.onSubmit(params);
    }
}