import React from 'react';
import { AddProfileView } from './AddProfileView';

export class AddProfileControl extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            permissions: [],
        };
        this.handleCreate = this.handleCreate.bind(this);
    }

    handleCreate(params){
        fetch('http://localhost:8080/profile/add' ,{
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }, 
            body: JSON.stringify(params)  
        })
        .then(res => res.json())
        .then(res => alert(res));
    }

    componentDidMount(){
        fetch('http://localhost:8080/rights' ,{
            method: 'GET',
            mode: 'cors',
            credentials: 'include'   
        })
        .then(response => response.json())
        .then(response => this.setState({
            permissions: response,
        }));
    }

    render(){
        return <AddProfileView onSubmit={this.handleCreate} permissions={this.state.permissions}/>;
    }
}