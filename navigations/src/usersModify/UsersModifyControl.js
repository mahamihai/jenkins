import React from 'react';
import { UserModifyView } from './UsersModifyView';

export class UserModifyControl extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            profiles : [],
            user : {
                profile : {
                    name: "mock",
                }
            }
        }

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(user){
        fetch('http://localhost:8080/user/update' ,{
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }, 
            body: JSON.stringify(user)  
        })
        .then(res => res.json())
        .then(res => alert(res));
    }

    componentDidMount(){
        fetch('http://localhost:8080/user/viewone?id=' + this.props.id ,{
            method: 'GET',
            mode: 'cors',
            credentials: 'include'   
        })
        .then(response => response.json())
        .then(response => this.setState({
            user: response
        }));

        fetch('http://localhost:8080/profile/view' ,{
            method: 'GET',
            mode: 'cors',
            credentials: 'include'   
        })
        .then(response => response.json())
        .then(response => response.filter(user => user.status === "ACTIVE"))
        .then(response => this.setState({
            profiles: response
        }));
    }

    render() {
        return ( 
          <UserModifyView 
            profiles = {this.state.profiles}
            user = {this.state.user }
            onSubmit={this.handleSubmit} />
        );
    }
}
export default UserModifyControl;