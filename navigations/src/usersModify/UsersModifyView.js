import React from 'react';
import '../register/registerStyle.css';

export class UserModifyView extends React.Component {

  constructor(props){
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);

  }

  handleSubmit(ev){
    ev.preventDefault();
    let userProfile = this.props.profiles.filter(profile => profile.name === document.getElementById("userProfile").value);
    let user = {
        id: document.getElementById("userId").innerText,
        username: document.getElementById("username").value,
        email : document.getElementById("email").value,
        profile: userProfile[0],
        name: document.getElementById("name").value,
        password: document.getElementById("password").value,
        address1: document.getElementById("country").value,
        address2: document.getElementById("district").value,
        address3: document.getElementById("city").value,
        address4: document.getElementById("street").value,
    }
    this.props.onSubmit(user);
  }

  createProfilesDropdown(){
    const profiles = this.props.profiles.map((profile, key) => <option value={profile.name} key={key} >{profile.name}</option>);
    return profiles;
  }

  render(){
    console.log("The profile is "+
JSON.stringify(this.props.user));    
      const details=(
          <form className="registerBox" onSubmit={this.handleSubmit}>
            <p id="userId" hidden>{this.props.user.id}</p>
            <h2 className="registerTitle">User Details</h2><br />
            Username: <input type="text" placeholder="Username" id="username" className="registerLine" defaultValue={this.props.user.username} readOnly/><br />
            E-mail: <input type="text" placeholder="E-mail" id="email" className="registerLine" defaultValue={this.props.user.email} /> <br />
            Profile:
            <select id="userProfile" className="registerSelect">
                <option selected disabled hidden>{this.props.user.profileName}</option>
                {this.createProfilesDropdown()}
            </select>
            <input type="password" placeholder="Password" id="password" className="registerLine" /><br />
            Name: <input type="text" placeholder="Name" id="name" className="registerLine" defaultValue={this.props.user.name} /> <br />
            Address: <input type="text" placeholder="Country" id="country" className="registerLine" defaultValue={this.props.user.address1}/><br />
            <input type="text" placeholder="District/County" id="district" className="registerLine" defaultValue={this.props.user.address2}/><br />
            <input type="text" placeholder="City/Town" id="city" className="registerLine" defaultValue={this.props.user.address3}/><br />
            <input type="text" placeholder="Street" id="street" className="registerLine" defaultValue={this.props.user.address4}/><br />
            <input type="submit" value="Submit" className="registerBtn"/>
          </form>
      )
      return (
          <div>
            {details}
          </div>
        )
  }
}


export default UserModifyView;