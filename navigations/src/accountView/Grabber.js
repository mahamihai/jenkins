import "./Grabber.css";
import React, { Component } from "react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import {AccountApprovalsControl} from '../approvals/AccountApprovalsControl';
import { UserContext } from "../App";
import { AccountCreateControl } from '../accountCreate/AccountCreateControl'

export class Grabber extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modify:false,
      accounts: [],
      accountModel: "",
      createClicked:false,
    };
    this.onRowClick = this.onRowClick.bind(this);
    this.createNewAccountClicked = this.createNewAccountClicked.bind(this);
    this.accountsOverview=this.accountsOverview.bind(this);
    this.showPendingTable=this.showPendingTable.bind(this);
   this.checkIfHasRight=this.checkIfHasRight.bind(this);
  }
  componentDidMount()
  {
    this.getData();
    this.getModel();
  }
  getFunctionUrl(functions,name) {
   // console.log("The functions are"+JSON.stringify(this.props.functions));

    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {

        if (functions[t].name == name) {
          console.log("Found the link "+functions[t].name);
          return "http://localhost:8080" + functions[t].url;
        }
      }
    }
  }
  checkIfHasRight(right) {
    let functions = this.props.functions;
    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {
        if (functions[t].name == right) {
         // console.log(JSON.stringify(functions[t]));

          return true;
        }
      }
    }
    return false;
  }
  getModel() {
   
    let url=this.getFunctionUrl(this.props.functions,"View account model");
    
    fetch(url, {
      method: "GET",
      mode: "cors",
      credentials: 'include',
    })
      .then(res => res.json())
      .then(
        result => {
          
          this.setState({
            accountModel: result
          });
        },
        error => {
          console.log(error);
        }
      )
      .catch(err => {});
  }
  

  createNewAccountClicked() {
    if(this.state.createClicked === "true")
      this.setState({
        createClicked : "false"
      })
    else 
      this.setState({
        createClicked : "true"
      })
  }
 
  checkRights(functions) {
    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {
        if (functions[t].name=="Create account") {
          return (
            <button
              type="button"
              onClick={this.createNewAccountClicked}
              className="btn btn-success"
            >
              Create new account
            </button>
          );
        }
      }
    }
    return null;
  }
  createAccount(){
    return <AccountCreateControl />
  }
  showPendingTable()
  {
    console.log("AICI");
    this.setState(
      {
        modify: (!this.state.modify)
      }
    );
    //console.log(this.state.modify);
  }
  accountsOverview(session)
  {
    //console.log(JSON.stringify(session));

    let rendered = [];
    rendered.push(this.checkRights(session.functions));
    rendered.push( this.checkIfHasRight("Modify account") &&
      <div>
      <label class="switch">
    <input id="modifySwitch" type="checkbox" onChange={this.showPendingTable}/>
    <span class="slider round"></span>
    </label>
          <label for="modifySwitch">Approve accounts

  </label>
  </div>);

     rendered.push((!this.state.modify) && this.createTable())
     rendered.push((this.state.modify) && this.accountsModify());

    return rendered;


    
  }
  accountsModify()
  {
    let rendered = [];
    rendered.push(<AccountApprovalsControl/>);



    return rendered;
  }
  
  render() 
  {
    return (
      <div>
        <UserContext.Consumer>
          {item => {
            let session = item.session;

            let rendered = [];
           
          
           // rendered.push(this.state.createClicked && this.accountsModify(session));
            rendered.push(!this.state.createClicked && this.accountsOverview(session));

            rendered.push(this.state.createClicked && <AccountCreateControl />);



            
           
           
            return rendered;
          }}
        </UserContext.Consumer>
      </div>
    );
  }

  onRowClick(state, rowInfo, column, instance) {
    return {
      onClick: e => {
        let clickedId = rowInfo.original["id"];
        console.log("The clicked id is " + clickedId),
          this.props.history.push({
            pathname: "viewAccount",
            state: { id: clickedId }
          });


        }
      }
  }

  filterActive(result){
    let filteredAccounts = result.filter(account => account.status == "ACTIVE");
    this.setState({
      accounts: filteredAccounts
    })
  }

  createCollumns(model) {
    let collumns = [];
    console.log("The model is " + JSON.stringify(model));
    collumns = Object.keys(model).map(key => {
      return {
        Header: key,
        accessor: key
      };
    });
    return collumns;
  }
  getData() {
    let url = this.getFunctionUrl(this.props.functions,"View account")

    console.log("The url is !!!!!!!!!! +" +JSON.stringify(url));
    
    fetch(url, {
      method: "GET",

      mode: "cors",
      credentials: 'include'
    })
      .then(res => res.json())
      .then(res => res.filter(account => !account.status.includes("PENDING")))
      .then(rez=>{this.setState(
        {accounts:rez}
      );
      console.log("The data is"+ JSON.stringify(rez));
      
    })
      .catch(err => {
      });
  }
  createTable() {
    let colls = this.createCollumns(this.state.accountModel);
    
    let data = this.state.accounts;
        return (
            <ReactTable
              showPaginationTop={true}
              data={data}
              columns={colls}
              getTrProps={this.onRowClick}
            />
          );
        }
}
export default Grabber;
