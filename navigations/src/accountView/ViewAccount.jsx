import React, { Component } from "react";
import { UserContext } from "../App";
import { AccountModifyControl } from '../accountModify/AccountModifyControl'

export class ViewAccount extends Component {
  constructor(props) {
    super(props);

    this.state = {
      accountInfo: "",
      modifyClicked: "false"
    };
    this.getAccountInfo();
    this.enableModify = this.enableModify.bind(this);
  }
  createDisplayForm;

  getAccountInfo() {
    let queryString = this.props.location.state; //get passed id from history
    let id = parseInt(queryString["id"]);
    //let id=1;
    fetch("http://localhost:8080/account/viewone?id=" + id, {
      method: "GET",
      mode: "cors",
      credentials: 'include',
    })
      .then(res => res.json())
      .then(
        result => {
          console.log(JSON.stringify(result));
          this.setState({
            accountInfo: result
          });
          console.log("the result is " + JSON.stringify(result));
        },

        error => {
          console.log(error);
        }
      )
      .catch(err => {});
  }

  enableModify(){
    if(this.state.modifyClicked === "true"){
      this.setState({
        modifyClicked: "false",
      })
    } else {
      this.setState({
        modifyClicked: "true",
      })
    }
  }

  deleteAccount(account, url) {
    console.log("I am deleting" + url);
    fetch("http://localhost:8080" + url, {
      method: "POST",
      mode: "cors",
      credentials: 'include',
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(account)
    })
      .then(res => res.json())
      .then(
        result => {
          console.log(JSON.stringify(result));
        },
        error => {
          console.log(error);
        }
      )
      .catch(err => {});
      this.props.history.push("/allAccounts")

  }

  getButtons(session) {
    let renderedButtons = [];
    let functions = session.functions;
    
    let backButton = <button className="btn btn-success" onClick={this.enableModify}>Back</button>
    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {
        switch (functions[t].name) {
          case "Delete account": {
            // console.log("I am deleting"+functions.functions[t].url);
            let url = functions[t].url;
            renderedButtons.push(
              <button
                type="button"
                onClick={() => {
                  this.deleteAccount(this.state.accountInfo, url);
                }}
                className="btn btn-danger"
              >
                Delete account
              </button>
            );
          }
          break;
          case "Modify account": {
            renderedButtons.push(
              <button
                type="button"
                onClick={this.enableModify}
                className="btn btn-warning "
              >
                Modify account
              </button>
            );
          }
          break;
        }
      }
    }
    if(this.state.modifyClicked != "true"){
      return renderedButtons;
    }
    else {
      return backButton;
    }
  }
  getAccountForm() {
    if(this.state.modifyClicked != "true"){
      return (
        this.state.accountInfo != "" && (
          <div>
            <div className="form-group row">
              <label htmlFor="nameBox" className="col-sm-2 col-form-label">
                Id
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="nameBox"
                  value={this.state.accountInfo.id}
                />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="nameBox" className="col-sm-2 col-form-label">
                Account number
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="nameBox"
                  value={this.state.accountInfo.number}
                />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="nameBox" className="col-sm-2 col-form-label">
                Account holder
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="nameBox"
                  value={this.state.accountInfo.name}
                />
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="nameBox" className="col-sm-2 col-form-label">
                Account holder address:
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="nameBox"
                  value={this.state.accountInfo.address1 + ', ' + this.state.accountInfo.address2 + ', ' + this.state.accountInfo.address3 + ', ' + this.state.accountInfo.address4}
                />
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="nameBox" className="col-sm-2 col-form-label">
                Account type
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="nameBox"
                  value={this.state.accountInfo.type}
                />
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="nameBox" className="col-sm-2 col-form-label">
                Account currency
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="nameBox"
                  value={this.state.accountInfo.currency}
                />
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="nameBox" className="col-sm-2 col-form-label">
                Account financial status
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="nameBox"
                  value={this.state.accountInfo.financialStatus}
                />
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="statusBox" className="col-sm-2 col-form-label">
                Status
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="statusBox"
                  value={this.state.accountInfo.status}
                />
              </div>
            </div>
          </div>
        )
      );
    }
  }

  getAccountModify(){
    if(this.state.modifyClicked === "true"){
      return <AccountModifyControl account={this.state.accountInfo} />
    }
  }

  render() {
    return (
      <div>
        <form>
          {
            <UserContext.Consumer>
              {item => {
                let rendered = [];
                let session = item.session;
                rendered.push(this.getAccountForm());
                rendered.push(this.getButtons(session));
                rendered.push(this.getAccountModify());
                return rendered;
              }}
            </UserContext.Consumer>
          }
        </form>
      </div>
    );
  }
}