import React from 'react'
import './homeAnimation.css'

export class HomeAnimation extends React.Component{
    constructor(props){
        super(props);

        this.dies = this.dies.bind(this);
    }

    dies(){
        alert("I died");
    }

     render(){
         return (<div>
             <div className="info">
             System version: 0.9.1 (beta)
             </div>
             <div class="container">
         <h1>Welcome{this.props.user.username && ", " + this.props.user.username}!</h1>
         
         <div class="bird-container bird-container--one">
             <div class="bird bird--one" ></div>
         </div>
         
         <div class="bird-container bird-container--two">
             <div class="bird bird--two" ></div>
         </div>
         
         <div class="bird-container bird-container--three">
             <div class="bird bird--three" ></div>
         </div>
         
         <div class="bird-container bird-container--four">
             <div class="bird bird--four"></div>
         </div>

         <div class="bird-container bird-container--five">
             <div class="bird bird--five"></div>
         </div>
         
     </div>
     </div>)
     }
}