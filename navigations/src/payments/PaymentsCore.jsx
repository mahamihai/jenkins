import React, { Component } from "react";
import ReactTable from "react-table";
import {PaymentCreate} from './PaymentCreate';
import "./PaymentsCore.css";
import {PaymentVerification} from './PaymentVerification';
import {PaymentsView} from './PaymentsView'
import {PaymentAuthApprove} from './PaymentAuthApprove';
export class PaymentsCore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
      page:"View payment"
    };
  }
  clicked(newPage) {
    console.log(JSON.stringify("I was clicked") + newPage);
    this.setState(
      {
        page:newPage
      }
    )
  }
  checkIfHasRight(right) {
    let functions = this.props.functions;
    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {
        if (functions[t].name == right) {
         // console.log(JSON.stringify(functions[t]));

          return true;
        }
      }
    }
    return false;
  }
  getDropDownButtons() {
    let buttons = [];
    buttons.push(
      this.checkIfHasRight("View payment") && (
        <button
          className="dropdown-item btn-lg"
          onClick={() => {
            this.clicked("View payment");
          }}
        >
          View payments
        </button>
      ),

      this.checkIfHasRight("Enter payment") && (
        <button
          className="dropdown-item btn-lg"
          onClick={() => {
            this.clicked("Enter payment");
          }}
        >
          Enter payment
        </button>
      ),
      this.checkIfHasRight("Verify payment") && (
        <button
          className="dropdown-item btn-lg"
          onClick={() => {
            this.clicked("Verify payment");
          }}
        >
          Verify payments
        </button>
      ),
      this.checkIfHasRight("Approve payment") && (
        <button
          className="dropdown-item btn-lg"
          onClick={() => {
            this.clicked("Approve payment");
          }}
        >
          Approve payment
        </button>
      ),
      this.checkIfHasRight("Authorize payment") && (
        <button
          className="dropdown-item btn-lg"
          onClick={() => {
            this.clicked("Authorize payment");
          }}
        >
          Authorize payment
        </button>
      )
    )
    return buttons;
  }
    getDropdDown()
    {
      return (
        <div className="dropdown">
        <button  className="dropbtn">
          Dropdown
        </button>
        <div id="myDropdown1" className="dropdown-content">
          
          {this.getDropDownButtons()}
        </div>
      </div>
      )
    }
/*
{
  public static final String ACTIVE = "ACTIVE";
		public static final String REJECTED = "REJECTED";
		public static final String VERIFY = "VERIFY";
		public static final String APPROVE = "APPROVE";
		public static final String AUTHORIZE = "AUTHORIZE";
		public static final String COMPLETED = "COMPLETED";
		public static final String CANCELLED = "CANCELLED";
}
*/

  render() {

    let ui=[];
    ui.push(this.getDropdDown());
    ui.push(
    ((this.state.page==="View payment")  &&   <PaymentsView functions={this.props.functions}/>),
    ((this.state.page==="Enter payment")  &&   <PaymentCreate functions={this.props.functions}/>),
    ((this.state.page==="Verify payment")  &&   <PaymentVerification page="Verify payment"functions={this.props.functions}/>),
    ((this.state.page==="Authorize payment")  &&   <PaymentAuthApprove filter="AUTHORIZE" page="Authorize payment" functions={this.props.functions}/>),
    ((this.state.page==="Approve payment")  &&   <PaymentAuthApprove filter="APPROVE" page="Approve payment" functions={this.props.functions}/>)
    );



    return (
      ui
    );
  }
}
