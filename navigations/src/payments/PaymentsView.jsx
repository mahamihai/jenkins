import React, { Component } from "react";
import ReactTable from "react-table";
import { Dropdown, DropdownToggle, DropdownItem } from "mdbreact";



export class PaymentsView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      payments: []
    };
  }
  getFunctionUrl(functions, name) {
    //console.log("The functions are" + JSON.stringify(this.props.functions));

    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {
        if (functions[t].name == name) {
          console.log("Found the link " + functions[t].name);
          return "http://localhost:8080" + functions[t].url;
        }
      }
    }
  }
  fetchPayments() {
    let url = this.getFunctionUrl(this.props.functions, "View payment");
    fetch(url, {
      method: "GET",

      mode: "cors",
      credentials: "include"
    })
      .then(res => res.json())
      .then(rez => {
        let filteredPayments = this.filterPayments(rez, "ACTIVE");
        console.log(JSON.stringify(filteredPayments));

        this.setState({ payments: rez });
      })
      .catch(err => {});
  }
  componentDidMount() {
    this.fetchPayments();
  }
  filterPayments(src, filter) {
    return src.filter(x => x.status === filter);
  }
  editRow(e, value) {
    console.log(
      "clicked on " + JSON.stringify(value) + " " + JSON.stringify(e)
    );
  }
  clickedOn(row)
  {
    var table =document.getElementById("myTable");
    console.log(JSON.stringify(table.rows[0]));
    
  }
  render() {
   

    const columns = [
      {
        Header: "Currency",
        accessor: "currency" // String-based value accessors!
      },
      {
        Header: "Debit Account",
        accessor: "debitAccountNumber",
        Cell: props => <span className="number">{props.value}</span> // Custom cell components!
      },
      {
        Header: "Credit Account",
        accessor: "creditAccountNumber",
        Cell: props => <span className="number">{props.value}</span> // Custom cell components!
      },
      {
        Header: "Amount",
        accessor:"amount",
       
      }
    
    ];
    return <ReactTable id="myTable" data={this.state.payments} columns={columns} />;
  }
}