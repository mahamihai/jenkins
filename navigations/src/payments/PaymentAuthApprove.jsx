import React, { Component } from "react";
import ReactTable from "react-table";
import "./PaymentsCore.css";
import { Dropdown, DropdownToggle, DropdownItem } from "mdbreact";
export class PaymentAuthApprove extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      payments: [],
      page: props.page,
      filter: props.filter
    };
  }
  getFunctionUrl(functions, name) {
    //console.log("The functions are" + JSON.stringify(this.props.functions));

    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {
        if (functions[t].name == name) {
          console.log("Found the link " + functions[t].name);
          return "http://localhost:8080" + functions[t].url;
        }
      }
    }
  }

  fetchPayments() {
    let url = this.getFunctionUrl(this.props.functions, "View payment");
    fetch(url, {
      method: "GET",

      mode: "cors",
      credentials: "include"
    })
      .then(res => res.json())
      .then(rez => {
        let filteredPayments = this.filterPayments(rez, this.state.filter);
        console.log(JSON.stringify(filteredPayments));

        this.setState({ payments: filteredPayments });
      })
      .catch(err => {});
  }
  componentDidMount() {
    this.fetchPayments();
  }
  filterPayments(src, filter) {
    return src.filter(x => x.status === filter);
  }

  generatePaymentBody(original) {
    var id = original.id;
    return {
      id: id
    };
  }
  MarkAsGood(index, original) {
    var payment = this.generatePaymentBody(index, original);

    let url = this.getFunctionUrl(this.props.functions, this.state.page);
    console.log("Url is" + JSON.stringify(url));

    fetch(url, {
      method: "POST",
      mode: "cors",
      credentials: "include",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify(payment)
    })
      .then(res => res.json())
      .then(res => {alert( JSON.stringify(res));
            this.fetchPayments();

        }
        );
  }
  getCancelUrl(functions, url) {
    let cancelUrl = this.getFunctionUrl(functions, this.state.page);
    cancelUrl += "/cancel";
    return cancelUrl;
  }
  cancelPayment(original) {
    var payment = this.generatePaymentBody(original);

    let url = this.getCancelUrl(this.props.functions, this.state.page);
    console.log(url + " " + JSON.stringify(payment));

    fetch(url, {
      method: "POST",
      mode: "cors",
      credentials: "include",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify(payment)
    })
      .then(res => res.json())
      .then(res => {alert(res);
        this.fetchPayments();
      });
  }
  render() {
    const columns = [
      {
        Header: "Currency",
        accessor: "currency" // String-based value accessors!
      },
      {
        Header: "Amount",
        accessor: "amount" // String-based value accessors!
      },
      {
        Header: "Debit Account",
        accessor: "debitAccountNumber",
        Cell: props => <span className="number">{props.value}</span> // Custom cell components!
      },
      {
        Header: "Credit Account",
        accessor: "creditAccountNumber",
        Cell: props => <span className="number">{props.value}</span> // Custom cell components!
      },

      {
        Header: "Approve",
        Cell: ({ original }) => (
          <button className="paymentBtn approve"
            onClick={() => {
              this.MarkAsGood(original);
            }}
          >
            Verify
          </button>
        )
      },
      {
        Header: "Cancel",
        Cell: ({ original }) => (
          <button className="paymentBtn reject"
            onClick={() => {
              this.cancelPayment(original);
            }}
          >
            Cancel
          </button>
        )
      }
    ];
    return (
      <ReactTable id="myTable" data={this.state.payments} columns={columns} />
    );
  }
}
