import React, { Component } from "react";
import ReactTable from "react-table";

import "./PaymentsCore.css";

export class PaymentCreate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      accounts: [],
      currencies: []
    };
    this.fetchData();
this.handleSubmit=this.handleSubmit.bind(this);
this.generatePayment=this.generatePayment.bind(this);
  }

  getFunctionUrl(name) {
    let functions = this.props.functions;
    console.log(JSON.stringify(functions));

    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {
        // console.log(functions[t].name+" "+name);

        if (functions[t].name == name) {
         // console.log(JSON.stringify(functions[t]));

          return "http://localhost:8080" + functions[t].url;
        }
      }
    }
  }
  fetchData() {
    // console.log("Here "+JSON.stringify(this.props.functions));

    let url = this.getFunctionUrl("Get currencies");
    fetch(url, {
      method: "GET",
      mode: "cors",
      credentials: "include"
    })
      .then(response => response.json())
      .then(response =>
        this.setState({
          currencies: response
        })
      );
    url = this.getFunctionUrl("View account");
    
    console.log("The url is" + url);

    fetch(url, {
      method: "GET",
      mode: "cors",
      credentials: "include"
    })
      .then(response => response.json())
      .then(response => response.filter(account => account.status === "ACTIVE"))
      .then(response =>
        this.setState({
          accounts: response
        })
      );
      url = this.getFunctionUrl("Get account currencies");
    console.log("The url for currencies is" + url);
      url='http://localhost:8080/getAccountCurrencies';
    fetch(url, {
      method: "GET",
      mode: "cors",
      credentials: "include"
    })
      .then(response => response.json())
      .then(response =>{
        this.setState({
          currencies: response
        });
        console.log("The currencies are "+JSON.stringify(response));
      }
      );
  }

  componentDidUpdate() {
  }
  createCurrenciesDropDown()
  {

    const accounts = this.state.currencies.map((currency) => (
      <option value={currency} key={currency}>
        {currency}
      </option>
    ));
    return accounts;
  }

  createAccountsDropdown() {
    //console.log(JSON.stringify(this.state.accounts));

    const accounts = this.state.accounts.map((account, key) => (
      <option value={account.numbers} key={key}>
        {account.number}
      </option>
    ));
    return accounts;
  }

  createCurrenciesDropdown() {
    //console.log(JSON.stringify(this.state.accounts));

    const currencies = this.state.currencies.map((currency, key) => (
      <option value={currency.name} key={key}>
        {currency.currency}
      </option>
    ));
    return currencies;
  }
generatePayment()
{
  
  let payment = {
      debitAccountNumber: document.getElementById("debitHolder").value,
      creditAccountNumber: document.getElementById("creditHolder").value,
      amount: document.getElementById("amountHolder").value,
      currency:  document.getElementById("currencyHolder").value ,
      
  }
 return payment
}
  handleSubmit(ev){
    ev.preventDefault();

    let payment=this.generatePayment();
    let url = this.getFunctionUrl("Enter payment");
    console.log(url+" " +JSON.stringify(payment));
    
    fetch(url ,{
        method: 'POST',
        mode: 'cors',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }, 
        body: JSON.stringify(payment)  
    })
    .then(res => res.json())
    .then(res => alert(res));
}

  newPaymentWindow() {
    let ui = [];
    const details = (
      <div>
        <form className="paymentCreateBox" onSubmit={this.handleSubmit}>
          <h2 className="paymentCreateTitle">payment Details</h2>
          <br />
          <br />
          <b className="innerText">Debit account:</b>
          <select id="debitHolder" className="paymentCreateSelect">
            {this.createAccountsDropdown()}
          </select>{" "}
          <br />
          <b className="innerText">Credit account:</b>
          <select id="creditHolder" className="paymentCreateSelect">
            {this.createAccountsDropdown()}
          </select>{" "}
          <br />
          <input
            type="number"
            min="0"
            placeholder="Amount"
            id="amountHolder"
            className="paymentCreateLine"
          />
            <br />
          <b className="innerText">Select currency</b>
          <select id="currencyHolder" className="paymentCreateSelect">
            {this.createCurrenciesDropDown()}
          </select>{" "}
          <br/>
          <input
            type="submit"
            value="Submit"
            id="submit"
            
            className="paymentCreateBtn"
          />
        </form>
      </div>
    );

    ui.push(details);
    return ui;
  }
  render() {
    return <div>{this.newPaymentWindow()}</div>;
  }
}
