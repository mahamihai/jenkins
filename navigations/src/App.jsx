import React, { Component } from "react";
import {HomeAnimation} from './home/HomeAnimation'
import "./App.css";
import { NewAccount } from "./accountView/NewAccount";
import {
  BrowserRouter as Router,
  Route,
  NavLink,
  Redirect
} from "react-router-dom";
import { Grabber } from "./accountView/Grabber";
import { ProfileViewTable } from "./profileView/ProfileViewTable";
import { LoginControl } from "./login/LoginControl";
import { RegisterControl } from "./register/RegisterControl";
import { UsersWindow } from "./register/UsersWIndow";
import { NavBar } from "./NavBar";
import { ViewAccount } from "./accountView/ViewAccount";
import { ViewUser } from "./register/ViewUser";
import { PaymentsCore } from "./payments/PaymentsCore";
import { ProfileView } from "./profileView/ProfileView";
import { BalanceViewTable } from "./balanceView/BalanceView";
export const UserContext = React.createContext({
  session: {
    functions: {},
    user: "",
    logout: [],
    login: [],
    rights: {},
    main: []
  }
});

class App extends Component {
  constructor(props) {
    super(props);

    this.setSelectedOption = this.setSelectedOption.bind(this);
    this.logOut = this.logOut.bind(this);
    this.login = this.login.bind(this);
    this.fetchRights = this.fetchRights.bind(this);
    this.isThisAuthenticated = this.isThisAuthenticated.bind(this);
    let oldState = this.getInitialState();
    console.log("The old state is" + JSON.stringify(oldState));

    if (typeof (oldState) === 'undefined' || oldState===null ) {
        console.log("Here");
      oldState = {
        user: "",
        functions: {}
      };
    }
    console.log("After check the state is "+JSON.stringify(oldState));
    
    this.state = {
      user: oldState.user,
      functions: oldState.functions,
      logIn: this.login,
      logOut: this.logOut,
      main: this
    };
  }

  getInitialState() {
    var selectedOption = localStorage.getItem("SavedState1") ;
    if (typeof selectedOption !== "undefined") {
      // console.log("What i got from memory " + JSON.stringify(selectedOption));
      selectedOption = JSON.parse(selectedOption);
      return selectedOption;
    }
    return {
      user: "",
      functions: {}
    };
  }

  setSelectedOption(save) {
    if (typeof save !== "undefined") {
      localStorage.setItem("SavedState1", JSON.stringify(save));
      // console.log("Saving the state" + JSON.stringify(save));
    }
  }
  componentWillMount() {
    //  this.fetchRights();
  }
  isThisAuthenticated() {
    //console.log("The user state  is " + this.state.user !== "");
    return this.state.user !== "" && typeof this.state.user != "undefined";
  }
  postProcessRights(raw) {
    let res = {};
    for (var t in raw) {
      if (raw.hasOwnProperty(t)) {
        res["Function" + Math.random()] = { name: raw[t], url: t };
      }
    }
    this.setState({
      functions: res
    });
    let state = {
      user: this.state.user,
      functions: res
    };
    
    this.setSelectedOption(state);
    //console.log("Sent the state " + JSON.stringify(state));
  }
  fetchRights() {
    console.log("Logged in");
    let url = "http://localhost:8080/user/current/urls";
    fetch(url, {
      method: "GET",
      mode: "cors",
      credentials: "include"
    })
      .then(res => res.json())
      .then(
        result => {
          console.log("Received the functions"+JSON.stringify(result));

          this.postProcessRights(result);
        },

        error => {
          console.log(error);
        }
      )
      .catch(err => {});
  }
  login(id, main) {
    console.log("Logged in here");
    let url = "http://localhost:8080/user/current";
     console.log("The id is:" + id);

    fetch(url + "?id=" + id, {
      method: "GET",
      mode: "cors",
      credentials: "include"
    })
      .then(res => res.json())
      .then(
        result => {
          console.log("Received the user", JSON.stringify(result));
          main.setState({
            user: result
          });
          main.fetchRights();
        },

        error => {
          console.log(error);
        }
      )
      .catch(err => {});
  }
  logOut(main) {
    console.log("logging out");
    main.setState({
      user: "",
      functions: {}
    });
    let state = {
      user: "",
      functions: {}
    };
    main.setSelectedOption(state);
  }
  componentDidMount() {}
  render() {
    let authenticated = this.isThisAuthenticated();

    console.log("The user in render  is" + authenticated);

    return (
      <UserContext.Provider
        value={{
          session: {
            functions: this.state.functions,
            user: this.state.user,
            logout: this.logOut,
            login: this.login,
            main: this
          }
        }}
      >
        <Router isAuthenticated={this.isThisAuthenticated}>
          <div>
            <div className="App-header" >
              <NavBar session={this.state} user={this.state.user} />
            </div>

            <div>
              <Route exact path="/" 
                render={({history}) => (
                  <HomeAnimation history={history} user={this.state.user}/>
                  )}/>
              <Route
                path="/allAccounts"
                render={({ history }) => 
                this.isThisAuthenticated() ? 

                  <Grabber history={history} functions={this.state.functions} />
                  : 
                    <Redirect to="/login" />
                }
              />

              <Route
                path="/allProfiles"
                render={({ history, authenticated }) => 
                  this.isThisAuthenticated() ? 
                    <ProfileViewTable
                      history={history}
                      functions={this.state.functions}
                    />
                   : 
                    <Redirect to="/login" />
                  
                }
              />

              <PrivateRoute isAuthenticated={this.isThisAuthenticated()} path="/insert" component={NewAccount} />
              <PrivateRoute isAuthenticated={this.isThisAuthenticated()}  path="/viewAccount" component={ViewAccount} />
              <PrivateRoute isAuthenticated={this.isThisAuthenticated()}  path="/viewProfile" component={ProfileView} />

              <Route
                path="/balances"
                history={this.history}
                render={({ history, authenticated }) =>
                  this.isThisAuthenticated() ? (
                    <BalanceViewTable
                      history={history}
                      functions={this.state.functions}
                    />
                  ) : (
                    <Redirect to="/login" />
                  )
                }
              />
              <Route
                path="/login"
                render={() => <LoginControl session={this.state} />}
              />
              <Route
                path="/viewUser"
                render={({ history }) => 
                this.isThisAuthenticated() ? 
                  <ViewUser
                    history={history}
                    functions={this.state.functions}
                    
                  />
                  : (
                    <Redirect to="/login" />
                  )
                }
              />
              <Route
                path="/payments"
                render={({ history }) => 
                this.isThisAuthenticated() ? 
                  <PaymentsCore
                    history={history}
                    functions={this.state.functions}
                  />
                  : (
                    <Redirect to="/login" />
                  )
                }
              />
              <Route path="/register" component={RegisterControl} />
              <Route
                path="/logout"
                render={props => {
                  this.logOut();
                  return null;
                }}
              />
              <Route
                path="/users"
                render={({ history }) => 
                this.isThisAuthenticated() ? 
                  <UsersWindow
                    history={history}
                    functions={this.state.functions}
                  />
                  : (
                    <Redirect to="/login" />
                  )
                }
              />
            </div>
          </div>
        </Router>
      </UserContext.Provider>
    );
  }
}
const PrivateRoute = ({ component: Component,isAuthenticated, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

export class Logout extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {}

  render() {
    console.log("Logged out");
    //this.props.history.push("/");
    return null;
  }
}

export class testing extends Component {
  render() {
    return <h1>hey</h1>;
  }
}

export const Home = () => (
  <div>
    <h2> Home page </h2>
  </div>
);
export default App;
//                            <Route path="/users" component={UsersWindow }/>} />
/*
<Private
                path="/test"
                isAuthenticated={this.state.user}
                render={() => (
                  <div>
                    <br />
                    <br />
                    <br />SIiiiii
                  </div>
                )}
              />


              */
