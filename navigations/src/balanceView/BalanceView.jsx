import React, { Component } from "react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import { UserContext } from "../App";
import './balanceView.css'

export class BalanceViewTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      balances: [],
      accounts: [],
      balanceModel: "",
      part: "",
      selectedAccountId: "",
    };

    this.expand = this.expand.bind(this);
    this.collapse = this.collapse.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.submitFinal  =this.submitFinal.bind(this);
    this.upperCase = this.upperCase.bind(this);
  }
  getFunctionUrl(functions,name) {

    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {

        if (functions[t].name == name) {
          return "http://localhost:8080" + functions[t].url;
        }
      }
    }
  }

  getModel() {
    //TODO here
    
    let url=this.getFunctionUrl(this.props.functions,"View balance model");
    fetch(url, {
      method: "GET",
      mode: "cors",
      credentials: 'include',
    })
      .then(res => res.json())
      .then(
        result => {
          
          this.setState({
            balanceModel: result
          });
        },
        error => {
          console.log(error);
        }
      )
      .catch(err => {});
  }

  getData() {
    fetch("http://localhost:8080/balance/view", {
      method: "GET",
      mode: "cors",
      credentials: 'include'
    })
      .then(res => res.json())
      .then(res => this.setState({
          balances: res,
      }))
      .catch(err => {
        err.text().then(errorMessage => {});
      });
  }

  getAccounts(){
    fetch("http://localhost:8080/account/view", {
      method: "GET",
      mode: "cors",
      credentials: 'include'
    })
      .then(res => res.json())
      .then(res => res.filter(account => !account.status.includes("PENDING")))
      .then(res => this.setState({
          accounts: res,
      }))
      .catch(err => {
        err.text().then(errorMessage => {});
      });
  }

  componentWillMount() {
    this.getData();
    this.getModel();
    this.getAccounts();
  }
  

  render() 
  {
    return (
      <div>
        <UserContext.Consumer>
          {item => {
            let session = item.session;

            let rendered = [];
            rendered.push(this.balancesOverview(session));
           
           
            return rendered;
          }}
        </UserContext.Consumer>
      </div>
    );
  }


  createCollumns(model) {
    let collumns = [];
    console.log("The model is " + JSON.stringify(model));
    collumns = Object.keys(model).map(key => {
      return {
        Header: key,
        accessor: key
      };
    });
    return collumns;
  }

  submitFinal(){
    let startDate = document.getElementById("startDate").value;
    let endDate = document.getElementById("endDate").value;
    let accountApi = {
      id: this.state.selectedAccountId,
      date1: startDate,
      date2: endDate,
    }
   let url=  this.getFunctionUrl(this.props.functions,"View balance for account between dates")
    fetch(url, {
      method: "POST",
      mode: "cors",
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(accountApi),
    })
      .then(res => res.json())
      .then(res => this.setState({
          balances: res,
      }))
      .catch(err => {
        err.text().then(errorMessage => {});
      });
  }

  handleChange(ev){
    this.upperCase(ev);
    let part = ev.target.value;
    let url= this.getFunctionUrl(this.props.functions,"View balance for account between dates")

    fetch("http://localhost:8080/balance/view", {
      method: "GET",
      mode: "cors",
      credentials: 'include'
    })
      .then(res => res.json())
      .then(res => res.filter(balance => balance.accountNumber.includes(part)))
      .then(res => this.setState({
          part: part,
          balances: res,
      }))
      .catch(err => {
        err.text().then(errorMessage => {});
      });
  }

  handleSubmit(ev){

    ev.preventDefault();
    let accountNum = document.getElementById("accountNum").value;
    let url= this.getFunctionUrl(this.props.functions,"View account")
    fetch(url, {
      method: "GET",
      mode: "cors",
      credentials: 'include'
    })
      .then(res => res.json())
      .then(res => res.filter(account => account.number === accountNum))
      .then(res => this.setState({
          selectedAccountId: res[0].id,
      }))
      .then(this.submitFinal)
      .catch(err => {
        err.text().then(errorMessage => {});
      });
  }

  generateAccounts(){
    const accounts = this.state.accounts.map((account, key) => <option key={key} value={account.number} >{account.number}</option>)
    return accounts;
  }

  collapse(){
    document.getElementById("searchOptions").hidden = true;
    document.getElementById("expandBtn").hidden = false;
  }

  expand(){
    document.getElementById("expandBtn").hidden = true;
    document.getElementById("searchOptions").hidden = false;
  }

  upperCase(ev){
    ev.target.value = ev.target.value.toUpperCase();
  }

  balancesOverview(session)
  {

    let rendered = [];
    let accountSearchInput = (
      <div className="balanceBox left">
        <h2 className="balanceTitle">Seach by account number:</h2>
        <input type="text" defaultValue={this.state.part} onChange={this.handleChange} placeholder="Account Number" className="balanceLine"
        style={{Bottom: 20}}/>
      </div>
    )
    let dateSearchForm = (
      <form onSubmit={this.handleSubmit}className="balanceBox right">
        <h2 className="balanceTitle">Search by account and date</h2><br />
        Account number: 
        <select id="accountNum" className="balanceSelect">
          {this.generateAccounts()}
        </select><br />
        Start date:
        <input type="date" id="startDate" className="balanceLine"/> <br />
        End date:
        <input type="date" id="endDate" className="balanceLine"/><br />
        <input type="submit" value="Submit" className="balanceBtn"/>
      </form>
    )
    let expandBtn = (
      <button onClick={this.expand} className="expandBtn" id="expandBtn">Click to search</button>
    )
    let searchOptions = (
      <div className="searchOptions" id="searchOptions" hidden="true">
        {accountSearchInput}
        <button className="collapseBtn" onClick={this.collapse}>Collapse</button> 
        {dateSearchForm}
      </div>
    )
     rendered.push(searchOptions);
     rendered.push(expandBtn);
     rendered.push(this.createTable());

    return rendered;
 
  }


  createTable() {
    let table = [];

    let colls = this.createCollumns(this.state.balanceModel);
    let data = this.state.balances;
    return (
      <ReactTable
        showPaginationTop={true}
        data={data}
        columns={colls}
      />
    );
  }
}
export default BalanceViewTable;