import React from 'react';
import { AccountModifyView } from './AccountModifyView';
import { UserContext } from "../App";

export class AccountModifyControl extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            currencies : [],
            users : [],
            accountTypes : [],
            financialStatuses : [],
        }

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(account){
        fetch('http://localhost:8080/account/update' ,{
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }, 
            body: JSON.stringify(account)  
        })
        .then(res => res.json())
        .then(res => alert(res));
    }

    componentDidMount(){
        fetch('http://localhost:8080/getAccountCurrencies' ,{
            method: 'GET',
            mode: 'cors',
            credentials: 'include'   
        })
        .then(response => response.json())
        .then(response => this.setState({
            currencies: response
        }));

        fetch('http://localhost:8080/getAccountTypes' ,{
            method: 'GET',
            mode: 'cors',
            credentials: 'include'   
        })
        .then(response => response.json())
        .then(response => this.setState({
            accountTypes: response
        }));

        fetch('http://localhost:8080/getAccountFinancialStatuses' ,{
            method: 'GET',
            mode: 'cors',
            credentials: 'include'   
        })
        .then(response => response.json())
        .then(response => this.setState({
            financialStatuses: response
        }));
    }

    render() {
        return ( 
          <AccountModifyView 
            accountTypes={this.state.accountTypes} 
            currencies={this.state.currencies} 
            users={this.state.users} 
            financialStatuses = {this.state.financialStatuses}
            account = {this.props.account}
            onSubmit={this.handleSubmit} />
        );
    }
}