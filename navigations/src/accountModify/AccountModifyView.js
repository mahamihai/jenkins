import React from 'react';
import '../accountCreate/accountCreateStyle.css';

export class AccountModifyView extends React.Component {

  constructor(props){
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);

  }

  handleSubmit(ev){
    ev.preventDefault();
    let account = {
        id: document.getElementById("accountId").innerText,
        number: document.getElementById("accountNum").value,
        name: document.getElementById("accountHolder").value,
        type: document.getElementById("accountType").value,
        address1: document.getElementById("country").value,
        address2: document.getElementById("district").value,
        address3: document.getElementById("city").value,
        address4: document.getElementById("street").value,
        currency: document.getElementById("currency").innerText,
        financialStatus: document.getElementById("financialStatus").value,
    }
    this.props.onSubmit(account);
  }

  createAccountTypesDropdown(){
    const types = this.props.accountTypes.map((account, key) => <option value={account} key={key}>{account}</option>);
    return types;
  }

  createFinancialStatusDropdown(){
    const financialStatuses = this.props.financialStatuses.map((financialStatus, key) => <option value={financialStatus} key={key}>{financialStatus}</option>);
    return financialStatuses;
  }

  render(){
      const details=(
          <form className="accountCreateBox" onSubmit={this.handleSubmit}>
            <p id="accountId" hidden>{this.props.account.id}</p>
            <h2 className="accountCreateTitle">Account Details</h2><br />
            <input type="text" placeholder="Account number" id="accountNum" className="accountCreateLine" defaultValue={this.props.account.number} readOnly/><br />
            <b className="innerText">Account holder:</b>
            <input type="text" placeholder="Account holder" id="accountHolder" className="accountCreateLine" defaultValue={this.props.account.name}/><br />
            <b className="innerText">Account type:</b>
            <select id="accountType" className="accountCreateSelect">
                <option selected disabled hidden>{this.props.account.type}</option>
                {this.createAccountTypesDropdown()}
            </select> 
            <input type="text" placeholder="Country" id="country" className="accountCreateLine" defaultValue={this.props.account.address1}/><br />
            <input type="text" placeholder="District/County" id="district" className="accountCreateLine" defaultValue={this.props.account.address2}/><br />
            <input type="text" placeholder="City/Town" id="city" className="accountCreateLine" defaultValue={this.props.account.address3}/><br />
            <input type="text" placeholder="Street" id="street" className="accountCreateLine" defaultValue={this.props.account.address4}/><br />
            Currency: <strong id="currency">{this.props.account.currency}</strong><br /><br />
            <b className="innerText">Financial Status:</b>
            <select id="financialStatus" className="accountCreateSelect">
                <option selected disabled hidden>{this.props.account.financialStatus}</option>
                {this.createFinancialStatusDropdown()}
            </select>
            <input type="submit" value="Submit" className="accountCreateBtn"/>
          </form>
      )
      return (
          <div>
            {details}
          </div>
        )
  }
}