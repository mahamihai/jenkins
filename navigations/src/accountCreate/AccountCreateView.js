import React from 'react';
import './accountCreateStyle.css';

export class AccountCreateView extends React.Component {

  constructor(props){
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateNumber = this.validateNumber.bind(this);
  }

  handleSubmit(ev){
    ev.preventDefault();
    let account = {
      number: document.getElementById("accountNum").value,
      name: document.getElementById("accountHolder").value,
      type: document.getElementById("accountType").value,
      address1: document.getElementById("country").value,
      address2: document.getElementById("district").value,
      address3: document.getElementById("city").value,
      address4: document.getElementById("street").value,
      currency: document.getElementById("currency").value,        
      financialStatus: document.getElementById("financialStatus").value,
    }
    this.props.onSubmit(account);
  }

  upperCase(ev){
    ev.target.value = ev.target.value.toUpperCase();
  }

  validateNumber(ev){
    this.upperCase(ev);
    if(document.getElementById("accountNum").value.length != 8){
      document.getElementById("accountNum").classList.add("red")
      document.getElementById("submit").disabled = true;
    } else {
      document.getElementById("accountNum").classList.remove("red");
      document.getElementById("submit").disabled = false;
    }
  }

  validateInput(){
    
  }

  createCurrenciesDropdown(){
    const currencies = this.props.currencies.map((currency, key) => <option value={currency} key={key}>{currency}</option>);
    return currencies;
  }

  createAccountTypesDropdown(){
    const types = this.props.accountTypes.map((account, key) => <option value={account} key={key}>{account}</option>);
    return types;
  }

  createFinancialStatusDropdown(){
    const financialStatuses = this.props.financialStatuses.map((financialStatus, key) => <option value={financialStatus} key={key}>{financialStatus}</option>);
    return financialStatuses;
  }

  render(){
      const details=(
          <form className="accountCreateBox" onSubmit={this.handleSubmit}>
            <h2 className="accountCreateTitle">Account Details</h2><br />
            <input type="text" placeholder="Account number" id="accountNum" className="accountCreateLine" onBlur={this.validateNumber} onKeyUp={this.validateNumber}/><br />
            <input type="text" placeholder="Account holder's name" id="accountHolder" className="accountCreateLine"/>
            <b className="innerText">Account type:</b>
            <select id="accountType" className="accountCreateSelect">
                {this.createAccountTypesDropdown()}
            </select> 
            <input type="text" placeholder="Country" id="country" className="accountCreateLine" /><br />
            <input type="text" placeholder="District/County" id="district" className="accountCreateLine" /><br />
            <input type="text" placeholder="City/Town" id="city" className="accountCreateLine" /><br />
            <input type="text" placeholder="Street" id="street" className="accountCreateLine" /><br />
            <b className="innerText">Currency:</b>
            <select id="currency" className="accountCreateSelect">
                {this.createCurrenciesDropdown()}
            </select> <br />
            <b className="innerText">Financial Status:</b>
            <select id="financialStatus" className="accountCreateSelect">
                {this.createFinancialStatusDropdown()}
            </select>
            <input type="submit" value="Submit" id="submit" disabled className="accountCreateBtn"/>
          </form>
      )
      return (
          <div>
            {details}
          </div>
        )
  }
}