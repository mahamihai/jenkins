import React from 'react';
import './loginStyle.css';

export class LoginView extends React.Component {

  constructor(props){
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

    render() {
        const login = (
          <form onSubmit={this.handleSubmit} className="loginBox">
          <h2 className="loginTitle">Log in</h2>
            <input type="text" placeholder="Username" defaultValue="gigel" id="user" className="loginLine" /><br />
            <input type="password" placeholder="Password" defaultValue="pass" id="password" className="loginLine" /><br />
            <input type="submit" value="Log in" className="loginBtn" />
          </form>
        );

        return (
          <div>
            {login}
          </div>
        );
      }
    
    handleSubmit(ev){
      ev.preventDefault();
      let params = {
        username: document.getElementById('user').value,
        password: document.getElementById('password').value
      };
      this.props.onSubmit(params);
    }
}