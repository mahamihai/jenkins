import React from 'react';
import { LoginView } from './LoginView';

export class LoginControl extends React.Component {

    constructor(props){
        super(props);

        this.handleLogin = this.handleLogin.bind(this);
    }

    handleLogin(params){
        let formData = new FormData();
        formData.append('username', params.username);
        formData.append('password', params.password);

        fetch('http://localhost:8080/login',
        {
            method: 'POST',
            mode: 'cors',
            body: formData,
            credentials: 'include'
        })
        .then(response => response.json())
        .then(response =>
            
            
            {
                this.props.session.logIn(response.id,this.props.session.main);
                console.log("FInal"+JSON.stringify(response));
            }
        );
                
        
    }

    //     var xhr = new XMLHttpRequest();
    //     xhr.withCredentials = true;
        
    //     xhr.addEventListener("readystatechange", function () {
    //       if (this.readyState === 4) {
    //         console.log(this.responseText);
    //       }
    //     });
        

     
    //     xhr.open("POST", "http://localhost:8080/login");
    // console.log('ok');
    //     xhr.withCredentials = true;
        
    //     xhr.send(formData);
    

    render(){
        return <LoginView onSubmit={this.handleLogin} />;
    }
}