//LEGACY, CAN BE DELETED

import React from 'react';
import '../profileAdd/profileStyle.css';

export class SelectProfileView extends React.Component {

  constructor(props){
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.createProfiles = this.createProfiles.bind(this);
  }
  
  createProfiles(){
    const profiles = this.props.profiles.map((profile) => <option type='submit' value={profile.name}>{profile.name}</option>);
    return profiles;
  }

    render() {
        const profile = (
          <form onSubmit={this.handleSubmit} className="profileBox">
            <h3 className="profileTitle">Select a profile to modify:</h3>
            <br />
            <br />
            <select id="selectedProfile" className = "profileSelect">
                {this.createProfiles()}
            </select>
            <input type='submit' value="Select" className="profileBtn" />
          </form>
        );

        return (
          <div>
            {profile}
          </div>
        );
      }
    

    handleSubmit(ev){
      ev.preventDefault();
      let params = document.getElementById('selectedProfile').value;
      this.props.onSubmit(params);
    }
}