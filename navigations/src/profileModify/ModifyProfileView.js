import React from 'react';
import '../profileAdd/profileStyle.css';

export class ModifyProfileView extends React.Component {

  constructor(props){
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.createPermissions = this.createPermissions.bind(this);
    this.parseCheckboxes = this.parseCheckboxes.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidUpdate(){
    this.fillExistingPermissions();
  }
  
  createPermissions(){
    const permissions = this.props.permissions.map((permission) => 
              <div><input type='checkbox' id={permission} className="checkBox" value={permission} />{permission}</div>);
    return permissions;
  }

  fillExistingPermissions(){
    let permissions = this.props.profile.functions;
    permissions = permissions.substring(0, permissions.length - 1);
    permissions = permissions.split(';');
    for (let index in permissions){
      if (document.getElementById(permissions[index]) !== null)
        document.getElementById(permissions[index]).checked = 'true';
    }
  }

    render() {
        const profile = (
          <form onSubmit={this.handleSubmit} className="profileBox">
            <h2 className="profileTitle"> Modify profile</h2>
            <br />
            <h3>Profile {this.props.profile.name}'s permissions:</h3>
            <br />
            {this.createPermissions()}
            <input type="submit" value="Submit" className="profileBtn" />
            <button className="profileBtn" onClick={this.handleClick}>Back </button>
          </form>
        );

        return (
          <div>
            {profile}
          </div>
        );
      }
    
    parseCheckboxes(){
      var checkedValues = ''; 
      var inputElements = document.getElementsByClassName('checkBox');
      for(var i=0; inputElements[i]; ++i){
            if(inputElements[i].checked){
                 checkedValues += inputElements[i].value + ';';
            }
      }
      return checkedValues;
    }  

    handleClick(ev){
        this.props.onClick();
    }

    handleSubmit(ev){
      ev.preventDefault();
      let params = {
        name: this.props.profile.name,
        functions: this.parseCheckboxes()
      };
      this.props.onSubmit(params);
    }
}