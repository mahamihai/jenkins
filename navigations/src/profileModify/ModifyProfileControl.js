import React from 'react';
import { ModifyProfileView } from './ModifyProfileView';
import { SelectProfileView } from './SelectProfileView';

export class ModifyProfileControl extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            permissions: [],
            status: "selection"
        };
        this.handleModify = this.handleModify.bind(this);
        this.handleBack = this.handleBack.bind(this);
    }

    handleModify(params){
        fetch('http://localhost:8080/profile/update' ,{
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            }, 
            body: JSON.stringify(params)  
        })
        .then(res => res.json())
        .then(res => alert(res))
        .then(this.setState({
            status: "selection"
        }))
    }

    handleBack(){
        alert("No")
    }


    componentDidMount(){
        fetch('http://localhost:8080/profile/view' ,{
            method: 'GET',
            mode: 'cors',
            credentials: 'include'   
        })
        .then(response => response.json())
        .then(response => response.map(profile => profile.status === "ACTIVE"))
        .then(response => this.setState({
            profiles: response,
        }));

        fetch('http://localhost:8080/rights' ,{
            method: 'GET',
            mode: 'cors',
            credentials: 'include'   
        })
        .then(response => response.json())
        .then(response => this.setState({
            permissions: response,
        }));
    }

    render(){
        return <ModifyProfileView 
                onClick={this.handleBack} 
                onSubmit={this.handleModify} 
                permissions={this.state.permissions} 
                profile={this.props.profile}/>;
        
    }
}