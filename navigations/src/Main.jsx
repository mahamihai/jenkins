import React, {
    Component
  } from 'react';
  import logo from './logo.svg';
  import './App.css';
  import {
    NewAccount
  } from './accountView/NewAccount';
  import {
    BrowserRouter as Router,
    Route,
    NavLink
  } from "react-router-dom";
  import {  Grabber} from './accountView/Grabber';
  import { LoginControl } from './login/LoginControl';
  import { RegisterControl } from './register/RegisterControl';
  
  import {NavBar} from './NavBar';
  import { ViewAccount } from './accountView/ViewAccount';
  export class App extends Component {
    render() {
      return (
  
  
        <Router >
        < div >
           <div className='App-header'>
          <NavBar />
          
          </div>
     
  
          <div >
          <Route exact path = "/"component = {Home}/> 
        <Route path = "/all"component = { Grabber} />
         <Route path = "/insert"component = {NewAccount}/>
         <Route path = "/viewAccount" component = {ViewAccount}/>
         <Route path = "/login" component = {LoginControl} />
         <Route path = "/register" component = {RegisterControl} />
         </div>
         </ div>
         </Router>
         
       
  
      )
    }
  
  }
  
  export class testing extends Component {
    render() {
      return ( <h1 >
         hey 
         </h1>);
      }
    }
  
    export const Home = () => ( 
      <div >
      <h2 > Home page </h2> 
      </div>
    );
    export default App;