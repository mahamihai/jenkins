import React,{Component} from 'react';
import { UserContext } from "./App";
import './NavigationBar/NavBar.css'

import {BrowserRouter as Router,Route,NavLink} from "react-router-dom";
export class NavBar extends Component
{

    constructor(props)
    {
      super(props);
      this.state={
        user:this.props.user,
        currentTime: new Date().toLocaleString()

      }
      console.log("the user is"+JSON.stringify(this.state.user));
      this.generateMenu=this.generateMenu.bind(this);
    }
    fetchOptions()
    {
      return ({option1:"Home",
      option3:"Users",
      option2:"Accounts"})
;
    }

    componentDidMount(){
        setInterval( () => {
          this.setState({
            currentTime : new Date().toLocaleString()
          })
        },1000)
    }

    getUserOptions(rights)
    {
      
      let rendering=[];
      rendering.push(
                
        <li className="nav-item active">
          <NavLink className="nav-link" to="/">Home</NavLink>
         </li>
    )
      for(var option in rights)
      {
       
          if (rights.hasOwnProperty(option)) {
             switch(rights[option].name){

          
             case "View user":
                            rendering.push(
                              
                                <li className="nav-item active">
                                  <NavLink className="nav-link" to="/users">Users</NavLink>
                                </li>
                            )
                            break;
              case "View payment":
                        rendering.push(
                          
                            <li className="nav-item active">
                              <NavLink className="nav-link" to="/payments">Payments</NavLink>
                            </li>
                        )
                          
                              break;
              case "View account":
              rendering.push(
               
                  <li className="nav-item active">
                    <NavLink className="nav-link" to="/allAccounts">Accounts</NavLink>
                  </li>
              )
          
                    break;
              case "View profile":
              rendering.push(
               
                    <li className="nav-item active">
                      <NavLink className="nav-link" to="/allProfiles">Profiles</NavLink>
                    </li>
                  )
              
                    break;

              case "View balance":
              rendering.push(
               
                    <li className="nav-item active">
                      <NavLink className="nav-link" to="/balances">Balances</NavLink>
                    </li>
                  )
              
                    break;


             }
          }
      
      }
      return (  <ul key={option}  className="navbar-nav mr-auto">{rendering}</ul>)
    }
    getUserInfo()
    {
      let userName="";
      if(typeof(this.props.user)!=="undefined")
      {
        userName =this.props.name
      }
      return (<div  >
          <span style={{float:"left", position:"relative", bottom: "15%", left: -15}}>
          <br/>
          <div className="nav-item active" ><strong>Hello,{" "+this.props.user.name} </strong></div>

          </span>
        
          <span style={{float:"left"}}>
          <img className="nav-item active" src="http://cdn.onlinewebfonts.com/svg/img_504582.png" height="42" width="42"  className="img-thumbnail rounded float-left"/>
          </span>
      </div>
      )
        
    }
   isAuthenticated()
   {
     return (this.state.user!=="");
   }
    

    generateMenu(permissions)
    {

      let options=this.getUserOptions(permissions);
      if(this.isAuthenticated())
      {
        return (
        <nav className="navbar  navbar-expand-md navbar-dark bg-dark" >
            <div className="navbar-collapse collapse w-100 order-0 order-md-0 dual-collapse2">
            {options}
             </div>
             <div class="mx-auto order-0">            <NavLink className="navbar-brand mx-auto" to="/">{this.state.currentTime}</NavLink>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
                <span className="navbar-toggler-icon"></span>
            </button>
            </div>
       
            <div className="navbar-collapse collapse w-100 order-3 dual-collapse2">
            <ul class="navbar-nav ml-auto">
                  {this.getUserInfo()}
                  <div className="divider"/>

                      <div class="dropdown">
                    <button className="dropbtnNav">My account 
                      <i className="fa fa-caret-down"></i>
                    </button>
                    <div className="dropdown-content">
                      <button className="button5" onClick={()=>{
                      
                        this.props.session.logOut(this.props.session.main)
                        
                        }} >LogOut</button>
                         </div>
                
                       
                 
                   
                  </div> 
            
  </ul>
  </div>
    </nav> )
      }
      else
      {
                  return (  <nav className="navbar  navbar-expand-md navbar-dark bg-dark">
                  <div className="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                
                  </div>
              <div className="mx-auto order-0">
                  <NavLink className="navbar-brand mx-auto" to="/">{this.state.currentTime}</NavLink>
                 
              </div>
              <div className="navbar-collapse collapse w-100 order-3 dual-collapse2">
                  <ul className="navbar-nav ml-auto">
                    
                      <li className="nav-item">
                          <NavLink className="nav-link" to="/login">LogIn</NavLink>
                      </li>
                  </ul>
              </div>
          </nav> )
      }
    }
    componentWillReceiveProps(nextProps) {
      if(JSON.stringify(this.props.user) !== JSON.stringify(nextProps.user)) // Check if it's a new user, you can also use some unique property, like the ID
      {

            this.setState(
              {
                user:nextProps.user
              }
            )
      }
  } 
    render()
    {
      let ui=[];
     
        return (
          
          <UserContext.Consumer>
              {item => {
                this.generateMenu();
                let session = item.session;
                ui.push( this.generateMenu(session.functions));
                return ui;
              }}
            </UserContext.Consumer>
          
          );



    }
  }
