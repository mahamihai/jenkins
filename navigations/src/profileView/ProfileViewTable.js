import './profileView.css'
import React, { Component } from "react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import {ProfileApprovalsControl} from '../approvals/ProfileApprovalsControl';
import { UserContext } from "../App";
import { AddProfileControl } from '../profileAdd/AddProfileControl'

export class ProfileViewTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modify:false,
      profiles: [],
      profileModel: ""
    };
    this.onRowClick = this.onRowClick.bind(this);
    this.createNewProfileClicked = this.createNewProfileClicked.bind(this);
    this.profilesOverview = this.profilesOverview.bind(this);
    this.showPendingTable = this.showPendingTable.bind(this);
  }
  getFunctionUrl(functions,name) {

    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {

        if (functions[t].name == name) {
          return "http://localhost:8080" + functions[t].url;
        }
      }
    }
  }
  getModel() {
    //TODO here
   
    let url=this.getFunctionUrl(this.props.functions,"View profile model");
    fetch(url, {
      method: "GET",
      mode: "cors",
      credentials: 'include',
    })
      .then(res => res.json())
      .then(
        result => {
          
          this.setState({
            profileModel: result
          });
        },
        error => {
          console.log(error);
        }
      )
      .catch(err => {});
  }

  createNewProfileClicked() {
    if(this.state.createClicked === "true")
      this.setState({
        createClicked : "false"
      })
    else 
      this.setState({
        createClicked : "true"
      })
  }

  componentWillMount() {
    this.getData();
    this.getModel();
  }

  checkRights(functions) {
    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {
        if (functions[t].name=="Create profile") {
          return (
            <button
              type="button"
              onClick={this.createNewProfileClicked}
              className="btn btn-success"
            >
              Create new profile
            </button>
          );
        }
      }
    }
    return null;
  }
  createProfile(){
    return <AddProfileControl />
  }

  render() 
  {
    return (
      <div>
        <UserContext.Consumer>
          {item => {
            let session = item.session;

            let rendered = [];
            rendered.push(!this.state.createClicked && this.profilesOverview(session));

            rendered.push(this.state.createClicked && <AddProfileControl />);
           
           
            return rendered;
          }}
        </UserContext.Consumer>
      </div>
    );
  }

  onRowClick(state, rowInfo, column, instance) {
    return {
      onClick: e => {
        let clickedId = rowInfo.original["id"];
        console.log("The clicked id is " + clickedId),
          this.props.history.push({
            pathname: "viewProfile",
            state: { id: clickedId }
          });


        }
      }
  }

  filterNonPending(result){
    let filteredAccounts = result.filter(profile => !profile.status.includes("PENDING"));
    this.setState({
      profiles: filteredAccounts
    })
  }

  createCollumns(model) {
    let collumns = [];
    console.log("The model is " + JSON.stringify(model));
    collumns = Object.keys(model).map(key => {
      return {
        Header: key,
        accessor: key
      };
    });
    return collumns;
  }
  getData() {
    let ui = [];
    fetch("http://localhost:8080/profile/view", {
      method: "GET",
      mode: "cors",
      credentials: 'include'
    })
      .then(res => res.json())
      .then(res => this.filterNonPending(res))
      .catch(err => {
        err.text().then(errorMessage => {});
      });
  }
  checkIfHasRight(right) {
    let functions = this.props.functions;
    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {
        if (functions[t].name == right) {
         // console.log(JSON.stringify(functions[t]));

          return true;
        }
      }
    }
    return false;
  }
  profilesOverview(session)
  {
    //console.log(JSON.stringify(session));

    let rendered = [];
    rendered.push(this.checkRights(session.functions));
    rendered.push(this.checkIfHasRight("Modify profile") &&
      <div>
         <label class="switch">
    <input id="modifyProfile" type="checkbox" onChange={this.showPendingTable}/>
    <span class="slider round"></span>
  </label>
  <label for="modifyProfile">Approve profiles</label>
</div>
  
  );
     rendered.push((!this.state.modify) && this.createTable())
     rendered.push((this.state.modify) && this.profilesModify());

    return rendered;
 
  }

  showPendingTable()
  {
    this.setState(
      {
        modify: (!this.state.modify)
      }
    );
  }

  profilesModify()
  {
    let rendered = [];
    rendered.push(<ProfileApprovalsControl/>);



    return rendered;
  }


  createTable() {
    let table = [];

    let colls = this.createCollumns(this.state.profileModel);
    console.log("cols are:" + JSON.stringify(colls));
    let data = this.state.profiles;
    console.log("the data is:" + JSON.stringify(this.state.profiles));
return (
      <ReactTable
        showPaginationTop={true}
        data={data}
        columns={colls}
        getTrProps={this.onRowClick}
      />
    );
  }
}
export default ProfileViewTable;