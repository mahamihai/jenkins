import React, { Component } from "react";
import { UserContext } from "../App";
import { ModifyProfileControl } from '../profileModify/ModifyProfileControl'

export class ProfileView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      profileInfo: "",
      modifyClicked: "false"
    };
    this.getProfileInfo();
    this.enableModify = this.enableModify.bind(this);
  }
  createDisplayForm;

  getProfileInfo() {
    let queryString = this.props.location.state; //get passed id from history
    let id = parseInt(queryString["id"]);
    //let id=1;
    fetch("http://localhost:8080/profile/viewone?id=" + id, {
      method: "GET",
      mode: "cors",
      credentials: 'include',
    })
      .then(res => res.json())
      .then(
        result => {
          console.log(JSON.stringify(result));
          this.setState({
            profileInfo: result
          });
          console.log("the result is " + JSON.stringify(result));
        },

        error => {
          console.log(error);
        }
      )
      .catch(err => {});
  }
  checkIfHasRight(right) {
    let functions = this.props.functions;
    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {
        if (functions[t].name == right) {
         // console.log(JSON.stringify(functions[t]));

          return true;
        }
      }
    }
    return false;
  }
  enableModify(){
    if(this.state.modifyClicked === "true"){
      this.setState({
        modifyClicked: "false",
      })
    } else {
      this.setState({
        modifyClicked: "true",
      })
    }
  }

  deleteProfile(profile, url) {
    console.log("I am deleting" + url);
    fetch("http://localhost:8080" + url, {
      method: "POST",
      mode: "cors",
      credentials: 'include',
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(profile)
    })
      .then(res => res.json())
      .then(
        result => {
          console.log(JSON.stringify(result));
        },
        error => {
          console.log(error);
        }
      )
      .catch(err => {});
      this.props.history.push("/allProfiles")

  }

  getButtons(session) {
    let thisAccount = this.props.location.history;
    let renderedButtons = [];
    let functions = session.functions;
    let backButton = <button className="btn btn-success" onClick={this.enableModify}>Back</button>
    for (var t in functions) {
      if (functions.hasOwnProperty(t)) {
        switch (functions[t].name) {
          case "Delete profile": {
            let url = functions[t].url;
            renderedButtons.push(
              <button
                type="button"
                onClick={() => {
                  this.deleteProfile(this.state.profileInfo, url);
                }}
                className="btn btn-danger "
              >
                Delete profile
              </button>
            );
          }
          break;
          case "Modify profile": {
            renderedButtons.push(
              <button
                type="button"
                onClick={this.enableModify}
                className="btn btn-warning "
              >
                Modify profile
              </button>
            );
          }
          break;
        }
      }
    }
    if(this.state.modifyClicked != "true")
      return renderedButtons;
    else
      return backButton;
  }
  getProfileForm() {
    if (this.state.modifyClicked != "true") {
      return (
        this.state.profileInfo != "" && (
          <div>
            <div className="form-group row">
              <label htmlFor="nameBox" className="col-sm-2 col-form-label">
                Id
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="nameBox"
                  value={this.state.profileInfo.id}
                />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="nameBox" className="col-sm-2 col-form-label">
                Name
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="nameBox"
                  value={this.state.profileInfo.name}
                />
              </div>
            </div>
            <div className="form-group row">
              <label htmlFor="nameBox" className="col-sm-2 col-form-label">
                Functions
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="nameBox"
                  value={this.state.profileInfo.functions}
                />
              </div>
            </div>

            <div className="form-group row">
              <label htmlFor="statusBox" className="col-sm-2 col-form-label">
                Status
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  readOnly
                  className="form-control"
                  id="statusBox"
                  value={this.state.profileInfo.status}
                />
              </div>
            </div>
          </div>
        )
      );
    }
  }

  getProfileModify(){
    if(this.state.modifyClicked === "true"){
      return <ModifyProfileControl profile={this.state.profileInfo} />
    }
  }

  render() {
    return (
      <div>
        <form>
          {
            <UserContext.Consumer>
              {item => {
                let rendered = [];
                let session = item.session;
                rendered.push(this.getProfileForm());
                
                rendered.push(this.getButtons(session));

                rendered.push(this.getProfileModify());
                return rendered;
              }}
            </UserContext.Consumer>
          }
        </form>
      </div>
    );
  }
}